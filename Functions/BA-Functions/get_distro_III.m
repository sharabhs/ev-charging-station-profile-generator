function [Distro_1] = get_distro_III(Input, Charger, Scale, Location, Scale_fit)
%% Get Distribution of Pcs, #LVs, Energy and Duration for WD, Sa & Su      
disp('Get distribution...')
set(0,'DefaultFigureVisible','off');

%% Constants                                                               
BA_Test_constants
Trash1.e_res = 1;                                                           % [kWh]
Trash1.d_res = 60;                                                          % [s]
trash.res = 1/3600;                                                         % h    
trash.max_scale = 300;
if numel(Scale_fit)==0
    load Scale_fit
end

%% example data                                                            
% Input = Good;
% Input = Test_simu(S);
% Charger = Test_simu(S);
% clear Distro_1 Trash

%% Get weekday and weekenddays elements                                    
Distro_1.daytype(1).name = 'Weekday';
Distro_1.daytype(2).name = 'Saturday';
Distro_1.daytype(3).name = 'Sunday';
Distro_1.daytype(4).name = 'Allday';

for w=1:3
Intern.daytype(w).elem = find(Input.data(:,16)==w);
end

Intern.daytype(4).elem = find(Input.data(:,16));

% for full discrete array:
% for d=1:24
%     Trash1.discr_elem{1,d} = sort([d:24*7:numel(Charger(1).full_D_h.x), d+24*1:24*7:numel(Charger(1).full_D_h.x), ...
%         d+24*2:24*7:numel(Charger(1).full_D_h.x), d+24*3:24*7:numel(Charger(1).full_D_h.x), ...
%          d+24*4:24*7:numel(Charger(1).full_D_h.x)]');
%     Trash1.discr_elem{2,d} = [d+24*5:24*7:numel(Charger(1).full_D_h.x)]';
%     Trash1.discr_elem{3,d} = [d+24*6:24*7:numel(Charger(1).full_D_h.x)]';
%     Trash1.discr_elem{4,d} = [d:24:numel(Charger(1).full_D_h.x)]';
% end

%% Preparation                                                             
Trash2.max_amount=0;
for c=1:numel(Charger)
    Trash2.max_amount = max([max(Charger(c).D_h.amount), Trash2.max_amount]);
end
counter = 0;
tic

% get idle time elements
trash.idle_case_el = find(Input.data(:,13) == 2 | Input.data(:,13) == 3); 

%% Loop through daytime                                                    
for d = 1:24

    %% Get daytime elements                                                
    Distro_1.elem_dt{d} = find(Input.data(:,17)==d-1);

    %% Power class distribution                                            
    Distro_1.pc_mat_dt(:,[1,d+1]) = count_same_integers(Input.data(Distro_1.elem_dt{d},12), 1, Power_classes.nu_of_classes);
    % Normalize distribution           
    Distro_1.pc_mat_dt_norm = ...
        [Distro_1.pc_mat_dt(:,1), Distro_1.pc_mat_dt(:,2:end)./...
        repmat(sum(Distro_1.pc_mat_dt(:,2:end)),Power_classes.nu_of_classes,1)];

    %% Charge case distribution                                            
    Distro_1.cc_mat_dt(:,[1,d+1]) = count_same_integers(Input.data(Distro_1.elem_dt{d},13), 1, 5);
    % Normalize distribution                                          
    Distro_1.cc_mat_dt_norm = ...
        [Distro_1.cc_mat_dt(:,1), Distro_1.cc_mat_dt(:,2:end)./...
        repmat(sum(Distro_1.cc_mat_dt(:,2:end)),5,1)];

    %% Energy & Duration Distribution per power class                      

        % loop through power classes 
        for p=1:Power_classes.nu_of_classes
            
            Power_classes.elements{p}= find(Input.data(:,12)==p);

            % Get Input elements of this daytime and power class combination
            Trash(d,p).elm = get_same_elements(Distro_1.elem_dt{d}, Power_classes.elements{p});

            % Energy (7) & Actual duration (15, rounded to min)  or measured duration (8)
            Trash(d,p).e = Input.data(Trash(d,p).elm,7);                                % kWh
            Trash(d,p).d = Input.data(Trash(d,p).elm,15);                               % s

            % If LVs exist in this combination
            if numel(Trash(d,p).elm)>0
                % get amount of bins for histogram
                Trash(d,p).hist_amount = round(max([(max(Trash(d,p).e)-min(Trash(d,p).e))/Trash1.e_res, ...
                    (max(Trash(d,p).d)-min(Trash(d,p).d))/Trash1.d_res]));
                % have at least one bin
                if  Trash(d,p).hist_amount==0  Trash(d,p).hist_amount=1; end
                % create 3D distribution with histogram 
                Trash(d,p).hist=histogram2(Trash(d,p).e, Trash(d,p).d, Trash(d,p).hist_amount);
                Distro_1.daytime(d).power(p).e_d_distr = Trash(d,p).hist.Values;
                % get X and Y vectors
                Distro_1.daytime(d).power(p).d = linspace(min(Trash(d,p).d), max(Trash(d,p).d), numel(Distro_1.daytime(d).power(p).e_d_distr(1,:)));
                Distro_1.daytime(d).power(p).e = linspace(min(Trash(d,p).e), max(Trash(d,p).e), numel(Distro_1.daytime(d).power(p).e_d_distr(:,1)));
            % If this combination doesnt exist -> zero distr.
            else
                Distro_1.daytime(d).power(p).e_d_distr = 0;
                Distro_1.daytime(d).power(p).e = 0;
                Distro_1.daytime(d).power(p).d = 0;
            end

        end
  
    %% Loop Through daytype & Location -> amount distribution              
    for w = 1:4
        % Loop through Location
        for l=1:numel(Location)
            %% Get Daytype & - time & Location elements                    
            Distro_1.location(l).daytype(w).elem_dt{d} = get_same_elements(Location(l).LV_el, find(Input.data(:,17)==d-1));
            Distro_1.location(l).daytype(w).elem_dt{d} = get_same_elements(Distro_1.location(l).daytype(w).elem_dt{d}, Intern.daytype(w).elem);

            %% Amount Distribution old                                     
%             % get distro from every charger (repeated discr)
%             for c = 1 : numel(Charger)
%                 Trash3(:,:,c) = count_same_integers(Charger(c).full_D_h.amount(Trash1.discr_elem{w,d}), 0, Trash2.max_amount);
%             end
%             % get mean amount for one charger and for one day
%             Distro_1.daytype(w).daytime(d).amount = ...
%                 mean(Trash3(:,2,:),3) / sum(mean(Trash3(:,2,:),3));
%             % get limited amount (<=2)
%             Distro_1.daytype(w).daytime(d).lim_amount = ...
%                 Distro_1.daytype(w).daytime(d).amount(1:3);
%             Distro_1.daytype(w).daytime(d).lim_amount(3) = ...
%                 Distro_1.daytype(w).daytime(d).lim_amount(3) + ...
%                 sum(Distro_1.daytype(w).daytime(d).amount(4:end));
        % get expected amount

            %% Scaled Amount Distribution                                  
%             for s=1:numel(Location(l).charger_el)
%                 Distro_1.location(l).daytype(w).daytime(d).scale(s).lim_amount = Scale(s).location(l).daytype(w).lim_distr(:,d);
%                 Distro_1.location(l).daytype(w).daytime(d).scale(s).exp_amount  = ...
%                     sum(prod([[0:2]', Distro_1.location(l).daytype(w).daytime(d).scale(s).lim_amount],2));
%             end
            
            %% Scaled fitted amount distribution                           
            for s=1:trash.max_scale
                Distro_1.location(l).daytype(w).daytime(d).scale(s).new_lim_amount_m = Scale_fit.new_lim_distr_m(:, d, s, w, l);
                Distro_1.location(l).daytype(w).daytime(d).scale(s).new_exp_amount_m = Scale_fit.new_lim_exp_am_m(d, s, w, l);
            end
            
            %% Idle time                                                   
            % get idle times of this time and charge case 2 | 3
            trash.idle_elements = get_same_elements(trash.idle_case_el, Distro_1.location(l).daytype(w).elem_dt{d});
            trash.wait = Input.data(trash.idle_elements  ,8) - ...
                Input.data(trash.idle_elements ,15);
            % if data exists
            if numel(trash.wait)~=0
                % remove negative values (0 ... -30 s)
                trash.wait(trash.wait<0)=0;
                % get normalized distribution (1/10th h)
                trash.trash = histogram(trash.wait/3600, ...
                    max([1 round(max(trash.wait/3600)/trash.res)]), 'Normalization', 'probability');
                trash2(d,w,l).wait_dis = trash.trash.Values;
                % get time 
                trash2(d,w,l).wait_t = 3600*[0:trash.res:numel(trash2(d,w,l).wait_dis)*trash.res];
                % cumsum distribution
                trash2(d,w,l).wait_cum = [0 cumsum(trash2(d,w,l).wait_dis)];
                % remove same elements
                [trash2(d,w,l).wait_cum, trash.ue] = unique(trash2(d,w,l).wait_cum);
                trash2(d,w,l).wait_t = trash2(d,w,l).wait_t(trash.ue);
                % do inverse interpolation
                Distro_1.location(l).daytype(w).daytime(d).idle = griddedInterpolant(trash2(d,w,l).wait_cum',trash2(d,w,l).wait_t');
                % test
    %             for c=1:10000
    %                 trash.test(c,1) = Distro_1.location(l).daytype(w).daytime(d).interp(rand(1));
    %             end
    %             figure
    %             histogram(trash.test, 42*60)
    %             figure
    %             plot(trash.interp.Values)
            % if no data exists
            else
                Distro_1.location(l).daytype(w).daytime(d).idle = @(x) 0;
            end
        end


    end
    
    %% Track progress                                                      
    counter = counter + 1;
    show_progress(counter, 1, 24);

end
    
%% Create Matrices                                                         
for l=1:numel(Location)
    for w=1:4
        for s=1:numel(Location(l).charger_el)
            for d=1:24
                Trash2.amount_mat{d,w,s,l} = Distro_1.location(l).daytype(w).daytime(d).scale(s).new_lim_amount_m;
                Trash2.exp_amount_mat(d,w,s,l) = Distro_1.location(l).daytype(w).daytime(d).scale(s).new_exp_amount_m;
            end
        end
    end
    Distro_1.pc_mat = sum(Distro_1.pc_mat_dt(:,2:end),2);
    Distro_1.pc_mat_norm = Distro_1.pc_mat/sum(Distro_1.pc_mat);
    Distro_1.cc_mat = sum(Distro_1.cc_mat_dt(:,2:end),2);
    Distro_1.cc_mat_norm = Distro_1.cc_mat/sum(Distro_1.cc_mat);
    Trash2.pc_mat_norm(:,l) = Distro_1.pc_mat_norm;
    Trash2.cc_mat_norm(:,l) = Distro_1.cc_mat_norm;
    Distro_1.location(l).max_scale = numel(Location(l).charger_el);
end
        
%% Get general info                                                        
for w=1:4
    for s=1:255
        Distro_1.daytype(w).scale(s).amount_per_day = sum(Trash2.exp_amount_mat(:,w,s));
    end
end

% Location name
for l=1:numel(Location)
    Distro_1.location(l).name = Location(l).name;
end

%% Finish                                                                  
set(0,'DefaultFigureVisible','on');

end

%% Check amount distro                                                     
% for d=1:24
%     Weekend(d) = sum(prod(Distro.weekend_dt(d).amount,2));
%     Weekday(d) = sum(prod(Distro.weekday_dt(d).amount,2));
% end
% figure
% hold on
% plot(Weekend)
% plot(Weekend*5/2)
% plot(Weekday)
% hold off
% legend('Weekend', 'Weekend-scaled', 'weekday')
