function [D_Out] = discr_power(Input, MINUTES)

if exist('MINUTES')~=1
    MINUTES = 1;
end

DATA_CHANGE = MINUTES/minutes(Input.datetime_step);

% Example data
% Input = Test_simu.actual_power;

% start at full hour
trash.min_off = minute(Input.datetime(1));
Input.data = [zeros(trash.min_off ,1); Input.data];
Input.nu_of_rows = numel(Input.data);


% reduce resolution
trash.missing = DATA_CHANGE-mod(Input.nu_of_rows, DATA_CHANGE);
if trash.missing==1
    trash.zeros=[]; 
    trash.missing=0;
else 
    trash.zeros = zeros(trash.missing ,1);
end
Input.data = [Input.data; trash.zeros];
Input.data = mean(reshape(Input.data, DATA_CHANGE, []),1)';
Input.nu_of_rows = numel(Input.data);
% Input.datetime = [Input.datetime; [Input.datetime(end)+minutes(MINUTES):minutes(MINUTES):Input.datetime(end)+minutes(MINUTES)*trash.missing]'];
Input.width = minutes(MINUTES);


Input.datetime = [Input.datetime(1)-minutes(trash.min_off):minutes(MINUTES):Input.datetime(end)+minutes(trash.missing)]';
Input.datenum = [datenum(Input.datetime(1)):MINUTES/60/24:datenum(Input.datetime(end))]';

% pass data
D_Out.sum= Input.data;
D_Out.mean= Input.data;
D_Out.amount= ones(Input.nu_of_rows, 1);
D_Out.density= Input.data;
D_Out.nu_of_cols= 1;
D_Out.datetime = Input.datetime;
D_Out.width = minutes(Input.width)/60/24;
D_Out.x= Input.datenum;
D_Out.std_dev = zeros(Input.nu_of_rows, 1);

end

