% _________________________________________________________________________
% =========================================================================
%              Generate a Power profile for EV charging station
% =========================================================================
% 09.11.2019                                                    Linus Kemme
%
% Generates a power profile for an EV charging station for a chosen
% location, resolution and number of days.
% Uses the distribution matrixes in "distro_1.mat" for the probabilities of
% the occurance of EV charging processes
%
% contact linus.kemme@uol.de for further information



%% -------------------------- Clean ---------------------------------------
clear
close all
clc

set(0,'DefaultFigureVisible','off');



%% ------------------------- Settings -------------------------------------

% YOU CAN CHANGE THIS:
% ------------------------------

SUMMERTIME = true;              % Save profile with summertime switch
SAVE_UNIT = 'MW';               % unit of power profile (W, kW, MW)
SAVE_PATH =  'Results/';        % for generated profiles
SAVE_SIMU = false;              % Save whole simulated values?

% ------------------------------
% .

% Get input:
dlg.fields = {"Duration [Days] (>30)"; "Resolution [min] (1-60)"; ["Scale Factor n (int, 1-300)";" n * #EV in 2018"]; ...
    ["Location (1-6)";" 1: Office";" 2: Office & Home";" 3: City & Home";" 4: Home";" 5: Home & Shop";" 6: All"];...
    "Multiplexing? (0/1)"; "Summertime? (0/1)"; "Number of profiles"; "Save profile? (0/1)"};
dlg.title = 'Select Parameters';
dlg.default = {'30', '15', '10', '1', '0', '1', '1', '1'};                  % default values for user input
Input = inputdlg(dlg.fields, dlg.title, [1 40], dlg.default);

MULTIPLEXING = str2num(Input{5});
SUMMERTIME = str2num(Input{6});
AMOUNT = str2num(Input{7});
SAVE = str2num(Input{8});

Gen.duration = str2num(Input{1});                                                           % [days] At least 30 days!!!
Gen.resolution_m = str2num(Input{2});                                                       % [minutes]
Gen.scale_factor = str2num(Input{3});                                                       % [*current amount of ev]
Gen.Location = str2num(Input{4});
Gen.year = '2010';                                                                          % String, not relevant

% Add folders
addpath('./Data/Statistics')
addpath(genpath('./Functions'))

% load
BA_Test_constants

if ~exist('Distro_1')
    load Distro_1
end



%% Iterate through amount                                                  
for counter = 1:AMOUNT
    
    close all
    
    
    %% ---------------- Generate the Power profile ------------------------

    disp(['Profile Number ', num2str(counter)])
    
    S=Gen.scale_factor;
    Gen.Loc_name = Distro_1.location(Gen.Location).name;
    
    % Create /simulate profile
    [Simu, error] = get_rnd_profile_m(Gen.duration, Gen.resolution_m, Gen.scale_factor, Distro_1, Gen.Location, Gen.year, MULTIPLEXING);
    
    % Info
    Simu.info.scale_factor = Gen.scale_factor;
    Simu.info.duration = Gen.duration;
    Simu.info.Location = Gen.Location;
    Simu.info.resolution_m = Gen.resolution_m;
    
    %% Convert to summertime                                               
    if SUMMERTIME
        Simu.actual_power.datetime.TimeZone = 'Europe/Berlin';
        Simu.actual_power.datenum = datenum(Simu.actual_power.datetime);
    end
    
    
    
    %% ---------------------- Save profile --------------------------------
    
    if SAVE
    
    % get name
    trash.loc_name = strrep(Gen.Loc_name, ' ', '');
    trash.loc_name = strrep(trash.loc_name, '&', '+');
    if MULTIPLEXING
        trash.folder_name = [trash.loc_name, '-scale', num2str(Gen.scale_factor), '-', num2str(Gen.resolution_m) 'min-MULTIPLEX-', num2str(Gen.duration), 'd-', date];
    else
        trash.folder_name = [trash.loc_name, '-scale', num2str(Gen.scale_factor), '-', num2str(Gen.resolution_m) 'min-', num2str(Gen.duration), 'd-', date];
    end
    
    old_location = cd(SAVE_PATH);
    mkdir(trash.folder_name)
    cd(trash.folder_name)
    
    % get number
    NUMBER = num2str(next_file_number());
    
    trash.save_name = [trash.folder_name, '_', NUMBER, '.csv'];
    
        disp('Save profile ...')
        
        
        % Convert unit
        if strcmp(SAVE_UNIT, 'MW')
            trash.unit_factor = 0.001;
        elseif strcmp(SAVE_UNIT, 'kW')
            trash.unit_factor = 1;
        elseif strcmp(SAVE_UNIT, 'W')
            trash.unit_factor = 1000;
        end
        
        trash.save_data =  Simu.actual_power.data(1:numel(Simu.actual_power.datetime(:)))*trash.unit_factor;
        
        % save
        fid = fopen(trash.save_name, 'w') ;
        for d = 1:numel(trash.save_data(:))
            fprintf(fid, '%s;', datestr(Simu.actual_power.datetime(d),'YYYY-mm-DD hh:MM:ss'));
            fprintf(fid, '%12.6f \n', trash.save_data(d)) ;
            show_progress(d,5000, numel(Simu.actual_power.data(:)) )
        end
        fclose(fid);
        
        % Save whole simulation
        if SAVE_SIMU
            save([trash.folder_name, '_', NUMBER, '.mat'], 'Test_simu')
        end
        
        cd ..
        cd(old_location)
        
        disp('done')
        
    else
        
        NUMBER = 1;
        
    end
    
    
    
    %% --------------- Validate generated profile -------------------------
    set(0,'DefaultFigureVisible','on');
    
    %% Preparation                                                         
    
    % Annotation box constants
    trash.string = {'Simulation:', ['Skalierung : ' num2str(S)], ['Standort: ' Gen.Loc_name], ['Dauer: ' num2str(Gen.duration) ' Tage']};
    trash.dim = [.75 .65 .15 .15];
    
    % discretize
    trash.d_res = 1/24;
    Simu.D_h = discretize_struct_fast(Simu.data(:,3), Simu.data, trash.d_res, 0, 'days');
    
    % get distribution
    Simu_distro = get_simu_distro(Simu, Gen.scale_factor, Gen.Location);
    
    % write utilization to annotation string
    trash.string = {trash.string{:}, ['Auslastung: ', num2str(Simu.info.max_occupation_perc,3), ' %']};
    
    % Get expected amount distro in matrix
    for w=1:4
        for d=1:24
            Intern.distro.new_exp_amount(d,w) = Distro_1.location(Gen.Location).daytype(w).daytime(d).scale(S).new_exp_amount_m;
            Intern.distro.new_exp_amount_2(d,w) = sum([0:120]' .* Distro_1.location(Gen.Location).daytype(w).daytime(d).scale(S).new_lim_amount_m);
        end
    end   
    
    set(0,'DefaultFigureVisible','on');
    
    %% Plot actual profile                                                 
    
    % total
    figure
    hold on
    plot(Simu.actual_power.datetime, Simu.actual_power.data, 'Color', Color_matrix{4}, 'Linewidth', 4)
    hold off
    xlabel('Start Time ')
    ylabel('Power [kW]')
    title(['Simulated power profile_', num2str(NUMBER)])
    trash.dim3 = [fig_pos_x(0.78,1,0) fig_pos_y(0.78,1,0) 0.15 0.15];
    a=annotation('textbox', trash.dim3,'String',trash.string,'FitBoxToText','on', 'Backgroundcolor', 'w', 'Edgecolor', Color_matrix{8});
    a.Color = Color_matrix{8};
    a.FontSize = 16;
    a.LineWidth = 2;
    set_color(6)
    
    %% Plot Week mean                                                      
    
    % get week profile
    Intern.week_simu.exp_amount = [Simu_distro.exp_amount_mat(:,1); ...
        Simu_distro.exp_amount_mat(:,2); Simu_distro.exp_amount_mat(:,3)];
    Intern.week_distro.exp_amount = Intern.distro.new_exp_amount(1:24*3)';
    
    % Validierung der Profile
    figure('Units', 'normalized', 'OuterPosition', [0 0 1 1])
    
    hold on
    plot([0:24*3-1]', Intern.week_distro.exp_amount, 'Color', Color_matrix{6}, 'Linewidth', 4)
    plot([0:24*3-1]', Intern.week_simu.exp_amount, 'Color', Color_matrix{4}, 'Linewidth', 4)
    plot([0:24*3-1]', Simu.info.rejections_per_dt_w(:), 'Color', Color_matrix{3}, 'Linewidth', 4)
    plot([0:24*3-1]', Simu.info.rejections_per_dt_w(:)+Intern.week_simu.exp_amount, '--', 'Color', Color_matrix{1}, 'Linewidth', 2)
    hold off
    
    set_legend('Erwartung', 'Simulation', 'Ablehnungen', 'Sim + Abl')
    ylabel('Mittlere #LV pro Tag', 'Fontweight', 'bold')
    title(['Validierung der Anzahl an LV in generiertem Profil_', num2str(NUMBER)])
    set_color(6)
    set_daytype_label([],8)
    
    % annotation box
    trash.dim2 = [fig_pos_x(0.04,1,0) fig_pos_y(0.8,1,0) 0.15 0.15];
    a=annotation('textbox',trash.dim2,'String',trash.string,'FitBoxToText','on', 'Backgroundcolor', 'w');
    a.Color = Color_matrix{8};
    a.EdgeColor = Color_matrix{8};
    a.FontSize = 16;
    a.LineWidth = 2;
    
    % Add total mean annotations
    trash.ca = gca;
    trash.distance = max(trash.ca.YLim)/6;
    trash.shift = 0;
    for c=1:3
        % expected amount per day
        trash.string2 = [num2str(sum(Intern.distro.new_exp_amount(:,c)),2) ' LV/Tag'];
        a=annotation('textbox', [.22+0.23*(c-1) 0.18+trash.shift .1 .1], 'String', trash.string2, 'FitBoxToText','on', 'Backgroundcolor', 'w', 'Edgecolor', Color_matrix{6});
        a.Color = Color_matrix{8};
        a.FontSize = 16;
        a.LineWidth = 2;
        
        % simulated amount per day
        trash.string2 = [num2str(Simu.info.daytype(c).mean_amount,2) ' LV/Tag'];
        a=annotation('textbox', [.22+0.23*(c-1) 0.12+trash.shift .1 .1], 'String', trash.string2, 'FitBoxToText','on', 'Backgroundcolor', 'w', 'Edgecolor', Color_matrix{4});    a.Color = Color_matrix{8};
        a.FontSize = 16;
        a.LineWidth = 2;
        % rejected amount per day
        trash.string2 = [num2str(Simu.info.rejections_per_day_w(c),2) ' LV/Tag'];
        a=annotation('textbox', [.22+0.23*(c-1) 0.06+trash.shift .1 .1], 'String', trash.string2, 'FitBoxToText','on', 'Backgroundcolor', 'w', 'Edgecolor', Color_matrix{3});    a.Color = Color_matrix{8};
        a.FontSize = 16;
        a.LineWidth = 2;
        
    end
    
    %% Save graphs                                                         
    if SAVE
        save_figures(1, [SAVE_PATH trash.folder_name], 'keep')
    end
    
    
    
end


%% ===================================================================== %%
