function [Output] = count_same_values_2D(input, resolution, Start_integer, End_integer)
% creates (nx2) matrix , indicating occurance of every number (n) in (mx1) input array

%% example data:
% input=[Simu.data(:,16), Simu.data(:,17)];
% resolution = [1,1];
% Start_integer = [1 0];
% End_integer = [3 23];
% input=[1 2 2 2 1 4; 0 0.3 0.1 0.3 0 0]'; resolution = [1,0.1];
% input = Good.data(:,[7,8]);
% resolution = [0.1, 60];
% input =Charger(c).data(:,[7,8]);
% resolution = [0.1, 60];
% input =[data{1}, data{2}];
% resolution = [1,1];

%% add start and end if exist
if exist('Start_integer') && numel(Start_integer) == 2
    input = [input; Start_integer];
end
if exist('End_integer') && numel(End_integer) == 2
    input = [input; End_integer];
end

%% round
rounded = round(input./repmat(resolution, numel(input(:,1)),1)).*repmat(resolution, numel(input(:,1)),1);

%% Headers
Output.row_values = [min(rounded(:,1)):resolution(1):max(rounded(:,1))]';
Output.col_values = min(rounded(:,2)):resolution(2):max(rounded(:,2));

%% Matrix
nu_of_rows = round(numel(Output.row_values));
nu_of_cols = round(numel(Output.col_values));
Output.matrix = zeros(nu_of_rows, nu_of_cols);

%% First Dimension
first_d_occurance = count_same_values(rounded(:,1), resolution(1));

% second dimension
for c=1:nu_of_rows
    row_elements{c} = rounded(rounded(:,1)==first_d_occurance(c,1),2);
    full_row_elements{c} = [row_elements{c}; min(rounded(:,2));max(rounded(:,2))];
    second_d_occurance = count_same_values(full_row_elements{c}, resolution(2));
    second_d_occurance([1,end],2) = second_d_occurance([1,end],2)-1;
    Output.matrix(c,:) = second_d_occurance(:,2)';
end

%% Remove start and end ints
if exist('Start_integer') && numel(Start_integer) == 2
    x = find(Output.row_values==Start_integer(1));
    y = find(Output.col_values==Start_integer(2));
    Output.matrix(x,y) = Output.matrix(x,y)-1;
end
if exist('End_integer') && numel(End_integer) == 2
    x = find(Output.row_values==End_integer(1));
    y = find(Output.col_values==End_integer(2));
    Output.matrix(x,y) = Output.matrix(x,y)-1;
end


end








