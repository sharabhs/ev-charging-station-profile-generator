function [] = show_progress_par(period, amount)
    persistent c 
    if isempty(c)
        c = 0;
    end
    c=c+1;
    if round(c/period)==c/period
        percentage = c/amount*100;
        t=toc;
        disp(['Second ', num2str(t,4), ', Progress: ', num2str(percentage,3), ' %.'])
    end

end

