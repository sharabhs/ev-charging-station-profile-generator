function [ output_args ] = my_nansum( input_args, dim )
% copy nansum (ignore nan elements)

s=input_args;
s(isnan(s)) = 0;
output_args = sum(s, dim);


end

