function [Simu_Distro] = get_simu_distro(Input, ScaleFactor, Loc)
%% Get Distribution of Pcs, #LVs, Energy and Duration for WD, Sa & Su      
disp('Get distribution...')
set(0,'DefaultFigureVisible','off');

%% Constants                                                               
BA_Test_constants
Trash1.e_res = 1;                                                          % [kWh]
Trash1.d_res = 60;                                                         % [s]

%% example data    
% Input = Test_simu(S);
% Charger = Test_simu(S);
% clear Simu_Distro Trash Intern

%% Get weekday and weekenddays elements                                    
Intern.daytype(1).name = 'Weekday';
Intern.daytype(2).name = 'Saturday';
Intern.daytype(3).name = 'Sunday';
Intern.daytype(4).name = 'Allday';

for w=1:3
Intern.daytype(w).elem = find(Input.data(:,16)==w);
end

Intern.daytype(4).elem = find(Input.data(:,16));


%% Preparation                                                             
Trash2.max_amount=2;

counter = 0;
tic

% Fill discrete array
Intern.full_D_h = fill_discretized_h(Input.D_h);

% Get first hour and daytype of discrete D_h
Intern.dh_start_daytype = weekday(datetime(datestr(Input.D_h.min_x)));
Intern.dh_start_hour = hour(datetime(datestr(Input.D_h.min_x)));

% Get weekday and daytime elements of discrete D_h
for d=1:24
   Intern.dh_elem{d,1} = find((weekday(Input.D_h.datetime)==2 | ...
                              weekday(Input.D_h.datetime)==3 |...
                              weekday(Input.D_h.datetime)==4 |...
                              weekday(Input.D_h.datetime)==5 |...
                              weekday(Input.D_h.datetime)==6) &...
                              hour(Input.D_h.datetime) == d-1);
   Intern.dh_elem{d,2} = find(sum(weekday(Input.D_h.datetime)==7,2) & hour(Input.D_h.datetime) == d-1);
   Intern.dh_elem{d,3} = find(sum(weekday(Input.D_h.datetime)==1,2) & hour(Input.D_h.datetime) == d-1);
   Intern.dh_elem{d,4} = find(hour(Input.D_h.datetime) == d-1);
end

% Power class elements
for p=1:Power_classes.nu_of_classes
    Power_classes.elements{p} = find(Input.data(:,12)==p);
end

%% Loop through daytime                                                    
for d = 1:24

    %% Get daytime elements                                                
    Simu_Distro.elem_dt{d} = find(Input.data(:,17)==d-1);

    %% Power class distribution                                            
    Simu_Distro.pc_mat_dt(:,[1,d+1]) = count_same_integers(Input.data(Simu_Distro.elem_dt{d},12), 1, Power_classes.nu_of_classes);
    % Normalize distribution           
    Simu_Distro.pc_mat_dt_norm = ...
        [Simu_Distro.pc_mat_dt(:,1), Simu_Distro.pc_mat_dt(:,2:end)./...
        repmat(sum(Simu_Distro.pc_mat_dt(:,2:end)),Power_classes.nu_of_classes,1)];

    %% Charge case distribution                                            
    Simu_Distro.cc_mat_dt(:,[1,d+1]) = count_same_integers(Input.data(Simu_Distro.elem_dt{d},13), 1, 5);
    % Normalize distribution                                          
    Simu_Distro.cc_mat_dt_norm = ...
        [Simu_Distro.cc_mat_dt(:,1), Simu_Distro.cc_mat_dt(:,2:end)./...
        repmat(sum(Simu_Distro.cc_mat_dt(:,2:end)),5,1)];

%% Energy & Duration Distribution                                      

    % loop through power classes 
    for p=1:Power_classes.nu_of_classes
        
        Power_classes.elements{p} = find(Input.data(:,12)==p);

        % Get Input elements of this daytime and power class combination
        Trash(d,p).elm = get_same_elements(Simu_Distro.elem_dt{d}, Power_classes.elements{p});

        % Energy (7) & Actual duration (15, rounded to min)  or measured duration (8)
        Trash(d,p).e = Input.data(Trash(d,p).elm,7);                                % kWh
        Trash(d,p).d = Input.data(Trash(d,p).elm,15);                               % s

        % If LVs exist in this combination
        if numel(Trash(d,p).elm)>0
            % get amount of bins for histogram
            Trash(d,p).hist_amount = round(max([(max(Trash(d,p).e)-min(Trash(d,p).e))/Trash1.e_res, ...
                (max(Trash(d,p).d)-min(Trash(d,p).d))/Trash1.d_res]));
            % have at least one bin
            if  Trash(d,p).hist_amount==0  Trash(d,p).hist_amount=1; end
            % create 3D distribution with histogram 
            Trash(d,p).hist=histogram2(Trash(d,p).e, Trash(d,p).d, Trash(d,p).hist_amount);
            Simu_Distro.daytime(d).power(p).e_d_distr = Trash(d,p).hist.Values;
            % get X and Y vectors
            Simu_Distro.daytime(d).power(p).d = linspace(min(Trash(d,p).d), max(Trash(d,p).d), numel(Simu_Distro.daytime(d).power(p).e_d_distr(1,:)));
            Simu_Distro.daytime(d).power(p).e = linspace(min(Trash(d,p).e), max(Trash(d,p).e), numel(Simu_Distro.daytime(d).power(p).e_d_distr(:,1)));
        % If this combination doesnt exist -> zero distr.
        else
            Simu_Distro.daytime(d).power(p).e_d_distr = 0;
            Simu_Distro.daytime(d).power(p).e = 0;
            Simu_Distro.daytime(d).power(p).d = 0;
        end

    end
  
    %% Loop Through daytype -> amount distribution                         
    for w = 1:4
        %% Get Daytype & - time & Location elements                    
        Simu_Distro.daytype(w).elem_dt{d} = find(Input.data(Intern.daytype(w).elem,17)==d-1);
        
        %% Scaled Amount Distribution           
        Trash2.am_dis = count_same_integers(Input.D_h.amount(Intern.dh_elem{d,w}), 0, 2);
        Simu_Distro.daytype(w).daytime(d).lim_amount = Trash2.am_dis(:,2)/sum(Trash2.am_dis(:,2));
        Simu_Distro.daytype(w).daytime(d).exp_amount  = ...
            sum(prod([[0:numel(Trash2.am_dis(:,1))-1]', Simu_Distro.daytype(w).daytime(d).lim_amount],2));

    end
    
    %% Track progress                                                      
    counter = counter + 1;
    show_progress(counter, 1, 24);

end
    
%% Create Matrices        
for w=1:4
    for d=1:24
        Simu_Distro.exp_amount_mat(d,w) = sum([0:numel(Simu_Distro.daytype(w).daytime(d).lim_amount)-1]' .* Simu_Distro.daytype(w).daytime(d).lim_amount);
        
    end
end
Simu_Distro.pc_distro = sum(Simu_Distro.pc_mat_dt(:,2:end),2);
Simu_Distro.pc_distro_norm = Simu_Distro.pc_distro/sum(Simu_Distro.pc_distro);
Simu_Distro.cc_distro = sum(Simu_Distro.cc_mat_dt(:,2:end),2);
Simu_Distro.cc_distro_norm = Simu_Distro.cc_distro/sum(Simu_Distro.cc_distro);

        
%% Get general info                                                        
for w=1:4
    Simu_Distro.daytype(w).amount_per_day = sum(Simu_Distro.exp_amount_mat(:,w));
end

%% Finish                                                                  
set(0,'DefaultFigureVisible','on');

end

%% Check amount distro                                                     
% for d=1:24
%     Weekend(d) = sum(prod(Distro.weekend_dt(d).amount,2));
%     Weekday(d) = sum(prod(Distro.weekday_dt(d).amount,2));
% end
% figure
% hold on
% plot(Weekend)
% plot(Weekend*5/2)
% plot(Weekday)
% hold off
% legend('Weekend', 'Weekend-scaled', 'weekday')
