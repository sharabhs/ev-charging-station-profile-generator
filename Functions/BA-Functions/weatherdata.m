%% change datetime Weatherdata

PF_TIME_FORMAT = 'yyyy-mm-dd HH:MM:SS';
PF_TIME_FORMAT2 = 'yyyy-MM-dd HH:mm:SS';
% old_location = cd('../../PV/linus/raw');
old_location = cd('../../PV/Weather_Data');
clear trash
File = dir;
File = File(3:end);
for c=1:numel(File)
    trash.isdir(c,1) = File(c).isdir;
end
File = File(~trash.isdir);

uiimport(File.name)


Time = strrep(Datetime, 'T', ' ');
for c=1:numel(Time)
    Time{c} = Time{c}(1:19);
end


cd(old_location)


%% Filter bad: 
Datetime_T = Time;
Datetime_I = Time;

% Temp
a=find(Datetime_T == datetime('2015-06-17 12:37:00'));
TemdegC(a) = (TemdegC(a-1)+TemdegC(a+1))/2;
TemdegC(a+2) = (TemdegC(a+1)+TemdegC(a+3))/2;

b= find(Datetime_T == datetime('2015-12-08 15:15:00'));
TemdegC(b:b+18) = [];
Datetime_T(b:b+18) = [];

b= find(Datetime_T == datetime('2015-06-17 12:40:00'));
TemdegC(b) = [];
Datetime_T(b) = [];

b= find(Datetime_T == datetime('2015-09-08 13:49:00'));
TemdegC(b) = [];
Datetime_T(b) = [];


c = find(isnan(TemdegC));
TemdegC(c) = [];
Datetime_T(c) = [];

c = find(TemdegC<=-40);
TemdegC(c) = [];
Datetime_T(c) = [];

% IRradiance
a=find(isnan(GHIUTC));
GHIUTC(a) = [];
Datetime_I(a) = [];


%% Show results
Datetime = datetime(Time, 'Format', PF_TIME_FORMAT2);
Datenum = datenum(Datetime);
Minutes = Datenum*24*60-Datenum(1)*24*60;
Datenum_T = datenum(Datetime_T);
Minutes_T = Datenum_T*24*60-Datenum_T(1)*24*60;
Datenum_I = datenum(Datetime_I);
Minutes_I = Datenum_I*24*60-Datenum_I(1)*24*60;

figure
plot(Datetime, [0:numel(Time)-1])
figure
plot(Datenum_I, GHIUTC)

figure
plot(Datetime, GHIUTC)
figure
plot(Datetime, TemdegC)

figure
plot(Datenum_T, TemdegC)
ylabel('Temp [�C]')

%% Interpolate
Temp = griddedInterpolant(Minutes_T, TemdegC);
Temperature = Temp(min(Minutes):max(Minutes));

figure
plot(Datetime, Temperature)

Irr = griddedInterpolant(Minutes_I, GHIUTC);
Irradiance = Irr(min(Minutes):max(Minutes));

figure
plot(Datetime, Irradiance)

d=diff(Datetime);
max(d)
min(d)
%% Convert time
if SUMMERTIME
   Datetime.Time
   
end

Time = datestr(Datetime, PF_TIME_FORMAT);

%% Avoid negative data
min(Irradiance)
Irradiance = Irradiance - min(Irradiance);
min(Irradiance)

 

%% save


tic
disp('Save profile ...')
trash.save_name = ['weather-data-2015-oldenburg.csv'];

for c=1:numel(Time(:,1))
    Time(c,4) = '0';
end
Time(numel(Datetime)+1:end,:)=[];
numel(Time(:,1))
Time(end,:) = [];

fid = fopen(trash.save_name, 'w') ;
for d = 1:numel(Time(:,1)) % Loop through each time/value row
    fprintf(fid, '%s,', Time(d,:)); % Print the time string
    fprintf(fid, '%12.6f,', double(Irradiance(d))) ; % Print the irradiance data
    fprintf(fid, '%12.6f \n', double(Temperature(d))) ; % Print the temperature data
    show_progress(d,5000,numel(Time(:,1)))
end
fclose(fid);


max(d) 
min(d)

cd ../../Datenverarbeitung/Skripte