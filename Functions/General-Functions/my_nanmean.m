function [ output_args ] = my_nanmean( input_args, dim )
% copy nanmean (ignore nan elements)
nu_of_nonnan = sum(~isnan(input_args), dim);
output_args = my_nansum(input_args, dim)./nu_of_nonnan;
% if all elments along that dimension are NaN: keep it nan

end

