function [amount] = count_same_integers(Input, Start_integer, End_integer, Steps)
% creates (nx2) matrix , indicating occurance of every number (n) in (mx1) input array

%% Example data
% input = Intern.case;

%% Count
% row vector:
Input = Input(:);

% check 
if find(round(Input)~=Input)>0
    display('ERROR: only integers allowed!')
end

% add start and end if exist
if exist('Start_integer') && numel(Start_integer) == 1
    Input = [Input; Start_integer];
end
if exist('End_integer') && numel(End_integer) == 1
    Input = [Input; End_integer];
end
% init output vector
amount = [[min(Input):max(Input)]' [zeros(max(Input)-min(Input)+1,1)]];

% sort
Input = Input(:)';
sorted = sort(Input);

% add value for diff
sorted_plus = [sorted max(sorted)+1];

% get indices of incremented values
[~, elements,~] = unique(sorted_plus);

% get occurance of existing values
difference = diff(elements);

% 
% allocate amount to value
amount(sorted(elements(1:end-1))-min(sorted)+1,2) = difference;

% remove start and end value
if exist('Start_integer') && numel(Start_integer) == 1
    amount(Start_integer-min(amount(:,1))+1,2) = amount(Start_integer-min(amount(:,1))+1,2)-1;
end
if exist('End_integer') && numel(End_integer) == 1
    amount(End_integer-min(amount(:,1))+1,2) = amount(End_integer-min(amount(:,1))+1,2)-1;
end

% consider only Steps
if exist('Steps')
    amount = amount(1:Steps:end,:);
end

end

