function [] = save_figures_II(FigH)
    disp('Saving figure ...')
    h1=get(gca,'title');
    Title=get(h1,'string');
    Title = strrep(Title, ' ', '-');
    Title = strrep(Title, ',', '-');
    Title = strrep(Title, ':', '-');
    F  = getframe(FigH);
    imwrite(F.cdata, [Title, '.png'], 'png')
%         imwrite(gf.cdata, [Title, '.jpg'], 'jpg')
    close

end

