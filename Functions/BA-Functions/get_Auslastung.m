%% Measure Utilization per Scale factor

%% Settings
LOCATION = 4;
DURATION = 300;
echo off
MULTIPLEXING = false;
BA_Test_constants
C = 1:100;
% C=21;

if ~exist('Distro_1')
    load Distro_1
end



%% Calculate Utilization
for c=C
    Trash = get_rnd_profile_m(DURATION, 60, c, Distro_1, LOCATION, '2010', MULTIPLEXING);
    Test_simu(c) = Trash;
    clear Trash
    Loading(c,1) = Test_simu(c).info.max_occupation_perc;
    Loading_h(c,1)= 100*max(Test_simu(c).info.mean_connected_hour_w)/2;
    
    show_progress(c,1,200)
    
end

Loading(Loading==0)=[];

for c=C
    Cars(c,1) =  Test_simu(c).info.nu_of_LVs/DURATION;
    Rejections(c,1) = Test_simu(c).info.rejections_per_day;
    Requests(c,1) = mean([5/7*sum(Test_simu(c).info.mean_requested_hour_w(1:24)) + ...
        sum(Test_simu(c).info.mean_requested_hour_w(25:24*2))/7 + ...
        sum(Test_simu(c).info.mean_requested_hour_w(24*2+1:end))/7]);
end
Cars(Cars==0)=[];


%% Expected amount
trash.exp = 0;
for d=1:24
    trash.exp = trash.exp + Distro_1.location(LOCATION).daytype(4).daytime(d).scale(1).new_exp_amount_m;
end

trash.req = linspace(trash.exp  , C(end)*trash.exp, numel(C))';

%% Plot results

figure
hold on
plot(Loading, 'LineWidth', 4, 'Color', Color_matrix{4})
plot(C, Cars, 'LineWidth', 4, 'Color', Color_matrix{1})
% plot(C, trash.req, 'LineWidth', 4, 'Color', Color_matrix{3})
plot(C, Rejections, 'LineWidth', 4, 'Color', Color_matrix{7})
plot(C, Requests, 'LineWidth', 4, 'Color', Color_matrix{3})
[a,b] = max(Cars-Rejections);
b=21;
s=scatter(C(b),  Loading(b),200,'x', 'LineWidth', 6, 'MarkerEdgeColor', Color_matrix{5});
plot([C(b) C(b)], [0 Loading(b)], '--','LineWidth', 4, 'Color', Color_matrix{5})
hold off
Rejections(b)
Cars(b)
l=legend('Auslastung', 'Angenommene Autos pro Tag',  'Abgewiesene Autos pro Tag', ...
    'Anfragen', ['Optimum, ', num2str(Loading(b),3), ' %, S = ', num2str(b)], 'Location', 'Northwest');
l.EdgeColor = Color_matrix{8};
l.TextColor = Color_matrix{8};

xlabel('Skalierfaktor S')
ylabel('Auslastung [%] | #Fahrzeuge')
title('Auslastung von Lades�ulen Standort Home')
set_color(6)

%% Save results
cd Results
mkdir Auslastung
cd Auslastung
Auslastung.requests = Requests;
Auslastung.rejections = Rejections;
Auslastung.Acceptions = Cars;
Auslastung.Loading = Loading;
save('Auslastung_1-100.mat', 'Auslastung')
cd ..\..

%% reload
cd Results\Auslastung\
load Auslastung_1-21.mat
cd ..\..
figure
plot(Auslastung.Acceptions)



% Multiplex results fit Auslastung:
% General model:
%      f(x) = s-a*exp(-b*x)
% Coefficients (with 95% confidence bounds):
%        a =       176.5  (169.5, 183.4)
%        b =      0.0466  (0.04217, 0.05103)
%        s =       171.8  (167.9, 175.7)
% 
% Goodness of fit:
%   SSE: 5026
%   R-square: 0.9712
%   Adjusted R-square: 0.9706
%   RMSE: 7.645
