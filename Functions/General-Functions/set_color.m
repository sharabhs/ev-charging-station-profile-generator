function [] = set_color(Size)
%% Set color and designs for current figure

    % Constants
    if ~exist('Size')
        Size = 6;
    end
    GCA = gca;
    BA_Test_constants
    
    % Design
    set(get(GCA,'Title'),'Color',Color_matrix{8})
    set(GCA, 'FontSize', 10+Size*2, 'FontWeight', 'normal', 'XColor', Color_matrix{8}, ...
        'YColor', Color_matrix{8}, 'ZColor', Color_matrix{8}, 'GridColor', Color_matrix{8},...
        'MinorGridColor', Color_matrix{8})
    set(gcf, 'color', 'w')
    set(get(GCA, 'XAxis'), 'LineWidth', 0.4*Size)
    set(get(GCA, 'YAxis'), 'LineWidth', 0.4*Size)
    set(GCA, 'LineWidth', Size*0.4)
    set(GCA, 'Box', 'on')
%     set(get(GCA, 'XLabel'), 'FontWeight', 'bold')
%     set(get(GCA, 'YLabel'), 'FontWeight', 'bold')
    grid on
    
    % Fullscreen
    pause(0.00001);
    frame_h = get(handle(gcf),'JavaFrame');
    set(frame_h,'Maximized',1);
    
end

