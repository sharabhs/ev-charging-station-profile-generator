%% Commands to optimize plots


set(0,'DefaultFigureVisible','on');

%% Test figure                                                             
close all 
figure('Units', 'normalized', 'OuterPosition', [0 0 1 1])
plot([0:24*3-1]', Intern.week_distro.exp_amount)

%% change XTicks  and Label                                                
clear label
for d=1:4
    label{d} = [num2str((d-1)*6) ':00'];
end
set(gca, 'XTick', 0:6:72, 'XTickLabel', label)

%% YTicks in %:
ytix = get(gca, 'YTick');
set(gca, 'YTick',ytix, 'YTickLabel',ytix*100)

%% Distance Label - Axis
y=ylabel('Auslastung [%]');
y.Position(1) = y.Position(1)*1.1;

%% Axis Limits
set(gca, 'YLim', [0 2])

%% Minor Ticks                                                             
set(gca,'XMinorTick','on','YMinorTick','on')

%% Remove Tick labes
set(gca, 'YTicklabel', '')

%% Minor Grid                                                              
set(gca,'XMinorGrid','on', 'YMinorGrid','on')

%% second xlabel                                                           
text('Units', 'Data', 'Position', [12,-0.003], 'HorizontalAlignment', 'center', 'String', 'Workday')
text('Units', 'Data', 'Position', [12+24,-0.003], 'HorizontalAlignment', 'center', 'String', 'Saturday')
text('Units', 'Data', 'Position', [12+24*2,-0.003], 'HorizontalAlignment', 'center', 'String', 'Sunday')
text('Units', 'Data', 'Position', [0,-0.003], 'HorizontalAlignment', 'center', 'String', '|')
text('Units', 'Data', 'Position', [24*3,-0.003], 'HorizontalAlignment', 'center', 'String', '|')
text('Units', 'Data', 'Position', [24,-0.003], 'HorizontalAlignment', 'center', 'String', '|')
text('Units', 'Data', 'Position', [24*2,-0.003], 'HorizontalAlignment', 'center', 'String', '|')

%% shift xlabel downwards                                                  
xlabel('Daytime')
xlp=get(gca, 'XLabel');
xlp.Position(2) = - 0.004;
set(xlp, 'Position', xlp.Position)

%% Axis Colors
g=gca;
g.XColor = Color_matrix{1};
g.YColor = Color_matrix{1};
set(gca, 'XColor', g.XColor, 'YColor', g.YColor)
xlabel('DDD')
title('DDD', 'Color', Color_matrix{1})

figure
hold on
for d=1:numel(Color_matrix)
    plot([0 1], [d d], 'Color', Color_matrix{d}, 'Linewidth', 40)
end
hold off

%% Colors
Blues.mat = ...
    { [168, 222, 244], [112, 212, 255],  [4, 85, 119], [54, 71, 79], [133, 159, 170],[0, 48, 68], [1, 9, 12]};
Blues.names = {'very light blue', 'light blue', 'intense blue', 'dark grey', 'light grey', 'dark blue', 'black'};

All.mat = {[51, 101, 138],[41, 51, 92],[243, 167, 18],[109, 163, 77],[155, 29, 32]};
All.names = {'blue', 'dark blue', 'yellow', 'light green', 'red'};

    
figure
hold on
for c=1:numel(Blues.mat)
    Blues.mat{c} = Blues.mat{c}/255;
    plot([0 1], [c c], 'Color', Blues.mat{c}, 'Linewidth', 40, 'DisplayName',Blues.names{c})
end
legend('show')
hold off

%% Set of color and format specs
set(gca, 'FontSize', 12, 'OuterPosition', [0 0 1 1], 'XColor', Color_matrix{8}, 'YColor', Color_matrix{8}, 'ZColor', Color_matrix{8})

%% Change legend color
l=legend('show');
l.EdgeColor = Color_matrix{8};
l.TextColor = Color_matrix{8};
