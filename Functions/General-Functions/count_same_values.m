function [amount] = count_same_values(input, resolution)
% creates (nx2) matrix , indicating occurance of every number (n) in (mx1) input array

% Example data
% input = rounded(:,1);
% resolution = resolution(1);
% input = 0.1* [0 1 3 3 3 5];
% resolution = 0.1;

rounded = round(input/resolution)*resolution;

% init output vector
amount = [[min(rounded):resolution:max(rounded)]' [zeros(round((max(rounded)-min(rounded))/resolution)+1,1)]];

% add every value to vector 
rounded_plus = [rounded(:); amount(:,1)];

% sort
sorted = round(sort(rounded_plus)/resolution)*resolution;

% add value for diff
sorted_plus = [sorted; max(sorted)+resolution];

% get indices of incremented values
[~, elements,~] = unique(sorted_plus);

% get amount of values
difference = diff(elements);

% allocate amount to value and substract values previously added
amount(:,2) = difference-1;

end

