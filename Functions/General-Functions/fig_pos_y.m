function [f_pos] = fig_pos_y (data, max_data, Width)
    if ~exist('Width')
        Width=0;
    end
    if ~exist('max_data')
        max_data=1;
    end
    a=0.111;
    b=0.0745;
    f_pos = (1-a-b)/max_data*data+a-Width*a;

end