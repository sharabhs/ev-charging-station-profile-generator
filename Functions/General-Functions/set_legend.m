function [] = set_legend(q,w,e,r,t,z,u,I,o,p)

if ~exist('q')
    q='';
end
if ~exist('w')
    w='';
end
if ~exist('e')
    e='';
end
if ~exist('r')
    r='';
end

if ~exist('t')
    t='';
end

if ~exist('z')
    z='';
end

if ~exist('u')
    u='';
end

if ~exist('I')
    I='';
end

if ~exist('o')
    o='';
end

if ~exist('p')
    p='';
end



l=legend(q,w,e,r,t,z,u,I,o,p);

BA_Test_constants;

l.EdgeColor = Color_matrix{8};
l.TextColor = Color_matrix{8};


end