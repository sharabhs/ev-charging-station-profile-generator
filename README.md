EV charging station power profile generator. This is a fork of the university of oldenbergs project on EV charging station load generator
=============================================================

09.11.2019, Linus Kemme
------------------------------------------------------------
This Matlab script generates a load profile for ev charging stations.

Run BA_generate_profile.m with MATLAB R2018a


------------------------------------------------------------
Enter the following values:

	Duration in days (minimum 30 days)
	
	Resolution in minutes (1-60)
	
	Scale Factor
		This is a measure of the utilization of the charging station according to the amount of ev on the street.
		1 refers to 111,000 evs in Germany in 2018. The Scale Factor simulates a higher share of evs. 
		2 would then refer to 222,000 evs, leading to a higher utilization of the charging station, assuming that the amount of charging stations stays constant. 
		Only integers are allowed.
		
	Location category 
		 1: close to business park
		 2: close to business park & residential area
		 3: close to city center & residential area
		 4: close to residential area
		 5: close to residential area & shops
		 6: all mixed together
		 
	Multiplexing
		= allowing more cars to connect to one charging station. They will then automatically be charged sequentially. 
		
	Summertime
		Add the time switching to the power profiles
		
	Number of profiles
	
	Save	 
		Save the profile and a verification graph in /Results
		Format: Date (YYYY-MM-DD HH:MM:SS);	Power (W, kW or MW)

The unit of the power can be set in the script BA_generate_profile.m in %% Settings

----------------------------------------------------------
The script will then create and save the profiles in /Results. 

The generation is based on measured charging processes of 256 charging stations in north-west germany from 2016-2018.



See /Background or contact linus.kemme@uol.de for further information
