%% test summertime 2 

% Create Summertime compatible file

TIME_FORMAT_S = 'yyyy-MM-dd HH:mm:ss';
TIME_FORMAT_S_Str = 'yyyy-mm-dd HH:MM:SS';
% TIME_FORMAT_S = 'dd.MM.yyyy HH:mm:ss';
% TIME_FORMAT_S_Str = 'dd.mm.yyyy HH:MM:SS';
% time_start = datetime('2010-03-28 00:00:00', 'Format',TIME_FORMAT_S , 'TimeZone', 'UTC+1');
% time_end = datetime('2010-10-31 04:00:00', 'Format', TIME_FORMAT_S, 'TimeZone', 'UTC+1');
% time_start = datetime('2010-03-28 00:00:00', 'Format',TIME_FORMAT_S , 'TimeZone', 'UTC+1');
% time_end = datetime('2010-03-28 04:00:00', 'Format', TIME_FORMAT_S, 'TimeZone', 'UTC+1');
time_start = datetime('2010-10-31 00:00:00', 'Format',TIME_FORMAT_S , 'TimeZone', 'UTC+1');
time_end = datetime('2010-10-31 04:00:00', 'Format', TIME_FORMAT_S, 'TimeZone', 'UTC+1');
time_shift2= datetime('2010-10-31 03:00:00', 'Format', TIME_FORMAT_S, 'TimeZone', 'Europe/Berlin');

Time = [time_start:minutes(10):time_end]';
%% Data
data = sin(datenum(Time)-datenum(time_start))+1;

%% shift

% Time(6*2-10:6*3+10)
% Time(end-6*3:end)
numel(Time);
% Time.TimeZone = 'Europe/Berlin';
% Time(end-6*3:end)
numel(Time);

% Oktober: 2:00 - 2:59, 2:00 - 2:59, 3:00 - ...

% Time(find(Time==time_shift2):end) = Time(find(Time==time_shift2):end)-hours(1);

max(diff(Time));
[a,b]=min(diff(Time));
% Time(1:20)


% figure
% p=plot(Time, data, '.', 'MarkerSize', 10)

%% Save
Time_str = datestr(Time,TIME_FORMAT_S_Str);

old_location = cd('..\..\PV\linus'); 
tic
fid = fopen('summertime_test_UTC_Oct.csv', 'w') ;
for d = 1:numel(Time) % Loop through each time/value row
    fprintf(fid, '%s;', Time_str(d,:)) ; % Print the time string
    fprintf(fid, '%12.6f \n', double(data(d))) ; % Print the data values
    show_progress(d,500,numel(Time))
end
fclose(fid);

cd(old_location)
    