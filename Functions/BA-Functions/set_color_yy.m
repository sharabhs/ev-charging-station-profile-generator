function [] = set_color_yy(Size, yyAx)
    if ~exist('Size')
        Size = 1;
    end
    BA_Test_constants
    for c=1:2
        GCA = yyAx(c);
        set(get(GCA,'Title'),'Color',Color_matrix{8})
        set(GCA, 'FontSize', 10+Size*2, 'FontWeight', 'normal', 'XColor', Color_matrix{8}, ...
            'YColor', Color_matrix{8}, 'ZColor', Color_matrix{8}, 'GridColor', Color_matrix{8},...
            'MinorGridColor', Color_matrix{8})
        set(gcf, 'color', 'w')
        set(get(GCA, 'XAxis'), 'LineWidth', 0.4*Size)
        set(get(GCA, 'YAxis'), 'LineWidth', 0.4*Size)
        set(GCA, 'LineWidth', Size*0.4)
        set(GCA, 'Box', 'on')
        grid on
    end
    
    pause(0.00001);
    frame_h = get(handle(gcf),'JavaFrame');
    set(frame_h,'Maximized',1);
end