%% Check and test distribution of LVs
% 
% Values to distribute:
% - (1) Start Time
% - (2) Power Class
% - (3) Charge Case
% - (4) Charge Amount
% - (5) Duration
% 
% Assumptions:
% - Distribution different per daytime, workday/weekend and season
% - All Chargers similar -> can be mixed
% 
% Unclear:
% - Correlation between Values?
% 
% Suggested Way Forward:
% - Get distribution of (2) to (5) per daytime, devided by amount of chargers
% - distribute LVs along these distributions per daytime
% - distribute start time over hour of daytime
% 
% What I already have:
% - distribution of power classes per daytime
% - distribution of charge cases per daytime
% - distribution function for charge amount
% - distribution function for duration

%% Preparation                                                             
r.start_hour = hour(D.h.datetime(1));
BA_Test_constants
                                     
set(0,'DefaultFigureVisible','on');

%% -------------------------- Power cases ---------------------------------
% goals:
% - plot measured occurance of PCs per daytime
% - create statistical distribution
% - compare both

%% Measured                                                                
% Loop through daytime
for d=1:24
    
    r.daytime = d-r.start_hour+24;
    % power classes of Lvs at this daytime
    r.daytime_pcs = D_Hour.Three_d(:,12,[r.daytime:24:end]);
    Distro.dt(d).nu_of_days = numel(r.daytime_pcs(1,1,:));
    r.daytime_pcs = r.daytime_pcs(:);
    r.daytime_pcs(isnan(r.daytime_pcs)) = [];
    % total number of LVs per power class at this daytime
    Distro.dt(d).pc_total_distro = count_same_integers(r.daytime_pcs, 1, Power_classes.nu_of_classes);
    % nu of LVs averaged over whole time period
    Distro.dt(d).pc_mean_distro = Distro.dt(d).pc_total_distro;
    Distro.dt(d).pc_mean_distro(:,2) = Distro.dt(d).pc_total_distro(:,2) / Distro.dt(d).nu_of_days;
    Distro.day.pc_mean_distro(:,d) = Distro.dt(d).pc_mean_distro(:,2);
    show_progress(d,1,24);
    
end

% BAR plot: Distribution of Power classes per daytime for all chargers
figure
hold on
    bar(Distro.day.pc_mean_distro', 'stacked')
    plot(Daytime.mean_amount)
hold off
legend([Power_classes.names; 'Mean Daytime amount'])  
title('Distribution of Power classes per daytime for all chargers, stacked')
xlabel('Daytime [hour]')
ylabel('Mean #LVs per daytime')
% Result: fits perfectly to daytime distro

% Line plot: Distribution of Power classes per daytime for all chargers
figure
hold on
for c=1:Power_classes.nu_of_classes
   plot(datetime(datestr(Daytime_Classes(c).datenum, CLOCK_FORMAT),'Format', CLOCK_FORMAT_DT), Daytime_Classes(c).mean_amount) 
end
hold off
title('Distribution of Power classes per daytime for all chargers')
xlabel('Daytime [hour]')
ylabel('Mean #LVs per daytime')
legend(Power_classes.names)
% Result: Distribution in the morning a bit different

% plot: relative Distribution of Power classes per daytime
figure
bar((100*Distro.day.pc_mean_distro./repmat(sum(Distro.day.pc_mean_distro,1),Power_classes.nu_of_classes,1))', 'stacked')
legend([Power_classes.names])  
title('Relative distribution of Power classes per daytime')
xlabel('Daytime [hour]')
ylabel('%')
% Result: distribution relatively stable. Could be assumed to not change.
% Change only in the morning (6-10:00)

%% ----------------------- Charge Cases -----------------------------------

%% Measured                                                                
% Loop through daytime
for d=1:24
    r.daytime = d-r.start_hour+24;
    % charge cases of Lvs at this daytime
    r.daytime_ccs = D_Hour.Three_d(:,13,[r.daytime:24:end]);
    Distro.dt(d).nu_of_days = numel(r.daytime_ccs(1,1,:));
    r.daytime_ccs = r.daytime_ccs(:);
    r.daytime_ccs(isnan(r.daytime_ccs)) = [];
    % total number of LVs per charge case at this daytime
    Distro.dt(d).cc_total_distro = count_same_integers(r.daytime_ccs, 1, Good.cases.nu_of_cases);
    % nu of LVs averaged over whole time period
    Distro.dt(d).cc_mean_distro = Distro.dt(d).cc_total_distro;
    Distro.dt(d).cc_mean_distro(:,2) = Distro.dt(d).cc_total_distro(:,2) / Distro.dt(d).nu_of_days;
    Distro.day.cc_mean_distro(:,d) = Distro.dt(d).cc_mean_distro(:,2);
    
end

% BAR plot: Distribution of Charge cases per daytime for all chargers
figure
hold on
    bar(Distro.day.cc_mean_distro', 'stacked')
    plot(Daytime.mean_amount)
hold off
legend([Good.cases.names; 'Mean Daytime amount'])  
title('Distribution of Charge cases per daytime for all chargers')
xlabel('Daytime [hour]')
ylabel('Mean #LVs per daytime')
% Result: fits perfectly to daytime distro


% LINE plot: Distribution of Charge cases per daytime for all chargers  
figure
hold on
for c=1:Good.cases.nu_of_cases
   plot(datetime(datestr(Daytime_Cases(c).datenum, CLOCK_FORMAT),'Format', CLOCK_FORMAT_DT), Daytime_Cases(c).mean_amount, 'LineWidth', 2) 
end
hold off
xlabel('Daytime [hour]')
ylabel('Mean #LVs per daytime')
title('Distribution of Charge cases per daytime for all chargers')
legend(Good.cases.names)
% Result: actually, the distribution differs from daytime to daytime.

% plot: relative Distribution of Charge cases per daytime
figure
bar((100*Distro.day.cc_mean_distro./repmat(sum(Distro.day.cc_mean_distro,1),Good.cases.nu_of_cases,1))', 'stacked')
legend([Good.cases.names])  
title('Relative distribution of Charge cases per daytime')
xlabel('Daytime [hour]')
ylabel('%')
% Result: relativele stable over daytime. Could be assumed to be
% independant of daytime


%% ------------------ Power class & Charge Case Combos --------------------

%% Measured                                                                
% Loop through daytime
for d=1:24
    r.daytime = d-r.start_hour+24;
    % power classes of Lvs at this daytime
    r.daytime_pcs = D.h.Three_d(:,12,[r.daytime:24:end]);
    % Charge cases of LVs at this daytime
    r.daytime_ccs = D.h.Three_d(:,13,[r.daytime:24:end]);
    % Number of days
    Distro.dt(d).nu_of_days = numel(r.daytime_ccs(1,1,:));
    
    r.daytime_pcs = r.daytime_pcs(:);
    r.daytime_pcs(isnan(r.daytime_pcs)) = [];
    r.daytime_ccs = r.daytime_ccs(:);
    r.daytime_ccs(isnan(r.daytime_ccs)) = [];
    
    % total number of LVs per power class at this daytime
    Distro.dt(d).pcc_total_distro = count_same_values_2D([r.daytime_pcs,r.daytime_ccs], [1,1], [1,1], [3,6]);
    
    % nu of LVs averaged over whole time period
    Distro.dt(d).pcc_mean_distro = Distro.dt(d).pcc_total_distro;
    Distro.dt(d).pcc_mean_distro.matrix = Distro.dt(d).pcc_total_distro.matrix / Distro.dt(d).nu_of_days;
    Distro.day.pcc_mean_distro.matrix(:,:,d) = Distro.dt(d).pcc_mean_distro.matrix;
    
end

% from here
d=0;
figure
hold on
for p=1:Power_classes.nu_of_classes
    for c=1:Good.cases.nu_of_cases
        Trash.array = Distro.day.pcc_mean_distro.matrix(p,c,:)/sum(Distro.day.pcc_mean_distro.matrix(p,c,:))*100;
        plot(Trash.array(:), 'Marker', Marker_matrix{c}, 'Color', Color_matrix{p})
        d=d+1;
        Name{d} = [Power_classes.names{p}, ', ' Good.cases.names{c}];
    end
end
hold off
xlabel('Time')
ylabel('Mean distribution of LVs over daytime [%]')
legend(Name)
title('Distribution of charge cases and power classes over daytime for all chargers - Here')
% Result: exactly the same as from cluster. 
% For simplification one can assume that the Pc & Cc distribution is not
% generally changing per daytime -> fixed percentages can be used
% And hard to see clusters/ probability of certain combinations
% except from one example: at 9:00 am: 3.68kW ConstP+Full slope more than twice as
% often as 22 kW in that Cc, although both PC occur similarly often in
% total at 9:00
% idea: calculate simple occurance (P_pc * P_cc) and compare to combined
% occurance

% Comparison Actual charge case & power class combo distribution and multiplication of
% both distributions
for p=1:Power_classes.nu_of_classes
    
    figure('Units', 'normalized', 'OuterPosition', [0 0 1 1])
    hold on
    d=0;
    for c=1:Good.cases.nu_of_cases
        Trash.array = Distro.day.pcc_mean_distro.matrix(p,c,:);
        Trash.array_b = sum(Distro.day.cc_mean_distro(:,:),1).*Distro.day.cc_mean_distro(c,:)./sum(Distro.day.cc_mean_distro(:,:),1) .* Distro.day.pc_mean_distro(p,:)./sum(Distro.day.pc_mean_distro(:,:),1);
        plot(Trash.array(:), 'Color', Color_matrix{c}, 'DisplayName', [Good.cases.names{c}, ', actual'])
        plot(Trash.array_b(:), 'Color', Color_matrix{c},'LineStyle', '--', 'DisplayName', [Good.cases.names{c}, ', simu'])
        d=d+1;
    end
    hold off
    xlabel('Time [h]')
    ylabel('Mean #LVs per daytime')
    legend('show')
    title(['Charge case for ', Power_classes.names{p}, ' over daytime (actual and simulated distro)'])
    
end
% Result: simu quite close to actual
% Except: Cc are actually not distributed over Pc in the same way -> few
% deviations (eg: 22 kW: actually finished slope more often (*2))
% Assumption of no cerrelation can be made

% -> Mean Pc and Cc distribution 
figure
pie(Power_classes.distribution(:,2), strcat(Power_classes.names, ':  ', num2str(Power_classes.distribution(:,2)/Good.info.nu_of_LVs*100, 2),  ' %'))
title('Mean Power Class Distribution')

figure
pie(Good.cases.distribution(:,2), strcat(Good.cases.names, ':  ', num2str(Good.cases.distribution(:,2)/Good.info.nu_of_LVs*100, 2),  ' %'))
title('Mean Charge Case Distribution')


%% ------------------------------ Energy ----------------------------------
%% Measured                                                                
% Total 3D over all days and chargers per daytime
Hour = mod(round(Good.data(:,3)*24),24);
figure
histogram2(Hour, Good.data(:,7))
xlabel('Daytime [h]')
ylabel('Energy [kWh]')
zlabel('Total occurance')
title('Total charge amount distribution per daytime')

% Mean Energy for all cases and classes:
figure
plot(Daytime.mean(:,7), 'LineWidth', 2)
xlabel('Start Daytime [h]')
ylabel('Mean energy [kWh]')
title('Mean charge amount per daytime')
ylim([0, max(Daytime.mean(:,7))*1.2])
% Result: Increasing over night (*1.5)

% Mean Energy per charge case over daytime
figure
hold on
for c=1:Good.cases.nu_of_cases
   plot(datetime(datestr(Daytime_Cases(c).datenum, CLOCK_FORMAT),'Format', CLOCK_FORMAT_DT), Daytime_Cases(c).mean(:,7)) 
end
hold off
xlabel('Start Time')
ylabel('Mean Energy per LV [kWh]')
title('Daily Charged Mean Energy per charging case')
legend(Good.cases.names)
% Results:
% - No change over daytime
% - Except Cons + slope + wait - case: higher energy over night
% - Energy depending on charge case (factor 4)

% Mean Energy per power class over daytime
figure
hold on
for c=1:Power_classes.nu_of_classes
   plot(datetime(datestr(Daytime_Classes(c).datenum, CLOCK_FORMAT),'Format', CLOCK_FORMAT_DT), Daytime_Classes(c).mean(:,7)) 
end
hold off
xlabel('Start Time')
ylabel('Mean Energy per LV [kWh]')
title('Daily Charged Mean Energy per power class')
legend(Power_classes.names)
% Result:
% - Charged Energy increasing with max power (factor 4)
% - Charged Energy higher during night

% For every p and c
figure;bar(Cluster.power(p).case(c).D_energy.x, Cluster.power(p).case(c).D_energy.amount)

% Total questions:
% Distro per Dt&Pc&Cc?? -> 7*5*24 Combinations!
% Correlation between Pc and Cc -> distro?

%% ----------------------------- Duration ---------------------------------
%% Measured with wait time                                                 
% Mean Duration for all cases and classes:
figure
plot(Daytime.mean(:,8)/3600, 'LineWidth', 2)
xlabel('Start Daytime [h]')
ylabel('Mean duration [h]')
title('Mean duration per daytime')
ylim([0, max(Daytime.mean(:,8)/3600)*1.2])
% Result: not changing over daytime!


% Total 3D over all days and chargers per daytime
Hour = mod(round(Good.data(:,3)*24),24);
figure
histogram2(Hour, Good.data(:,8)/3600, [24, round(max(Good.data(:,8)/3600))])
xlabel('Daytime [h]')
ylabel('Duration [h]')
zlabel('Total occurance')
title('Total duration distribution per daytime')
% Result: repeting occurance after n*24h duration
% Changing

%% Measured actual duration                                                
% Mean actual Duration for all cases and classes:
figure
plot(Daytime.mean(:,15)/3600, 'LineWidth', 2)
xlabel('Start Daytime [h]')
ylabel('Mean duration [h]')
title('Mean actual duration per daytime')
ylim([0, max(Daytime.mean(:,15)/3600)*1.2])
% Result: 
% - Changing over daytime:
% - Peak at 7:00-9:00
% - increasing over night


% Total 3D actual duration over all days and chargers per daytime
Hour = mod(round(Good.data(:,3)*24),24);
figure
histogram2(Hour, Good.data(:,15)/3600, [24, round(max(Good.data(:,15)/3600))*4])
xlabel('Daytime [h]')
ylabel('Duration [h]')
zlabel('Total occurance')
title('Total actual duration distribution per daytime')
% Result: 
% - max actual duration = 18 h?
% - no periodic repetition
% - morning: much higher duration
% - nights: higher duration

% Per Cc
figure
hold on
for c=1:Good.cases.nu_of_cases
   plot(datetime(datestr(Daytime_Cases(c).datenum, CLOCK_FORMAT),'Format', CLOCK_FORMAT_DT), Daytime_Cases(c).mean(:,15)/3600) 
end
hold off
xlabel('Start Time')
ylabel('Mean Duration per LV [h]')
title('Daily Mean Actual Duration per charging case')
legend(Good.cases.names)
% - Results:
% - Duration highly depending on charge case
% - Daytime course depending on Cc

% Per Pc
figure
hold on
for c=1:Power_classes.nu_of_classes
   plot(datetime(datestr(Daytime_Classes(c).datenum, CLOCK_FORMAT),'Format', CLOCK_FORMAT_DT), Daytime_Classes(c).mean(:,15)/3600) 
end
hold off
xlabel('Start Time')
ylabel('Mean Duration per LV [h]')
title('Daily Mean Actual Duration per power class')
legend(Power_classes.names)
% Result: 
% - Duration increasing with decreasing max power
% - Duration increasing over night

%% E&D
% Total Actual Distribution over Duration and Energy
figure
histogram2(Good.data(:,15)/3600, Good.data(:,7), round([max(Good.data(:,15)/3600)*60, max(Good.data(:,7))*10]))
xlabel('Duration [h]')
ylabel('Energy [kWh]')
zlabel('Total #LVs')
title('Total Distribution over Duration and Energy')

%% 3D Spline
% one PC:
% energy
DD.e=Good.data(Power_classes.elements{1},7);                                % kWh
% Actual duration
DD.d=Good.data(Power_classes.elements{1},15)/3600;                          % h

% 2D:
figure
h=histogram2(DD.e, DD.d, round([max(DD.e)*10, max(DD.d)*10]));
DD.csm=cumsum(cumsum(h.Values),2);
DD.csm_d=cumsum(h.Values,1);
% Cumsum
DD.cse = cumsum(DD.e);
DD.csd = cumsum(DD.d);



% 1st energy:
figure
e=histogram(DD.e, round(max(DD.e)*10));
% plot(e.Values)
[DD.cse, DD.ue] = unique(cumsum(e.Values));
% plot(DD.cse)
E = linspace(min(DD.e), max(DD.e), numel(e.Values));
E=E(DD.ue);
figure
plot(E,DD.cse)
F=griddedInterpolant(DD.cse', E');
DD.e_rand=rand(numel(DD.e),1)*(DD.cse(end)-min(DD.cse))+min(DD.cse);
DD.e_simu=F(DD.rand);
figure
histogram(DD.e_simu, round(max(DD.simu)*100))

% 2nd Duration
h=histogram2(DD.e, DD.d,round([max(DD.e)*10, max(DD.d)*10]));
xlabel('Energy [kWh]')
ylabel('Duration [h]')
DD.csm_d = cumsum(h.Values,2);
% for c=1:numel(h.Values(:,1))
% [Trash.csm, Trash.ue]=unique(DD.csm_d(c,:));
% DD.csm_d(c,Trash.ue) = nan;
% end
DD.D = linspace(min(DD.d), max(DD.d), numel(h.Values(1,:)));
DD.E = linspace(min(DD.e), max(DD.e), numel(h.Values(:,1)));
figure
hold on 
for c=1:numel(DD.csm_d(:,1))
plot(DD.D', DD.csm_d(c,:)')
end
hold off

G=griddedInterpolant(DD.D, DD.E', DD.csm_d);
DD.d_rand = rand(numel(DD.d),1)*(DD.csm_d(1,end)-DD.csm_d(1,1))+DD.csm_d(1,1);
DD.d_simu=G(DD.d_rand, 1*ones(numel(DD.d_rand),1))
histogram(DD.d_simu)
surf(G.Values, 'EdgeColor', 'none')
xlabel('Duration')
ylabel('Energy')
figure
surf(DD.csm_d, 'EdgeColor', 'none')
xlabel('Energy [kWh/10]')
ylabel('Duration [h/10]')

%% with pinky:

[E, D] = get_energy_and_duration(Good.data(1:10:end,12),mod(floor(Good.data(1:10:end,3)*24),24));


% Plot resulting distribution
figure
histogram2(DD.simu_e, DD.simu_d, round([max(DD.simu_e), max(DD.simu_d)*60]))
xlabel('Energy [kWh]')
ylabel('Duration[h]')

%% Check amount distro:
    % Amount
    for d=1:24
        % weekday
        Intern.amount_distr(:,d,1) = [0; cumsum(Distro_ad.weekday_dt(d).amount(:,2))];
        Intern.amount_distr(:,d,1) = Intern.amount_distr(:,d,1) / max(Intern.amount_distr(:,d,1));
        % weekend
        Intern.amount_distr(:,d,2) = [0; cumsum(Distro_ad.weekend_dt(d).amount(:,2))];
        Intern.amount_distr(:,d,2) = Intern.amount_distr(:,d,2) / max(Intern.amount_distr(:,d,2));
    end
    
Intern.start_date = datenum(datestr('01-01-2020, 00:00:00'));  % day (mm-dd-yyyy, .. )!!!
Days = 10000;
Intern.datenum_h = [Intern.start_date:1/24:Intern.start_date+Days-1/24]'; % day, in hour steps

% Type of day array (7&1: Sa&So)
Intern.start_weekday = weekday(Intern.start_date)-1;
Trash2.daytype = mod(floor(Intern.datenum_h-Intern.datenum_h(1))+Intern.start_weekday,7)+1;
    % Weekday
    Intern.daytype(find(Trash2.daytype == 2 | ...
                        Trash2.daytype == 3 | ...
                        Trash2.daytype == 4 | ...
                        Trash2.daytype == 5 | ...
                        Trash2.daytype == 6)) = 1;
    % Weekend
    Intern.daytype(find(Trash2.daytype == 7 | ...
                        Trash2.daytype == 1)) = 2;

Intern.hours = repmat([0:23]', Days,1);

for h=1:numel(Intern.hours)
        
    %% Preparation
        d = Intern.hours(h);
        w = Intern.daytype(h);
    %% Random amount of LVs 
        Trash.rnd_amount(h) = sum([0;0;1;2] .* (Intern.amount_distr(:,d+1,w)==roundtowardvec(rand(1), Intern.amount_distr(:,d+1,w), 'ceil')));
        show_progress(h,100,numel(Intern.hours))
end
WE_el = find(Intern.daytype==2);
WD_el = find(Intern.daytype==1);
% plot(Trash.rnd_amount)
count_same_integers( repelem(Intern.hours(WE_el),Trash.rnd_amount(WE_el)))
count_same_integers( repelem(Intern.hours(WD_el),Trash.rnd_amount(WD_el)))
B=count_same_integers( repelem(Intern.hours,Trash.rnd_amount))

figure
hold on
plot(B(:,1), B(:,2))
plot(B(:,1), Daytime.mean_amount/7.2*44)
hold off

    Test_simu.D_hour = discretize_struct_fast(Test_simu.data(:,3), Test_simu.data, 1/24, 0, 'days');
    % peridoify:
    Test_simu.D_hour.Daytime = periodic_course_III(Test_simu.D_hour, 'day');
% Check daytype distro
A=count_same_integers(Intern.daytype)
A(:,2)/sum(A(:,2))*7

%% Check discretization

D_H_test = discretize_struct_fast(Good.data(1:10,3), Good.data(1:10,:), hour_width, hour_start, 'hours');

[D_H_test.datetime(find(D_H_test.amount)), Good.start_datetime(1:10)]

% Result: so far the time  was rounded, not floored.

%% ---------------------------- Daytype -----------------------------------

%% Check energy distro per Daytype                                         
figure
hold on
histogram(Good.data(Good.data(:,16)==1,7), round(max(Good.data(Good.data(:,16)==1,7))),'Normalization','probability', 'DisplayStyle','stairs')
histogram(Good.data(Good.data(:,16)==2,7), round(max(Good.data(Good.data(:,16)==2,7))),'Normalization','probability', 'DisplayStyle','stairs')
histogram(Good.data(Good.data(:,16)==3,7), round(max(Good.data(Good.data(:,16)==3,7))),'Normalization','probability', 'DisplayStyle','stairs')
hold off
ylabel('Occurance [%]')
xlabel('Energy [kWh]')
legend('Weekday', 'Saturday', 'Sunday')
ytix = get(gca, 'YTick');
set(gca, 'YTick',ytix, 'YTickLabel',ytix*100)
title('Energy distro comparison')
% Result: slight difference, Saturday smaller Energy
% however, could be assumed to be independant of daytype

%% Check duration distro per Daytype                                       
figure
hold on
histogram(Good.data(Good.data(:,16)==1,15)/3600, round(max(Good.data(Good.data(:,16)==1,15)/60)),'Normalization','probability', 'DisplayStyle','stairs')
histogram(Good.data(Good.data(:,16)==2,15)/3600, round(max(Good.data(Good.data(:,16)==2,15)/60)),'Normalization','probability', 'DisplayStyle','stairs')
histogram(Good.data(Good.data(:,16)==3,15)/3600, round(max(Good.data(Good.data(:,16)==3,15)/60)),'Normalization','probability', 'DisplayStyle','stairs')
hold off
ylabel('Occurance [%]')
xlabel('Duration [h]')
legend('Weekday', 'Saturday', 'Sunday')
ytix = get(gca, 'YTick');
set(gca, 'YTick',ytix, 'YTickLabel',ytix*100)
title('Duration distro comparison')
% Result: almost no difference. slightly shorter charging on saturdays
% (while shopping?)

%% Check Energy - Duration Distro per Daytype                              
Trash.d_res = 60;
Trash.e_res = 1;

% Weekday
Trash.e = Good.data(Good.data(:,16)==1,7);     
Trash.d = Good.data(Good.data(:,16)==1,15)/3600;
Trash.hist_amount = round(max([(max(Trash.e)-min(Trash.e))/Trash.e_res, ...
                (max(Trash.d)-min(Trash.d))/Trash.d_res],[], 'omitnan'));
figure
histogram2(Trash.d, Trash.e, Trash.hist_amount)
ylabel('Energy [kWh]')
xlabel('Duration [h]')
title('Energy - Duration Distro on Weekdays')

% Saturday
Trash.e = Good.data(Good.data(:,16)==2,7);     
Trash.d = Good.data(Good.data(:,16)==2,15)/3600;
Trash.hist_amount = round(max([(max(Trash.e)-min(Trash.e))/Trash.e_res, ...
                (max(Trash.d)-min(Trash.d))/Trash.d_res],[], 'omitnan'));
figure
histogram2(Trash.d, Trash.e, Trash.hist_amount)
ylabel('Energy [kWh]')
xlabel('Duration [h]')
title('Energy - Duration Distro on Saturdays')

% Sunday
Trash.e = Good.data(Good.data(:,16)==3,7);     
Trash.d = Good.data(Good.data(:,16)==3,15)/3600;
Trash.hist_amount = round(max([(max(Trash.e)-min(Trash.e))/Trash.e_res, ...
                (max(Trash.d)-min(Trash.d))/Trash.d_res],[], 'omitnan'));
figure
histogram2(Trash.d, Trash.e, Trash.hist_amount)
ylabel('Energy [kWh]')
xlabel('Duration [h]')
title('Energy - Duration Distro on Sundays')

%Result: Hard to see difference. more short charging on Saturday

%% Check PC distro per daytype                                             
for c=1:3
Trash.d = count_same_integers(Good.data(Good.data(:,16)==c,12), 1, Power_classes.nu_of_classes);
Trash.pcd(:,c) = Trash.d(:,2)/sum(Trash.d(:,2));
end
figure
bar(Trash.pcd)
legend('Weekday', 'Saturday', 'Sunday')
xlabel('Power class')
ylabel('Occurance [100%]')
% Result: no significant change

%% Check CC distro per daytype                                             
for c=1:3
Trash.d = count_same_integers(Good.data(Good.data(:,16)==c,13), 1, Good.cases.nu_of_cases);
Trash.ccd(:,c) = Trash.d(:,2)/sum(Trash.d(:,2));
end
figure
bar(Trash.ccd)
legend('Weekday', 'Saturday', 'Sunday')
xlabel('Charge case')
ylabel('Occurance [100%]')
% Result: no significant change

%% -------------------------- Per Location --------------------------------

%% day mean scaling

figure 
hold on 
w=1;
for l=1:numel(Location)
    trash.a=sum(Trash2.exp_amount_mat(:,w,1:Distro_2.location(l).max_scale,l),1);
    plot(trash.a(:))
    Name{l} = Location(l).name;
end
hold off
legend(Name)


%% Power classes
% per daytime
for l=1:numel(Location)
    figure('Units', 'normalized', 'OuterPosition', [0 0 1 1])
    bar(Distro_2.location(l).pc_mat_dt_norm(:,2:end)')
    legend(Power_classes.names)
    xlabel('Daytime [h]')
    ylabel('Occurance')
    title([Location(l).name, ', Power classes per daytime'])
end

% for whole day
figure('Units', 'normalized', 'OuterPosition', [0 0 1 1])
bar(Trash2.pc_mat_norm)
legend(Name)
xlabel('Power Classes')
ylabel('Occurance')
title('Power classes per Location')
% Result: no significant dependance on location

%% Charge cases

% per daytime
for l=1:numel(Location)
    figure('Units', 'normalized', 'OuterPosition', [0 0 1 1])
    bar(Distro_2.location(l).cc_mat_dt_norm(:,2:end)')
    legend(Good.cases.names)
    xlabel('Daytime [h]')
    ylabel('Occurance')
    title([Location(l).name, ', Charge Case per daytime'])
end

% for whole day
figure('Units', 'normalized', 'OuterPosition', [0 0 1 1])
bar(Trash2.cc_mat_norm)
legend(Name)
xlabel('Charge Cases')
ylabel('Occurance')
title('Charge Cases per Location')
% Result: no significant dependance on location

%% Energy
trash.e = 0;
trash.d = 0;
trash.max_e = 0;
trash.max_d = 0;
for d=1:24
    for p=1:7
        trash.a = sum(Distro_2.location(1).daytime(d).power(p).e_d_distr,1);
        trash.e = [trash.e, zeros(1,numel(trash.a)-numel(trash.e))] + [trash.a, zeros(1,numel(trash.e)-numel(trash.a))];
        if trash.max_e<numel(trash.a)
           trash.max_e = numel(trash.a);
           trash.e_vec = Distro_2.location(1).daytime(d).power(p).e;
        end
        trash.a = sum(Distro_2.location(1).daytime(d).power(p).e_d_distr,2)';
        trash.d = [trash.d, zeros(1,numel(trash.a)-numel(trash.d))] + [trash.a, zeros(1,numel(trash.d)-numel(trash.a))];
        if trash.max_d<numel(trash.a)
           trash.max_d = numel(trash.a);
           trash.max_d 
           trash.d_vec = Distro_2.location(1).daytime(d).power(p).d;
        end
    end
end
% trash.e = trash.e / 24;
% trash.d = trash.d / 24;

figure
hold on
plot(trash.e_vec, trash.e/sum(trash.e))
histogram(Good.data(:,7), round(max(Good.data(:,7))),'Normalization','probability', 'DisplayStyle','stairs')
hold off
legend('Dist', 'Hist')

%% Get E & D Distro per Location
% 
 Intern.p_c_distr = [zeros(24,1), cumsum(Distro_2.location(6).pc_mat_dt_norm(:,2:end),1)'];
    Intern.p_c_distr = Intern.p_c_distr ./ repmat(Intern.p_c_distr(:,end),1,8);
 Intern.dt_distr = cumsum(Scale(1).location(6).daytype(4).mean_amount)';
    Intern.dt_distr = Intern.dt_distr / Intern.dt_distr(end);
   
e=0;    
number = 10000;
for l=1:6
for c=1:number
    e=e+1;
        Intern.dt(c,l) = find(Intern.dt_distr==...
                roundtowardvec(rand(1),Intern.dt_distr, 'ceil'),1);
    d=Intern.dt(c,l) ;
        Intern.power_classes(c,l) = find(Intern.p_c_distr(d,:)==...
                roundtowardvec(rand(1),Intern.p_c_distr(d,:), 'ceil'),1)-1;
    if sum(Distro_2.location(l).daytime(d).power(Intern.power_classes(c,l)).e)~=0
        [Intern.energy(c,l), Intern.duration(c,l)] = pinky(...  
            Distro_2.location(l).daytime(d).power(Intern.power_classes(c,l)).e,...
            Distro_2.location(l).daytime(d).power(Intern.power_classes(c,l)).d,...
            Distro_2.location(l).daytime(d).power(Intern.power_classes(c,l)).e_d_distr');
    else
        Intern.energy(c,l) = 0;
        Intern.duration(c,l) = 0;
    end
    show_progress(e,10,number*6)
    
end
end
%% Sort by daytime
for l=1:6
for d=1:24
    Intern.mean_e_dt(d,l) = mean(Intern.energy(find(Intern.dt(:,l)==d,l)));
    Intern.mean_d_dt(d,l) = mean(Intern.duration(find(Intern.dt(:,l)==d,l)));
end
end

%% Get E & D Distro from all Good data
H=histogram2(Good.data(:,7), Good.data(:,15), round([max(Good.data(:,7))*10, max(Good.data(:,15))/60]));
trash.e_d_distr = H.Values;
trash.e = 0:0.1:round(max(Good.data(:,7))*10)/10-0.1;
trash.d = 0:60:round(max(Good.data(:,15))/60)*60-60;
for c=1:number
     [Intern2.energy(c,1), Intern2.duration(c,1)] = pinky(...  
            trash.e',trash.d',trash.e_d_distr');
        show_progress(c,10,number)
end

H=histogram2(Good.data(Location(6).LV_el,7), Good.data(Location(6).LV_el,15), round([max(Good.data(Location(6).LV_el,7))*10, max(Good.data(Location(6).LV_el,15))/60]));
trash.e_d_distr = H.Values;
trash.e = 0:0.1:round(max(Good.data(Location(6).LV_el,7))*10)/10-0.1;
trash.d = 0:60:round(max(Good.data(Location(6).LV_el,15))/60)*60-60;
for c=1:number
     [Intern3.energy(c,1), Intern3.duration(c,1)] = pinky(...  
            trash.e',trash.d',trash.e_d_distr');
        show_progress(c,10,number)
end

%% daytime and pc distro

figure
hold on
for l=1:6
histogram(Intern.dt(:,l),24,'Normalization','probability', 'DisplayStyle','stairs');
end
plot(Period.Day_h.mean_amount/sum(Period.Day_h.mean_amount))
hold off
legend(Location.name)


figure
hold on
histogram(Good.data(:,12),'Normalization','probability', 'DisplayStyle','stairs');
histogram(Intern.power_classes,'Normalization','probability', 'DisplayStyle','stairs');
hold off

%% Energy distro
figure
hold on
for l=1:6
histogram(Intern.energy(:,l), round(max(Intern.energy(:,l))), 'Normalization','probability', 'DisplayStyle','stairs');    % kWh, s
Name{l} = Location(l).name ;
end
histogram(Good.data(:,7), round(max(Good.data(:,7))),'Normalization','probability', 'DisplayStyle','stairs')
% histogram(Intern3.energy(:), round(max(Intern3.energy(:))), 'Normalization','probability', 'DisplayStyle','stairs');    % kWh, s
Name{l+1} =  'Hist';
hold off
legend(Name)
xlabel('Energy [kWh]')
ylabel('normed Occurance')
title('Charge Amount Distribution per Location')
% Result: Home & shop slighty smaller energy, else very similar

%% Duration distro
figure
hold on
for l=1:6
histogram(Intern.duration(:,l), round(max(Intern.duration(:,l))/360), 'Normalization','probability', 'DisplayStyle','stairs');    % kWh, s
Name{l} = Location(l).name ;
end
histogram(Good.data(:,15), round(max(Good.data(:,15))/360),'Normalization','probability', 'DisplayStyle','stairs')
Name{l+1} =  'Hist';
hold off
xtix = get(gca, 'XTick');
set(gca, 'XTick',0:3600:70000, 'XTickLabel',0:floor(70000/3600))
legend(Name)
xlabel('Duration [h]')
ylabel('normed Occurance')
title('Charge Duration Distribution per Location')
% Result: Home & shop slighty shorter, else very similar

%% Again: Duration distro per location

figure
hold on
for l=1:numel(Location)
    act_dur{l} = Good.data(Location(l).LV_el,15)/3600;
    histogram(act_dur{l}, round(max(act_dur{l})*4),'Normalization','probability', 'DisplayStyle','stairs', 'Edgecolor', Color_matrix{l}, 'Linewidth', 4)
    name{l} = Location(l).name;
end
hold off
legend(name)
title('Ladezeit pro Standortkategorie')
xlabel('Zeit [h]')
ylabel('Häufigkeit [%]')
ytix = get(gca, 'YTick');
GCA = gca;
GCA.XLim = [0,24];
GCA.YTickLabel = ytix*100;
set_color(4)



figure
hold on
for l=1:numel(Location)
    whole_dur{l} =  Good.data(Location(l).LV_el,8)/3600;
    histogram(whole_dur{l}, round(max(whole_dur{l})*4),'Normalization','probability', 'DisplayStyle','stairs', 'Edgecolor', Color_matrix{l}, 'Linewidth', 4)
    name{l} = Location(l).name;
end
hold off
legend(name)
title('Anschlusszeit pro Standortkategorie')
xlabel('Zeit [h]')
ylabel('Häufigkeit [%]')
ytix = get(gca, 'YTick');
GCA = gca;
GCA.XLim = [0,24];
GCA.YTickLabel = ytix*100;
set_color(4)


figure
hold on
for l=1:numel(Location)
    idle_dur{l} = whole_dur{l} - act_dur{l};
    histogram(idle_dur{l}, round(max(idle_dur{l})*4),'Normalization','probability', 'DisplayStyle','stairs', 'Edgecolor', Color_matrix{l}, 'Linewidth', 4)
    name{l} = Location(l).name;
end
hold off
legend(name)
title('Standzeit pro Standortkategorie')
xlabel('Zeit [h]')
ylabel('Häufigkeit [%]')
ytix = get(gca, 'YTick');
GCA = gca;
GCA.XLim = [0,24];
GCA.YTickLabel = ytix*100;
set_color(4)
    
% Result: 

% Actual:
% - longer act charging time at office
% Whole
% - @office <3h half as often as at other locations
% - peak at 7-8:00 connection time at office
% - home % shop: 1/2h peak
% - home: small peak -> Routine?
% Idle:
% - very similar
% - @office: longer idle time 
% - @home & shop: slightly less idle time

% Comparison to private LS:
% Idle: Similar. Private: more 10-15 h
% Whole: bit different: Private: way more 13 h
% Act: different: Private: small peak at 1h and huge Peak at 2 h -> daily
% 30 km, routine?

%% Mean Energy per daytime 
figure
hold on
for l=1:6
    plot([0:23]', Intern.mean_e_dt(:,l), 'Linewidth', 2)
    Name{l} = Location(l).name;
end
plot(Period.Day_h.mean(:,7), 'Linewidth', 2)
Name{l+1} = 'Total';
hold off
ylabel('Mean Energy [kWh]')
xlabel('Daytime [h]')
title('Mean Charge Amount per Daytime')
legend(Name)
% Result: large deviations, but very random
% -> can be assumed to be independant of Location

%% Mean Duration per daytime 
figure
hold on
for l=1:6
    plot([0:23]', Intern.mean_d_dt(:,l)/3600, 'Linewidth', 2)
    Name{l} = Location(l).name;
end
plot(Period.Day_h.mean(:,15)/3600, 'Linewidth', 2)
Name{l+1} = 'Total';
hold off
ylabel('Mean Duration [h]')
xlabel('Daytime [h]')
title('Mean Charge Duration per Daytime')
legend(Name)
% Result: large deviations, but very random
% -> can be assumed to be independant of Location

%% ------------------------ Clean up --------------------------------------                                                                
clear r Distro
close all
clc

