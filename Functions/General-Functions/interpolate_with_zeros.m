function [Output_x,Output_y] = interpolate_with_zeros(Input_x, Input_y , Start, End, Resolution)

% example data:
% End = 1;
% Start = 0;
% Resolution = 0.1; % kWh
% Input_x = Cluster.daytime(23).power(7).case(5).D_energy.x;
% Input_x = [.3 .8 0.9]';
% Input_y = Input_x*2;

% TODO: Start = max([Start, min(Input_x)]);

% Input_y = Input_y(:)
% Sort
Input_x = sort(Input_x(:));         % VORSICHT! Input_y nicht mitgesorted!

% First value
Intern_x = Input_x;
Intern_y = Input_y;
if min(Input_x)>Start
    Intern_x = [Start; Intern_x];
    Intern_y = [0; Intern_y];
end
if max(Input_x)<End
    Intern_x = [Intern_x; End];
    Intern_y = [Intern_y; 0];
end
% difference
Diff = diff(Intern_x);
if max(Intern_x) < End
    Diff=[Diff; End-max(Input_x)]; % +1, if End element is not there -> has to be added later as well
end

gap_elements = find(Diff>Resolution);                                       % after those elements of Intern_x, gap_size values have to be inserted
gap_size = Diff(gap_elements)-Resolution;
gap_size_plus = [0; gap_size];
filled=Intern_x;
missing_x = [];
Output_y = Intern_y;
before_size=0;

for c=1:numel(gap_elements)
    before_gap = Output_y(1:gap_elements(c)+before_size); % c=1 -> e=1 -> b=[0]
                                              % c=2 -> e=9 -> b=[0x7 14 16]
    after_gap = Intern_y(gap_elements(c)+1:end);
    gap_fill =  zeros(round(gap_size(c)/Resolution),1);  %[Intern_y(gap_elements(c))+Resolution:Resolution:Intern_y(gap_elements(c))+gap_size(c)]';
    
    Output_y = [before_gap; gap_fill; after_gap];
    before_size = before_size + round(gap_size(c)/Resolution);
    
    missing_x = [missing_x; [Intern_x(gap_elements(c))+Resolution:Resolution:Intern_x(gap_elements(c)+1)-Resolution]'];
    
end

filled = [filled; missing_x];
Output_x=sort(filled);


end

% TODO: error if not rounded to resolution

% TODO: viel einfacher:
% start:end(Input_x) = Input_y