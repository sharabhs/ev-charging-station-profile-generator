%% Check for duplicates

cd Results\Profiles
cd Home-scale21-1min-365d-09-Apr-2019
% cd Check

for c=1:9
    trash.foldername = ['Home-scale21-1min-365d-09-Apr-2019-' num2str(c)];
    trash.name = ['Home-scale21-1min-365d-09-Apr-2019_' num2str(c), '.mat'];
%     old_dir = cd(trash.foldername);
    File(c) = load(trash.name);
%     cd(old_dir)
end

for c=2:numel(File)
    max(File(c).Test_simu.actual_power.data-File(c-1).Test_simu.actual_power.data)
end

for c=1:numel(File)
    File(c).Test_simu.actual_power
end
    

figure
hold on
for c=1:9
    title(num2str(c))
    plot(File(c).Test_simu.actual_power.datetime, File(c).Test_simu.actual_power.data(1:numel(File(c).Test_simu.actual_power.datetime)), 'Color', Color_matrix{c})
   
name{c} = num2str(c);
end
hold off

legend(name)

cd ..