function [] = save_figures(amount, FOLDER, KEEP)
    % saves last 'amount' figures to folder.
    
disp('Saving figures ...')
if exist('FOLDER') && numel(FOLDER)~=0
    old_location = cd(FOLDER);
end
    for c=1:amount
        h1=get(gca,'title');
        Title=get(h1,'string');
        Title = strrep(Title, ' ', '-');
        Title = strrep(Title, ',', '-');
        Title = strrep(Title, ':', '-');
%         gf = getframe(gca);
%         saveas(gca, [Title, '.fig'])
        saveas(gca, [Title, '.png'])
%         imwrite(gf.cdata, [Title, '.jpg'], 'jpg')
        if ~exist('KEEP')
            close
        end
    end
if exist('FOLDER') && numel(FOLDER)~=0
    cd(old_location);
end

end
