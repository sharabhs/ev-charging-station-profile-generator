
% _________________________________________________________________________
% =========================================================================
%           Constants for EV data analysis and profile generation
% =========================================================================
% 28.01.2019                                                    Linus Kemme
%
% - Design & Time parameters
% - Power class parameters
%


%% Time                                                                    

TIME_FORMAT = 'dd.mm.yyyy, HH:MM:SS';
TIME_FORMAT_DT = 'dd.MM.yyyy, HH:mm:ss';
CLOCK_FORMAT = 'HH:MM:SS';
CLOCK_FORMAT_DT = 'HH:mm:ss';


%% Power classes                                                           
Power_classes.nu_of_classes = 6;
Power_classes.names = {'3.68 kW'; '4.60 kW'; '7.20 kW'; '11.0 kW'; '17.2 kW'; '22.0 kW'};
% Power of power class
Power_classes.max = [3.68; 4.60; 7.20; 11.0; 17.2; 22.0];             % kW
% Upper limit of power class
Power_classes.upper = [3.76; 4.70; 7.33; 11.30; 17.30; 22.60];
% Minimum power of Power class (depending of amount of phases)
Power_classes.min = [1.38; 1.38; 1.38; 4.16; 4.16; 4.16];             % kW      

Power_classes.diff = Power_classes.max - Power_classes.min;           % kW
Power_classes.unit = '[kW]';

% Charge Slope characteristics
% slope of linearly decreasing power phase
Power_classes.slope = [0.1565; 0.2880; 0.2190; 0.2619; 0.4000; 0.3014]/60;    % kW/s      
% duration of linearly decreasing power phase
Power_classes.full_slope_time = Power_classes.diff ./ Power_classes.slope; % s
% Energy in this phase
Power_classes.full_slope_energy = (Power_classes.min.*Power_classes.full_slope_time + ...       % kWh
    1/2* Power_classes.slope .* Power_classes.full_slope_time.^2)/3600;
Power_classes.full_only_slope_energy = (1/2* Power_classes.slope .* Power_classes.full_slope_time.^2)/3600;

% Power course in sec resolution
for c=1:Power_classes.nu_of_classes
    Power_classes.full_slope_profile{c} = linspace(Power_classes.max(c), ...
        Power_classes.min(c), round(Power_classes.full_slope_time(c)))';  % kW, 1 element per s
end


%% Colors, Markers and Linestyles                                          
Marker_matrix = {'o', '+', '*','.','x','s','d','^'};
% Color_matrix_bad = {'y', 'm','c','r','g','b','k'};
Line_matrix = {'-.', ':', '--', '-'};
Color_matrix = {
            [0, 0.4470, 0.7410], 	          	
          	[0.8500, 0.3250, 0.0980], 	         
          	[0.9290, 0.6940, 0.1250], 	          	
          	[0.4940, 0.1840, 0.5560], 	         
          	[0.4660, 0.6740, 0.1880], 	          	
          	[0.3010, 0.7450, 0.9330], 	         
          	[0.6350, 0.0780, 0.1840],
            %[10,36,99]/255,
            %[8,17,38]/255,
            %[63,67,76]/255,
%             [14,44,114]/255,
            [6, 51, 81]/255,
            [159,170,196]/270};
  

Color_ind = [6,1,4,3,8,7,9,2,5];             


%% Fullscreen                                                              
% FULLSCREEN = '''Units'', ''normalized'', ''OuterPosition'', [0 0 1 1]';


