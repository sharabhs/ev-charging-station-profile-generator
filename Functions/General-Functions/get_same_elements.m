function [Same] = get_same_elements(A,B)
% Out of two integer vectors of different size, get vector with only those integers that appear in both
    
% Example data:
% B =[ 7     3     1     2 ];
% A =[ 1     3     5     4 ];

% Prepare input
A=A(:);
B=B(:);

% Add all values 
Min = min([min(A), min(B)]);
Max = max([max(A), max(B)]);
All = [Min:Max]';
A=[A; All];
B=[B; All];

% count occurance and substract 1 again -> 0/1 vector
CA=count_same_integers(A);
CA(:,2) = CA(:,2)-1;
CB=count_same_integers(B);
CB(:,2) = CB(:,2)-1;

% get same (both 1)
Same = CA(CA(:,2) & CB(:,2),1);

end

