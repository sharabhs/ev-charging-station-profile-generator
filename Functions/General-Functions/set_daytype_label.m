function [] = set_daytype_label(STEPS_PER_DAY, DISTANCE, FONTSIZE, LANGUAGE)

%% Input
BA_Test_constants

if ~exist('STEPS_PER_DAY') || numel(STEPS_PER_DAY)~=1
    STEPS_PER_DAY = 24;
end

if ~exist('DISTANCE') || numel(DISTANCE)~=1
    DISTANCE = 4;
end

if ~exist('FONTSIZE') || numel(FONTSIZE)~=1
    FONTSIZE = 16;
end

if ~exist('LANGUAGE') || numel(LANGUAGE)==0
    LANGUAGE = 'DE';
end


%% Preparation
for d=1:12*3
    day_label{d} = [num2str(mod(d-1,12)*2) ':00'];
end

trash.ca=gca;
% change xlabel
set(trash.ca, 'XTick', 0:6:72, 'XTickLabel', day_label(1:3:end))

trash.distance = min(trash.ca.YLim)-diff(trash.ca.YLim)*DISTANCE/100; 

if strcmp(LANGUAGE, 'EN') 
    trash.names =  {'Workday', 'Saturday', 'Sunday'};
elseif strcmp(LANGUAGE, 'DE')
    trash.names =  {'Werktag', 'Samstag', 'Sonntag'};
end

%% Add Labels


for c=1:3
    text('Units', 'Data', 'Position', [STEPS_PER_DAY/2+STEPS_PER_DAY*(c-1),trash.distance], ...
        'HorizontalAlignment', 'center', 'String', trash.names{c}, 'Fontweight', 'bold',...
        'Color', Color_matrix{8}, 'FontSize', FONTSIZE)
    text('Units', 'Data', 'Position', [STEPS_PER_DAY*(c-1),trash.distance],...
        'HorizontalAlignment', 'center', 'String', '|', 'Fontweight', 'bold',...
        'Color', Color_matrix{8}, 'FontSize', FONTSIZE)
end
 
text('Units', 'Data', 'Position', [STEPS_PER_DAY*(c),trash.distance],...
    'HorizontalAlignment', 'center', 'String', '|', 'Fontweight', 'bold',...
    'Color', Color_matrix{8}, 'FontSize', FONTSIZE)

end
