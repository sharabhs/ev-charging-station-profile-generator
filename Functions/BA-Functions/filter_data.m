function [Filtered] = filter_data(Raw)
% Filter false data and allocate values

%% Unit Convention:                                                        

% Time:      |  datenum (days)
% Duration:  |  s
% Power:     |  kW
% Energy:    |  kWh
% slope:     |  kW/s
% Raw = 
% 
%   struct with fields:
% 
%           data: [80418×9 double]
%       textdata: {1×9 cell}
%     colheaders: {1×9 cell}
%           date: '09-Jan-2019 16:50:15'
%           name: 'Reduziert_AC_Oeff_21-12-18.txt'

%% Load global constants                                                   
BA_Test_constants

%% Headers and Units                                                       
[Raw.no_of_rows, Raw.no_of_cols] = size(Raw.data);

% create headers matrix
for c=1:Raw.no_of_cols
    Raw.headers{c,1} = ['(' num2str(c) ') ' Raw.colheaders{c}]; 
end

Filtered.units = {'[]'; '[]'; '[d]'; '[d]'; '[kWh]'; '[kWh]'; '[kWh]'; '[s]'; '[]'};

%% Pass Data                                                               
Filtered.data = Raw.data;
Filtered.headers = Raw.headers;

%% Convert Units                                                           

% Energy into kWh
Filtered.data(:,5:7) = Filtered.data(:,5:7)/1000;                                   % kWh
Filtered.headers{7} = '(7) Totalchargeamount [kWh]';

%% Time:                                                                   
latest_start_date = datetime('21.12.2018, 10:51:06', 'Format', TIME_FORMAT_DT);
latest_end_date = datetime('21.12.2018, 11:11:19', 'Format', TIME_FORMAT_DT);

Filtered.data(:,3) = datenum(latest_start_date + days(Raw.data(:,3)- max(Raw.data(:,3))));   % [datenum]
Filtered.data(:,4) = datenum(latest_end_date + days((Raw.data(:,4)- max(Raw.data(:,4)))));   % [datenum]

% flip time order
Filtered.data = flip(Filtered.data,1);

%% Mean Power over each LV:                                                
% Charge amount [kWh] / Duration [in h, not s -> /3600]
Filtered.data(:,10) = Filtered.data(:,7)./ (Filtered.data(:,8)/3600);                   % kW
Filtered.headers{10} = '(10) Mean Power [kW]';
% add new units
Filtered.units{10} = '[kW]';

%% Filter out bad data                                                     

% Get old data
start_date =  datetime('01.11.2016, 00:00:00', 'Format', TIME_FORMAT_DT);
old_elements = find(Filtered.data(:,3) < datenum(start_date));

% Get wrong duration:
% IGNORED, AS THE DURATION IS PROBABLY RIGHT AND THE START TIMES WRONG

% Get zero energy data (< 10 Wh)
zero_energy = find(Filtered.data(:,7) <= 0.01);

% Get zero duration data (less than 30 sec)
zero_duration=find(Filtered.data(:,8) <= 30);

% Get unrealistic power data
MAX_POWER = 25;     % [kW]
MIN_POWER = 0;     % [kW]
too_much_power = find(Filtered.data(:,10) >= MAX_POWER | Filtered.data(:,10) <= MIN_POWER);

% Get single power noise data (if max power of user is twice as high as second max power -> throw away)
c=1;
for ID = 1:max(Filtered.data(:,2))
    user_elements = find(Filtered.data(:,2) == ID);
    if ~isempty(user_elements)
        [~, sort_index] = sort(Filtered.data(user_elements,10));
        if numel(user_elements)>1
            variation = diff(Filtered.data(user_elements(sort_index([end-1,end])),10))/max(Filtered.data(user_elements,10));
        else 
            variation = 0;
        end
        if variation > 0.5
            power_noise(c) = user_elements(sort_index(end));
            c=c+1;
        end
    end
end
power_noise = power_noise';
% find(power_noise==find(Good.data(:,2) == 13))

% Get NaN user IDs
nan_users = find(isnan(Filtered.data(:,2)));

% Filter out false data
Filtered.data(unique([old_elements; zero_energy; zero_duration; too_much_power; nan_users; power_noise]),:) = [];
% Filtered.data(unique([old_elements; zero_energy; zero_duration; too_much_power; nan_users]),:) = [];

[Filtered.no_of_rows, Filtered.no_of_cols] = size(Filtered.data);

%% Get Daytype and time of day                                             
% Get datetime
Filtered.start_datetime = datetime(datestr(Filtered.data(:,3), TIME_FORMAT), 'Format', TIME_FORMAT_DT);

% Daytype (1:Weekday, 2: saturday, 3: Sunday)
Filtered.data(:,16) = ones(Filtered.no_of_rows,1);
Filtered.headers{16} = '(16) Daytype';
Filtered.units{16} = '1: Weekday, 2: Saturday, 3: Sunday';
Trash.saturday_elm = find(weekday(Filtered.start_datetime)==7);
Trash.sunday_elm = find(weekday(Filtered.start_datetime)==1);
Filtered.data(Trash.saturday_elm,16)=2*ones(numel(Trash.saturday_elm),1);
Filtered.data(Trash.sunday_elm,16)=3*ones(numel(Trash.sunday_elm),1);

% Time of day
Filtered.data(:,17) = mod(floor(Filtered.data(:,3)*24),24);
Filtered.headers{17} = '(17) Daytime';
Filtered.units{17} = 'o`clock';

%% Get general Info                                                        
Filtered.info.earliest_start = datetime(datestr(min(Filtered.data(:,3))));
Filtered.info.latest_end = datetime(datestr(max(Filtered.data(:,4))));
Filtered.info.nu_of_days = days(Filtered.info.latest_end-Filtered.info.earliest_start);
Filtered.info.nu_of_LVs = numel(Filtered.data(:,1));
Filtered.info.nu_of_LSs = numel(unique(Filtered.data(:,1)));
% Get new size of data matrix
[Filtered.no_of_rows, Filtered.no_of_cols] = size(Filtered.data);

end

