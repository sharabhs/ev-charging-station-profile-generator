%% Prepare data for curve clustering
clear Day Week Name ops Weekday
close all
number=255;
tic
for c=[1:number]
    Day.Y{c,1} = Charger(c).Daytime.mean_amount/sum(Charger(c).Daytime.mean_amount);
    Day.X{c,1}=[0:23]';
    Week.Y{c,1} = Charger(c).Weektime.mean_amount/sum(Charger(c).Weektime.mean_amount);
    Week.X{c,1} = [0:1/24:7-1/24]';
    Charger(c).week_distro = count_same_integers(Charger(c).data(:,16));
    Charger(c).weekday = mean(reshape(Week.Y{c,1}(1:24*5),24,5),2);
    Charger(c).weekend = mean(reshape(Week.Y{c,1}(24*5+1:end),24,2),2);
    Weekday.Y{c,1} = [Charger(c).weekday; Week.Y{c,1}(24*5+1:24*6); Week.Y{c,1}(24*6+1:end)];
    Weekday.X{c,1}=[0:24*3-1]';
    show_progress(c, 10,number)
end

%% Wiuck plot
figure
hold on
for c=1:255
    plot(Charger(c).Weektime.datenum, Charger(c).Weektime.mean_amount)
end
hold off
xlabel('Day')

figure
hold on
d=0;
clear Name_1
for c=[242]
    d=d+1;
    plot(Weekday.X{c,1}, Weekday.Y{c}, 'LineWidth', 2)
    Name_1{d} = num2str(c);
end
legend(Name_1)
hold off

figure
hold on
for c=1:numel(Location)-1
    plot([0:numel(Weekday.Y{1})-1]', Location(c).daytype_mean, 'LineWidth', 2)
    Name_2{c} = Location(c).name;
end
hold off
legend(Name_2)

count_same_integers(weekday(Good.start_datetime( Charger(242).elements)))
count_same_integers(hour(Good.start_datetime( Charger(242).elements)))


%% Curve clustering
% Cluster method:
ops.method = 'lrm';

% quadratic model
ops.order =20;
for k=2:10
% Amount of Clusters

ops.K = k;

Weekday.Y = Weekday.Y(1:100);
Weekday.X = Weekday.X(1:100);
% Start CCT
Model=curve_clust(Weekday, ops);

% Show result
% showmodel(Model, Weekday)
% title(['charger-cluster-srm_d-', num2str(k), 'all'])
% xlabel('Daytime [h]')
% ylabel('Normed mean #LVs')



%% Use result
% Clusterdata=nan*ones(numel(Weekday.Y{1}),number,ops.K);
% Clusterdistro = zeros(2,ops.K);
for c=1:ops.K
    Clusterdata{:,c} = reshape(cell2mat(Weekday.Y(find(Model.C==c))),72,numel(Weekday.Y(find(Model.C==c))));
    Clustermean(:,c) = mean(Clusterdata{:,c},2);
%     Clusterdata(:,c,Model.C(c)) = [Weekday.Y{c}];
%     Clusterdistro(:,Model.C(c)) = Clusterdistro(:,Model.C(c)) + Charger(c).week_distro(:,2);
end

% for c=1:ops.K
% Clustermean(:,c)=my_nanmean(Clusterdata(:,:,c),2);
% end

Occ=count_same_integers(Model.C);

% plot all cluster
d=0;
figure('units','normalized','outerposition',[0 0 1 1])
hold on
for c=[1:10]
    d=d+1;
plot([0:numel(Weekday.Y{1})-1]', Clustermean(:,c), 'LineWidth', 2)
Name{d} = ['Cluster ', num2str(c), ', Occurance: ' num2str(Occ(c,2))];%, ' Distro: ', num2str(Clusterdistro(1,c)/sum(Clusterdistro(:,c))/5*7)];
end
hold off
legend(Name)
xlabel('Daytime [h] weekday and weekend')
ylabel('Normed mean #LVs')
title(['Charger cluster daytype Cl', num2str(k)])


% office
clear Name
d=0;
figure('units','normalized','outerposition',[0 0 1 1])
hold on
for c=[6 8]
    d=d+1;
plot([0:numel(Weekday.Y{1})-1]', Clustermean(:,c), 'LineWidth', 2)
Name{d} = ['Cluster ', num2str(c), ', Occurance: ' num2str(Occ(c,2))];%, ' Distro: ', num2str(Clusterdistro(1,c)/sum(Clusterdistro(:,c))/5*7)];
end
plot([0:numel(Weekday.Y{1})-1]', mean(Clustermean(:,[6 8]),2), 'LineWidth', 2)
Name{d+1} = 'Mean'; 
hold off
legend(Name)
xlabel('Daytime [h] weekday and weekend')
ylabel('Normed mean #LVs')
title(['Charger at office k= ', num2str(k)])

% home
clear Name
d=0;
figure('units','normalized','outerposition',[0 0 1 1])
hold on
for c=[5]
    d=d+1;
plot([0:numel(Weekday.Y{1})-1]', Clustermean(:,c), 'LineWidth', 2)
Name{d} = ['Cluster ', num2str(c), ', Occurance: ' num2str(Occ(c,2))];%, ' Distro: ', num2str(Clusterdistro(1,c)/sum(Clusterdistro(:,c))/5*7)];
end
plot([0:numel(Weekday.Y{1})-1]', mean(Clustermean(:,[5]),2), 'LineWidth', 2)
Name{d+1} = 'Mean'; 
hold off
legend(Name)
xlabel('Daytime [h] weekday and weekend')
ylabel('Normed mean #LVs')
title(['Charger at home ', num2str(k)])

% home + shop
clear Name
d=0;
figure('units','normalized','outerposition',[0 0 1 1])
hold on
for c=[2 9]
    d=d+1;
plot([0:numel(Weekday.Y{1})-1]', Clustermean(:,c), 'LineWidth', 2)
Name{d} = ['Cluster ', num2str(c), ', Occurance: ' num2str(Occ(c,2))];%, ' Distro: ', num2str(Clusterdistro(1,c)/sum(Clusterdistro(:,c))/5*7)];
end
plot([0:numel(Weekday.Y{1})-1]', mean(Clustermean(:,[2 9]),2), 'LineWidth', 2)
Name{d+1} = 'Mean'; 
hold off
legend(Name)
xlabel('Daytime [h] weekday and weekend')
ylabel('Normed mean #LVs')
title(['Charger near home and shop ', num2str(k)])

% Office & Home 
clear Name
d=0;
figure('units','normalized','outerposition',[0 0 1 1])
hold on
for c=[4 10]
    d=d+1;
plot([0:numel(Weekday.Y{1})-1]', Clustermean(:,c), 'LineWidth', 2)
Name{d} = ['Cluster ', num2str(c), ', Occurance: ' num2str(Occ(c,2))];%, ' Distro: ', num2str(Clusterdistro(1,c)/sum(Clusterdistro(:,c))/5*7)];
end
plot([0:numel(Weekday.Y{1})-1]', mean(Clustermean(:,[4 10]),2), 'LineWidth', 2)
Name{d+1} = 'Mean'; 
hold off
legend(Name)
xlabel('Daytime [h] weekday and weekend')
ylabel('Normed mean #LVs')
title(['Charger near Office & Home, k=', num2str(k)])

% City and home ?
clear Name
d=0;
figure('units','normalized','outerposition',[0 0 1 1])
hold on
for c=[1 3 7]
    d=d+1;
plot([0:numel(Weekday.Y{1})-1]', Clustermean(:,c), 'LineWidth', 2)
Name{d} = ['Cluster ', num2str(c), ', Occurance: ' num2str(Occ(c,2))];%, ' Distro: ', num2str(Clusterdistro(1,c)/sum(Clusterdistro(:,c))/5*7)];
end
plot([0:numel(Weekday.Y{1})-1]', mean(Clustermean(:,[1 3 7]),2), 'LineWidth', 2)
Name{d+1} = 'Mean'; 
hold off
legend(Name)
xlabel('Daytime [h] weekday and weekend')
ylabel('Normed mean #LVs')
title(['Charger near city and home ', num2str(k)])

end

%% Allocate Charger
Location(1).name ='Office';
Location(1).k=[6,8];

Location(2).name ='Office & Home';
Location(2).k=[4,10];

Location(3).name ='City & Home';
Location(3).k=[1,3,7];

Location(4).name ='Home';
Location(4).k=[5];

Location(5).name ='Home & Shop';
Location(5).k=[2,9];

for c=1:numel(Location)
%     Location(c).charger_el = find(sum(Model.C==Location(c).k,2));
    Location(c).total_nu_of_LVs = 0;
    Location(c).daytype_mean = zeros(numel(Weekday.Y{Trash.added{c}(1)}),1);
    Location(c).mean_amount = [];
    for d=1:numel(Location(c).charger_el)
        Location(c).daytype_mean = my_nansum([Location(c).daytype_mean, Weekday.Y{Location(c).charger_el(d)}*sum(Charger(Location(c).charger_el(d)).Daytime.amount)],2);
        Location(c).total_nu_of_LVs = Location(c).total_nu_of_LVs + sum(Charger(Location(c).charger_el(d)).Daytime.amount);
    end
    Location(c).daytype_mean  = Location(c).daytype_mean / Location(c).total_nu_of_LVs;
end

% Add Charger 211-255 manually:

Trash.added{1}=[217 222 220 234 242 243 245 252]';
Trash.added{2}=[214 213 211 232 239 241 244 255 251 223 248 249]';
Trash.added{3}=[227 228 246]';
Trash.added{4}=[215 216 212 221 219 218 231 235 250 254]';
Trash.added{5}=[224 226 225 229 230 236 237 233 238 240 247 253]';
Trash.added{6} = [211:255]';

for d=1:5;
e=0;
figure 
hold on
clear Name
Trash.mean_add{d} = zeros(numel(Weekday.Y{Trash.added{d}(1)}),1);
for c=1:numel(Trash.added{d})
    plot(Weekday.Y{Trash.added{d}(c)})
    if ~isnan(Weekday.Y{Trash.added{d}(c)})
    Trash.mean_add{d} = Trash.mean_add{d} + Weekday.Y{Trash.added{d}(c)};
    e=e+1;
    end
    Name{c} = num2str(Trash.added{d}(c));
end
Trash.mean_add{d} = Trash.mean_add{d} / e;
plot(Trash.mean_add{d}, 'LineWidth', 2)
Name{c+1} = 'Mean'; 
plot(Location(d).daytype_mean, 'LineWidth', 2) 
Name{c+2} = 'Acutal'; 
hold off
legend(Name)
title(Location(d).name)
end
% Result: well distributed!

Trash.check = [];
for d=1:5
%     Trash.check = [Trash.check; Trash.added{d}];
    Location(d).charger_el = sort( [Location(d).charger_el; Trash.added{d}]);
end
% Trash.check = sort(Trash.check);


% All Charger as well:

Location(6).name ='All Chargers';
Location(6).k=1:10; 
Location(6).charger_el = 1:number;
Location(6).total_nu_of_LVs = 0;
Location(6).daytype_mean = 0;
for d=1:numel(Location(6).charger_el)
    Location(6).daytype_mean = Location(6).daytype_mean + Weekday.Y{Location(6).charger_el(d)}*sum(Charger(Location(6).charger_el(d)).Daytime.amount);
    Location(6).total_nu_of_LVs = Location(6).total_nu_of_LVs + sum(Charger(Location(6).charger_el(d)).Daytime.amount);
end
Location(6).daytype_mean  = Location(6).daytype_mean / Location(6).total_nu_of_LVs;



figure 
hold on
for c=1:numel(Location)
plot([0:numel(Weekday.Y{1})-1]', Location(c).daytype_mean, 'LineWidth', 2)
Name_2{c} = Location(c).name;
end
legend(Name_2)
xlabel('Daytime [h] weekday and weekend')
ylabel('Normed mean #LVs')
title('Charger week profile per Location')

%TODO: normed, unnormed? consider occurance

%% Allocate LVs
for c=1:numel(Location)
    Location(c).LV_el = [];
    for d=1:numel(Location(c).charger_el)
        Location(c).LV_el = [Location(c).LV_el; find(Good.data(:,1) == Charger(Location(c).charger_el(d)).ID)];
    end
    Location(c).LV_el = sort(Location(c).LV_el);
end

%% Info
for l=1:numel(Location)
    Location(l).nu_of_cars = numel(unique(Good.data(Location(l).LV_el,2)));
end

%% Test:
clear test
set(0,'DefaultFigureVisible','off');
figure
hold on
for c=1:5
    test.X{c,1}=[0:24]';
    test.Y{c,1} = (1+rand(1))*(test.X{c,1}-13+rand(1)).^2;
    plot(test.Y{c,1})
end
for c=5:10
    test.X{c,1}=[0:24]';
    test.Y{c,1} = (1+rand(1))*(test.X{c,1}+rand(1));
    plot(test.Y{c,1})
end
hold off

set(0,'DefaultFigureVisible','on');
ops.method = 'lrm';

% quadratic model
ops.order = 2;

% Amount of Clusters
ops.K = 2;


% Start CCT
Model=curve_clust(test, ops)



showmodel(Model, test)


%% Check Charger 200 bis 220
clc
for c=1:255
    Trash.nu_of_periods(c,1)=Charger(c).Weektime.nu_of_periods;
end
find(Trash.nu_of_periods==0)
