function [Output] = get_charge_time(Discrete, resolution)
%% Creates datenum arrays per user with zeros, while not charging and its
% User ID while charging
% -> needs to create chraging frames

%% Preparation
% nonzero input only, clustered into user IDs

%% Init


%% fill time between start and end
% looop through user
for c = 1:Discrete.nu_of_nonzero        
    % Init with minimum start time of user
    Intern.input(c).data = Discrete.cluster(Discrete.nonzero_e(c)).data;
    Output.user(c).charge_time = Discrete.min(Discrete.nonzero_e(c),3);
    % loop through start times
    for d = 1:numel(Intern.input(c).data(:,3))
        start_time = Intern.input(c).data(d,3);
        end_time = Intern.input(c).data(d,4);
        s_e_array = [start_time:resolution:end_time]';
        Output.user(c).charge_time = [Output.user(c).charge_time; s_e_array];
    end
    % allocate number (new ID) to user
    Output.user(c).number = c*ones(numel(Output.user(c).charge_time),1);
end

Output.nu_of_user = c;

% transpose
Output.user = Output.user';

end