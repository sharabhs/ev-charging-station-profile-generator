function [All_charger_rep] = get_repeated_charger_data(Charger)
% repeat Charger data to fill whole period (3yrs) -> All_charger_rep

%% Preparation
All_charger_rep.D_h.x = Charger(1).full_D_h.x;
All_charger_rep.D_h.datetime = datetime(datestr(Charger(1).full_D_h.x));

%% Add amount of all chargers
All_charger_rep.D_h.total_amount = zeros(numel(Charger(1).full_D_h.amount),1);
for c=1:255
    All_charger_rep.D_h.total_amount = All_charger_rep.D_h.total_amount + Charger(c).full_D_h.amount;
end

%% Get weekday and weekend elements
Trash.daytype(1).els = sort([24*0+1:24*7:numel(All_charger_rep.D_h.x),...
                             24*1+1:24*7:numel(All_charger_rep.D_h.x),...
                             24*2+1:24*7:numel(All_charger_rep.D_h.x),...
                             24*3+1:24*7:numel(All_charger_rep.D_h.x),...
                             24*4+1:24*7:numel(All_charger_rep.D_h.x)])';
Trash.daytype(2).els = [24*5+1:24*7:numel(All_charger_rep.D_h.x)]';
Trash.daytype(3).els = [24*6+1:24*7:numel(All_charger_rep.D_h.x)]';

for w=1:3
    Trash.daytype(w).els = repmat(Trash.daytype(w).els,1,24) + repmat(0:23,numel(Trash.daytype(w).els),1);
    Trash.daytype(w).els = sort(Trash.daytype(w).els(:));
end

Trash.daytype(4).els = [1:numel(All_charger_rep.D_h.x)]';

%% Get mean amount per weekday, weekend day, normal day and week

All_charger_rep.D_h.amount = All_charger_rep.D_h.total_amount/255;

for w=1:4
    All_charger_rep.D_h.daytype(w).mean_amount = mean(reshape(All_charger_rep.D_h.amount(Trash.daytype(w).els), 24,numel(All_charger_rep.D_h.amount(Trash.daytype(w).els))/24),2);
    All_charger_rep.D_h.daytype(w).day_mean = sum(All_charger_rep.D_h.daytype(w).mean_amount);
end

All_charger_rep.D_h.Week.mean_amount = mean(reshape(All_charger_rep.D_h.amount, 24*7,numel(All_charger_rep.D_h.amount)/24/7),2);
All_charger_rep.D_h.Week.week_mean = sum(All_charger_rep.D_h.Week.mean_amount);

end

