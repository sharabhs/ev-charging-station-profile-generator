function [Scale_fit] = fit_scaling(Scale)
%% Create Fit for Scaling

%% Settings                                                                
PLOT = false;
SAVE = false;
set(0,'DefaultFigureVisible','on');
load Scale_fit.mat
clear trash

trash.number = 10000;
trash.max_scale = 300;
trash.max_cars = 120;

%% Binomial Fit to Scale                                                   

% get success probability and fit 
set(0,'DefaultFigureVisible','off');
eq = @(a,x) 1-exp(-a*x);
e=0;
hold on
tic
for l=1:6
    for w=1:4
        for d=1:24
            s=1;
            while s <= numel(Scale) && numel(Scale(s).location(l).daytype)>0 
                trash.rows = numel(Scale(s).location(l).daytype(w).distr(:,d));
                data = repelem([0:trash.rows-1], round(Scale(s).location(l).daytype(w).distr(:,d)*trash.number));
                [P_fix{d,w,l}(s),~] = mle(data,'distribution','binomial','ntrials',16*s);
                P_mean{d,w,l} = mean(P_fix{d,w,l});
                P_first{d,w,l} = P_fix{d,w,l}(1);
                s=s+1;
            end
            e=e+1;
            show_progress(e,10,24*4*6)
        end
    end
end
set(0,'DefaultFigureVisible','on');


% get distribution from fit
tic
e=0;
for l=1:6
    for w=1:4
        for d=1:24
            for s=1:trash.max_scale
                %trash.d = count_same_integers(binornd(2,Fit{d,w,l}(s),trash.number,1),0,2);
%                 Scale_fit.distr(:, d, s, w, l) = trash.d(:,2)/trash.number;
                trash.fix_distr(:, d, s, w, l) = binopdf([0:120], 16*s, P_mean{d,w,l})';
%                 Scale_fit.unlim_distr(:, d, s, w, l) = [(1-Fit_unlim{d,w,l}(s))^2; 2*Fit_unlim{d,w,l}(s)*(1-Fit_unlim{d,w,l}(s)); (Fit_unlim{d,w,l}(s))^2];
                Scale_fit.fix_exp_amount(d, s, w, l) = sum(trash.fix_distr(:, d, s, w, l) .* [0:numel(trash.fix_distr(:, d, s, w, l))-1]');
                if s<numel(Scale) && numel(Scale(s).location(l).daytype)>0 && numel(Scale(s).location(l).daytype)~=0
                    trash.fix_exp_am(d, s, w, l) = Scale(s).location(l).daytype(w).mean_amount(:,d);
                end
            end
            e=e+1;
            show_progress(e,1,6*4*24)
        end
    end
end

 Scale_fit.new_lim_distr_m = [];
 
%% Limit to max 2 cars per minute                                          
tic
e=0;
for l=1:6
    for w=1:4
        for d=1:24
            for s=1:trash.max_scale
                Scale_fit.new_lim_distr_m(:, d, s, w, l) =  trash.fix_distr(1:trash.max_cars+1, d, s, w, l);
                Scale_fit.new_lim_distr_m(61, d, s, w, l) = Scale_fit.new_lim_distr_m(trash.max_cars+1, d, s, w, l) + sum(trash.fix_distr(trash.max_cars+2:end, d, s, w, l));
                Scale_fit.new_lim_exp_am_m(d, s, w, l) = sum([0:trash.max_cars]' .* Scale_fit.new_lim_distr_m(:, d, s, w, l));
            end
            e=e+1;
            show_progress(e,10,4*6*24)
        end
    end
end

% %% Pass to distro                                                          
% for l=1:6
%     for w=1:4
%         for d=1:24
%             for s=1:trash.max_scale
%                 Distro_1.location(l).daytype(w).daytime(d).scale(s).new_lim_amount_m = Scale_fit.new_lim_distr_m(:, d, s, w, l);     
%                 Distro_1.location(l).daytype(w).daytime(d).scale(s).new_exp_amount_m = Scale_fit.new_lim_exp_am_m(d, s, w, l);        
%             end
%         end
%     end
% end

%% save Scale fit and Distro                                               
if SAVE
save('Scale_fit.mat', 'Scale_fit')
% save('Distro_1.mat', 'Distro_1')
end


%% ---------------------------- Check result ------------------------------

if PLOT

%% Plot exemplary distributions
% preparation
for c=1
close all
w=1;
d=9;
l=6;
trash.max=20;

S=[1 50 255];
trash.color = [6 1 4];
for c=1:numel(S)
    trash.distr{c} = [Scale(S(c)).location(l).daytype(w).distr(:,d); zeros(trash.max-numel(Scale(S(c)).location(l).daytype(w).distr(:,d)),1)];
    trash.distr{c} = trash.distr{c}(1:trash.max);
    trash.exp{c} = Scale(S(c)).location(l).daytype(w).mean_amount(d);
    trash.fit_exp{c} = Scale_fit.fix_exp_amount(d,S(c), w, l);
%     h(2*c-1) = histogram(Scale_fit.new_lim_distr_m(1:trash.max, d, S(c), w, l));
%     h(2*c) = histogram(trash.distr{c}(1:trash.max));
end
end

% plot
for c=1
figure 

for c=1:3
subplot(3,1,c)
hold on
plot(0, 'Visible', 'on', 'Color', 'none')
b=bar([Scale_fit.new_lim_distr_m(1:trash.max, d, S(c), w, l), trash.distr{c}(1:trash.max)]);
plot(0, 'Visible', 'on', 'Color', 'none')

b(1).FaceColor = Color_matrix{trash.color(c)};
b(2).FaceColor = Color_matrix{trash.color(c)};
b(2).FaceAlpha = 0.5;
b(1).EdgeColor = 'none';
b(2).EdgeColor = 'none';
b(1).BarWidth = 1;   
ytix = get(gca, 'YTick');
set(gca, 'YTick',ytix, 'YTickLabel',ytix*100)
if c~=3
L=legend(['n = ',num2str(S(c))], ['Fit,              ', num2str(trash.fit_exp{c},'%2.2f'), ' LV/h'], ['Gemessen, ', num2str(trash.exp{c},'%2.2f'), ' LV/h'], 'Location', 'east');
set(gca, 'XTick', 1:2:20, 'XTickLabel', [])
else
L=legend(['n = ',num2str(S(c))], ['Fit,              ', num2str(trash.fit_exp{c},'%2.2f'), ' LV/h'], ['Gemessen, ', num2str(trash.exp{c},'%2.2f'), ' LV/h'], 'Location', 'west');
set(gca, 'XTick', 1:2:20, 'XTickLabel', 0:2:19)
end

L.EdgeColor = Color_matrix{8};
L.TextColor = Color_matrix{8};
if c==2
    y=ylabel('Wahrscheinlichkeit [%]');
    y.Position(1) = y.Position(1)*1.5;
elseif c==1
    title({'Skalierte Wahrscheinlichkeit der LV-Anfragen um 9:00 eines Werktages';''})
end

set_color(6)
grid off
end



xlabel('#Anfragen zu dieser Uhrzeit')
    
end


%% Check exp amount
close all
figure
hold on
for w=1:3
    e=0;
    for l=[6]
        e=e+1;
        for d= [11]
            plot(Scale_fit.new_lim_exp_am_m(d, :, w, l), ...
                'DisplayName', [Location(l).name, ', ', Scale(1).location(1).daytype(w).name, ', ', num2str(d), ':00 Fit'], ...
                'Color', Color_matrix{w}, 'LineStyle',  Line_matrix{4})
            c=1;
            while c<=numel(Scale) && numel(Scale(c).location(l).daytype(w).mean_amount)>0
%                 trash.exp(c) = Distro_1.location(l).daytype(w).daytime(d).scale(c).new_exp_amount;
                trash.exp(c) = Scale(c).location(l).daytype(w).mean_amount(d);
                c=c+1;
            end
            plot(trash.exp, ...
                'DisplayName', [Location(l).name, ', ', Scale(1).location(1).daytype(w).name, ', ', num2str(d), ':00'], ...
                'Color', Color_matrix{w}, 'LineStyle',  Line_matrix{e})
            trash.exp = [];
        end
    end
end
hold off
legend('show')
xlabel('Scaling of EV amount')
ylabel('#LVs/day/daytime')
title('scaled  expected limited amount')
% Result: perfect match!

%% Check Probabilities
% preparation
for c=1
trash.combi = [9,1,1;15,3,1;
               9,1,4;15,3,4;
               15,2,5;15,3,5];
trash.color = [1 6 2 3 7 4]           
end

for c=1
figure
hold on
for c=1:numel(trash.combi(:,1))
    d=trash.combi(c,1);
    w=trash.combi(c,2);
    l=trash.combi(c,3);
    trash.name{c} = [num2str(d-1), ':00, ', Scale(1).location(l).daytype(w).name  ', ', Scale(1).location(l).name];   
    p(c)=plot(P_fix{trash.combi(c,1), trash.combi(c,2), trash.combi(c,3)},...
        'Color', Color_matrix{trash.color(c)}, ...
        'LineWidth', 4);
    plot([0, numel(P_fix{trash.combi(c,1), trash.combi(c,2), trash.combi(c,3)})], ...
        [P_mean{trash.combi(c,1), trash.combi(c,2), trash.combi(c,3)}, ...
        P_mean{trash.combi(c,1), trash.combi(c,2), trash.combi(c,3)}], '--' ,...
        'Color', Color_matrix{trash.color(c)}, ...
        'LineWidth', 4)          
end
hold off
title(['"Erfolgs"-Wahrscheinlichkeit der Binomialverteilung'])
trash.name_s = ':00, Sonntag, Office & Home';
l=legend(p, trash.name, 'Location', 'east');
l.EdgeColor = Color_matrix{8};
l.TextColor = Color_matrix{8};
set_color(5)
xlabel('Skalierungsfaktor n')
ylabel('Wahrscheinlichkeit P_L_V [%]')
ytix = get(gca, 'YTick');
set(gca, 'YTick',ytix, 'YTickLabel',ytix*100)

end

%% Compare distribution


end


end

