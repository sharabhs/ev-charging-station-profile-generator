function [Charger, All_charger] = get_charger_data(Good)

BA_Test_constants
disp('Get Charger data...')

% get list of charger IDs
Charger_list = sort(unique(Good.data(:,1)));
No_of_Chargers = numel(Charger_list);
Checksum = 0;

% Sort data to Charger
for c=1:No_of_Chargers
    Charger(c).elements = find(Good.data(:,1)==Charger_list(c));
    Charger(c).ID = Charger_list(c);
    Charger(c).headers = Good.headers;
    Charger(c).data = Good.data(Charger(c).elements, :);
    Charger(c).amount = numel(Charger(c).elements);
    Checksum = Checksum + Charger(c).amount;
end

% Total energy per Charger
for c=1:No_of_Chargers
    Charger(c).total_energy = sum(Charger(c).data(:,7));                    % kWh
    Charger(c).mean_energy = Charger(c).total_energy/Charger(c).amount; % kWh
    Charger(c).mean_duration = sum(Charger(c).data(:,8));                   % s    
    Charger(c).mean_power = Charger(c).total_energy/Charger(c).mean_duration*3600;     % kWh  
end    

% daytime profile for every Charger (takes long: 11 min!)
tic
for c=1:No_of_Chargers
    a{c} = Charger(c).elements;
end

for c=1:No_of_Chargers
    Charger(c).D_h = discretize_struct_fast(Good.data(Charger(c).elements,3), 1, 1/24, 0, 'hour');
    Charger(c).Daytime = periodic_course_III(Charger(c).D_h, 'day');
    Charger(c).Weektime = periodic_course_III(Charger(c).D_h, 'week');
    Charger(c).Daytime.period_amount = Charger(c).Daytime.nu_of_periods*ones(24,1);
    Trash.Charger_Matrix(:,:,c) = [Charger(c).Daytime.mean_amount, Charger(c).Daytime.amount];
    show_progress(c ,10, No_of_Chargers)
end


%% Get filled discrete 

Trash.min_x = 1/0;
Trash.max_x = 0;    

% Fill full day
for c=1:255
%     datetime(datestr(Charger(c).D_h.min_x))
%     datetime(datestr(max(Charger(c).full_D_h.x)))
    Trash.miss_d_start = mod(7-2+weekday(datetime(datestr(Charger(c).D_h.min_x))),7);
    Trash.miss_d_end = mod(7+2-1-weekday(datetime(datestr(Charger(c).D_h.max_x))),7);
    Trash.miss_h_start = round((Charger(c).D_h.min_x - floor(Charger(c).D_h.min_x))*24)+24*Trash.miss_d_start;
    Trash.miss_h_end = round((ceil(Charger(c).D_h.max_x) - Charger(c).D_h.max_x)*24)-1 +24*Trash.miss_d_end;
    Charger(c).full_D_h.x = [(Charger(c).D_h.min_x-Trash.miss_h_start/24:1/24:Charger(c).D_h.min_x-1/24)';...
        Charger(c).D_h.x; (Charger(c).D_h.max_x+1/24:1/24:Charger(c).D_h.max_x+Trash.miss_h_end/24)' ]; 
    Charger(c).full_D_h.amount = [zeros(Trash.miss_h_start,1);...
        Charger(c).D_h.amount; zeros(Trash.miss_h_end,1) ]; 
    Charger(c).full_D_h.min_x = min(Charger(c).full_D_h.x);
    Charger(c).full_D_h.max_x = max(Charger(c).full_D_h.x);
end

for c=1:255
    Trash.min_x = min(Trash.min_x, Charger(c).full_D_h.min_x);
    Trash.max_x = max(Trash.max_x, Charger(c).full_D_h.max_x);
end
Trash.min_x = round(Trash.min_x);
Trash.max_x = round(Trash.max_x*24)/24;

Trash.nu_of_rows = round((Trash.max_x - Trash.min_x)*24)+1;
% Trash.daytime = hour(datetime(..
% Trash.max_amount = max(Period.Day_h.amount);

% Get weekday and daytime elements for full discrete array:     
for d=1:24
    Trash.discr_elem{1,d} = sort([d:24*7:numel(Charger(1).full_D_h.x), d+24*1:24*7:numel(Charger(1).full_D_h.x), ...
        d+24*2:24*7:numel(Charger(1).full_D_h.x), d+24*3:24*7:numel(Charger(1).full_D_h.x), ...
         d+24*4:24*7:numel(Charger(1).full_D_h.x)]');
    Trash.discr_elem{2,d} = [d+24*5:24*7:numel(Charger(1).full_D_h.x)]';
    Trash.discr_elem{3,d} = [d+24*6:24*7:numel(Charger(1).full_D_h.x)]';
    Trash.discr_elem{4,d} = [d:24:numel(Charger(1).full_D_h.x)]';
end

%% Repeat Charger arrays -> All same length                                
for c=1:255
    % repeat vector to fit max length
    Trash.start_rep_factor = ceil((Charger(c).full_D_h.min_x-Trash.min_x)/numel(Charger(c).full_D_h.x));
    Trash.rep_factor = ceil(Trash.nu_of_rows/numel(Charger(c).full_D_h.x));
    Charger(c).full_D_h.amount = repmat(Charger(c).full_D_h.amount, Trash.rep_factor,1);
    Charger(c).full_D_h.amount = Charger(c).full_D_h.amount(1:Trash.nu_of_rows);
    Charger(c).full_D_h.x = [Trash.min_x:1/24:Trash.max_x]';
end



%% Mean daytime profile:
% Daytime arrary
All_charger.Daytime.datetime = datetime(datestr(Charger(1).Daytime.datenum, CLOCK_FORMAT), 'Format', CLOCK_FORMAT_DT);
% Mean of mean daily amount of all chargers -> Period amount neglected
All_charger.Daytime.mean_amount = my_nanmean(Trash.Charger_Matrix(:,1,:),3);
% Total daily amount -> should be same as Period.Day_h
All_charger.Daytime.amount = my_nansum(Trash.Charger_Matrix(:,2,:),3);

end

