%% Check idle time

% quick exp

% googl idle
trash.good_idle = (Good.data(Location(Gen.Location).LV_el, 8) - Good.data(Location(Gen.Location).LV_el, 15))/3600;

% Distro for this
trash.idle_dis_test = [];
for w=1:3
    for d=1:24
        trash.occ = sum(Test_simu.data(Test_simu.data(:,16)==w,17)==d);
        trash.idle_dis_test = [trash.idle_dis_test ; Distro_1.location(Gen.Location).daytype(w).daytime(d).idle(rand(trash.occ,1))];
    end 
end

% simu idle
trash.simu_idle = (Test_simu.idle_time)/3600;
trash.simu_idle(trash.simu_idle<0)=0;

figure
hold on
histogram(trash.good_idle, round(max(trash.good_idle))*60,'Normalization','probability', 'DisplayStyle','stairs')
histogram(trash.simu_idle , round(max(trash.simu_idle))*60,'Normalization','probability', 'DisplayStyle','stairs')
histogram((trash.idle_dis_test)/3600, round(max((trash.idle_dis_test)/3600))*60,'Normalization','probability', 'DisplayStyle','stairs')
hold off
legend('Good', 'Simu', 'dis')