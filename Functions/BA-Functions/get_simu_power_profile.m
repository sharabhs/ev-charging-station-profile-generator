function [Output_profile, Input] = get_simu_power_profile(Input)

%% Units:
% Time:      |  s
% Power:     |  kW
% Energy:    |  kWh
% slope:     |  kW/s

%% Example data
% Input = Simu;

% clear Intern Cases false false_e nu_of_false Output_profile rounded_duration_m start_elements end_elements specific_time Power_bools 
% clear Power_bools_headers P_slope t t_m t_f_s T T_m P_max P_min a Output_profile Power_course m_end m_start actual_duration LV_power Input
% clear Power_nu_of_bools P_mean 

%% Load constants
BA_Test_constants

tic;

disp('get_power_profiles_fast...')

%% Input Data Headers
%     {'(1) ChargingstationID'     }
%     {'(2) UserID'                }
%     {'(3) Starttime[d]'          }
%     {'(4) Endtime[d]'            }
%     {'(5) Meterstart'            }
%     {'(6) Meterend'              }
%     {'(7) Totalchargeamount[Wh?]'}
%     {'(8) Totalduration[s]'      }
%     {'(9) ConnectorID'           }
%     {'(10) Mean Power [kW]'      }
%     {'(11) Peak Power [kW]'      }
%     {'(12) Power_class'          }

%% Settings
TOLERANCE = 0.006;    % * max power -> kW

%% Constants



for c=1:Power_classes.nu_of_classes
    Power_classes.full_slope_profile{c} = linspace(Power_classes.max(c), ...
        Power_classes.min(c), round(Power_classes.full_slope_time(c)/60))';  % kW, 1 element per minute
end


%% Apply to whole Input data
% Input data
Intern.duration = Input.data(:,8);                                          % s
[Intern.nu_of_rows, ~] = size(Input.data);
Intern.energy = Input.data(:,7);                                            % kWh
Intern.datetime(:,1) = datetime(datestr(round(Input.data(:,3)*24*60)/24/60), 'Format', TIME_FORMAT_DT); % datetime, rounded to full minutes
Intern.datetime(:,2) = datetime(datestr(round(Input.data(:,4)*24*60)/24/60), 'Format', TIME_FORMAT_DT);

% independant of charge duration
Intern.max_power = Power_classes.max(Input.data(:,12));                     % kW
Intern.min_power = Power_classes.min(Input.data(:,12));                     % kW
Intern.power_diff = Power_classes.diff(Input.data(:,12));                   % kW
Intern.slope = Power_classes.slope(Input.data(:,12));                       % kW/s
Intern.full_slope_time = Power_classes.full_slope_time(Input.data(:,12));     % s
Intern.full_slope_energy = Power_classes.full_slope_energy(Input.data(:,12)); % kWh

% dependant of charge duration
% boolean = 1, if  duration is longer than the max slope time
Intern.longer_than_full_slope_time = Intern.duration > Intern.full_slope_time;
% min Power bias energy
Intern.min_p_energy = Intern.min_power.*Intern.duration/3600;               % kWh
% unlimited slope energy as function of duration (including min power)
Intern.inf_slope_energy = (Intern.min_power.*Intern.duration + 1/2*Intern.slope.*Intern.duration.^2)/3600; % kWh
% limit slope energy as function of duration to max slope energy
Intern.slope_energy = Intern.inf_slope_energy; % kWh
too_large_slope_elementes = find(Intern.slope_energy>Intern.full_slope_energy);
Intern.slope_energy(too_large_slope_elementes) = Intern.full_slope_energy(too_large_slope_elementes);
% Energy over constant power phase, if exists
Intern.const_p_energy = Intern.longer_than_full_slope_time .* Intern.max_power .* (Intern.duration-Intern.full_slope_time)/3600; % kWh
% Full  energy over duration in case , if battery fully charged -> slope + const power (if exists)
Intern.full_energy = Intern.slope_energy + Intern.const_p_energy; % kWh
% Max energy, in case of constant power charging only
Intern.max_energy = Intern.max_power .* Intern.duration / 3600; % kWh


%% Allocation of Charging Cases

Intern.case = Input.data(:,13);

% Distribution over cases:
Cases.distribution = count_same_integers(Intern.case);

% Intern.case = Intern.case';
%% Case elements
for c=1:max(Intern.case)
    Intern.case_elements{c} = find(Intern.case==c);
end

%% Times of charge process

Output_profile.datetime_step = minutes(1);
Output_profile.datetime_start = min(Intern.datetime(:,1)); % datetime
Output_profile.datetime_end = max(Intern.datetime(:,2)); % datetime
Output_profile.datetime = [Output_profile.datetime_start:Output_profile.datetime_step:Output_profile.datetime_end+Output_profile.datetime_step]'; % 
Output_profile.nu_of_rows = numel(Output_profile.datetime);
Output_profile.datenum = datenum(Output_profile.datetime);

rounded_duration_m = round(Intern.duration/60); % min
start_elements = round((Input.data(:,3)-min(Input.data(:,3)))*24*60); % -> min, rounded to full min
end_elements = start_elements + rounded_duration_m-1; % -> min

specific_time = zeros(Intern.nu_of_rows,1);

specific_time(Intern.case_elements{1}) = rounded_duration_m(Intern.case_elements{1})*60; % s, rounded to min
specific_time(Intern.case_elements{2}) = rounded_duration_m(Intern.case_elements{2})*60; % s, rounded to min
specific_time(Intern.case_elements{3}) = rounded_duration_m(Intern.case_elements{3})*60 - ...
    round(Power_classes.full_slope_time(Input.data(Intern.case_elements{3},12))/60)*60; % s, rounded to min
specific_time(Intern.case_elements{4}) = rounded_duration_m(Intern.case_elements{4})*60; % s, rounded to min
specific_time(Intern.case_elements{5}) = rounded_duration_m(Intern.case_elements{5})*60 - ...
    sqrt(2./Intern.slope(Intern.case_elements{5}).*(Intern.max_power(Intern.case_elements{5})...
    .*Intern.duration(Intern.case_elements{5})-Intern.energy(Intern.case_elements{5})*3600));
% Problems: 
% Case 3: full slope time sometimes significantly higher than duration
% Case 5: Duration smaller than const power time
% TODO:  Limit Energy-Duration Distribution.

%% DEBUGGING
if DEBUG
rounded_duration_m(e)*60 - ...
    sqrt(2./Intern.slope(e).*(Intern.max_power(e)...
    .*Intern.duration(e)-Intern.energy(e)*3600));
e=find((specific_time)<0);

[specific_time(e), Intern.case(e), Intern.duration(e), Intern.energy(e), Intern.max_power(e)]
end

%% Calculate Power profile
% Flags to activate power terms
Power_bools_headers = {'Constant Power Phase', 'Finished but short slope', ...
    'interrupted short slope', 'interrupted long slope', 'full slope', 'zeros'}; 
Power_nu_of_bools = numel(Power_bools_headers);
Power_bools(Intern.case_elements{1},:) = repmat(logical([1,0,0,0,0,0]), numel(Intern.case_elements{1}),1); % logicals (bools)
Power_bools(Intern.case_elements{2},:) = repmat(logical([0,1,0,0,0,1]), numel(Intern.case_elements{2}),1);
Power_bools(Intern.case_elements{3},:) = repmat(logical([1,0,0,0,1,1]), numel(Intern.case_elements{3}),1);
Power_bools(Intern.case_elements{4},:) = repmat(logical([0,0,1,0,0,0]), numel(Intern.case_elements{4}),1);
Power_bools(Intern.case_elements{5},:) = repmat(logical([1,0,0,1,0,0]), numel(Intern.case_elements{5}),1);

Power_course = zeros(numel(Output_profile.datetime), 1);


% e=find(specific_time<0)
% Intern.case(e)
% numel(e)
% round(Intern.energy(e)-Intern.full_slope_energy(e))

Intern.full_slope_profile = cell(Intern.nu_of_rows, 1);
Intern.full_slope_profile(:) = {0};
Intern.full_slope_profile(Intern.case_elements{3}) =  Power_classes.full_slope_profile(Input.data([Intern.case_elements{3}],12));  % kW, 1 element per minute

numel(Intern.full_slope_profile);
% copy to short name vaiables
P_slope = Intern.full_slope_profile;
max(Intern.case_elements{3});
t=specific_time;
t_m=round(specific_time/60);
t_f_s = Intern.full_slope_time;
T=Intern.duration; % s
T_m = round(T/60);    
P_max = Intern.max_power;
P_min = Intern.min_power;
P_mean = Input.data(:,10);
a = Intern.slope;
Output_profile.datenum_start = datenum(Output_profile.datetime_start);
m_start = round(datenum(Intern.datetime(:,1)-Output_profile.datetime_start)*24*60)+1;
m_end = round(datenum(Intern.datetime(:,2)-Output_profile.datetime_start)*24*60)+1;

% Loop trough LVs
Iterations = Intern.nu_of_rows;
% Iterations = 100;
d=0;

for c=1:Iterations

    % actual duration
    Input.data(c,15) = t(c) + Power_bools(c,5)*t_f_s(c) + Power_bools(c,4)*(T(c)-t(c)); % s
    actual_duration(c) = round(Input.data(c,15)/60);  % min
    
    if actual_duration(c) <2
        if actual_duration(c)==0 actual_duration(c)=1; end
        LV_power{c} = [P_mean(c)*T_m(c); zeros(T_m(c)-actual_duration(c),1)];
        d=d+1;
        el(d)=c;
    else
        LV_power{c} = [ P_max(c)*ones(Power_bools(c,1)*(t_m(c),1);...
                        linspace(P_min(c)+a(c)*T(c), P_min(c), Power_bools(c,2)*T_m(c))';...
                        linspace(P_mean(c)+0.5*a(c)*T(c), P_mean(c) - 0.5*a(c)*T(c), Power_bools(c,3)*T_m(c))'; ...
                        linspace(P_max(c), P_max(c)-0.5*a(c)*(T(c)-t(c)), Power_bools(c,4)*(T_m(c)-t_m(c)))'; ...
                        P_slope{c}];
    end
    LV_power{c}(LV_power{c}==0) = [];
    LV_power{c} = [LV_power{c}; 0];
    
    % Allocate time vectors to LV power profiles
    LV_time{c} = [m_start(c):m_start(c)+actual_duration(c)]';

    % add to Power_course vector
    Power_course(LV_time{c}) = Power_course(LV_time{c}) + LV_power{c};
    
    LV_time{c} = LV_time{c}/60/24 + Output_profile.datenum_start;
    
    show_progress(c, 1000, Iterations);
 
end

Input.headers{15} = '(15) Actual Duration [s]';
Input.units{15} = '[s]';
[Input.no_of_rows, Input.no_of_cols] = size(Input.data);

%% Quick plot 
% figure
% hold on
% % for c=1:Iterations
% %     plot(LV_power{c})
% % %     pause(2)
% %     
% % end
% plot(Power_course)
% hold off
% xlabel('minutes')
% ylabel('power kW')

%% Check Energy
check_e_two = 0;
for c=1:numel(LV_power)
    check_energy(c) = trapz(LV_power{c})/60; % kWh
    check_e_two(c) = mean(LV_power{c})/60*numel(LV_power{c}); % kWh
end
check_energy = check_energy';
check_e_two = check_e_two';

sum(check_e_two);
sum(check_energy);

% check_e_two;
% sum(Intern.energy)
% check_e_two-sum(Intern.energy);
% e_dev = Intern.energy - check_energy;
% max(e_dev)
% min(e_dev)
% mean(e_dev)
% sum(e_dev)
% sum(check_energy)
% %% Plot Check energy
% close all
% figure
% hold on
% plot(check_e_two)
% plot(check_energy)
% plot(Intern.energy)
% hold off
% legend('check 2', 'Check 1 (trapz)', 'actual')


%% test root substitute
% slope = Power_classes.slope(1:2);
% min_power = Power_classes.min(1:2);
% energy = Intern.energy(1:2);
% 
% alt = (sqrt(energy+1./(2*slope).*min_power.^2)-1./sqrt(2*slope).*min_power).*sqrt(2./slope);

%% Hand data over to output struct
Output_profile.data = Power_course;
Output_profile.single_profiles_power = LV_power'; % kW
Output_profile.single_profiles_time = LV_time'; % datenum
Output_profile.check_energy_trapz = check_energy; % kWh
Output_profile.check_energy_rect = check_e_two; % kWh
Output_profile.width =1/24/60;
Input.data(:,13) = Intern.case;
Input.data(:,14) = specific_time;
Input.headers{13} = '(13) Charge case';
Input.headers{14} = '(14) Specific time [s]';
Input.units{13} = '[]';
Input.units{14} = '[s]';
Input.cases.elements = Intern.case_elements';
Input.cases.names = Cases.names;
Input.cases.nu_of_cases = Cases.nu_of_cases;
Input.cases.distribution = Cases.distribution;

t=toc;
disp(['Second ', num2str(t,4), ', done'])
% TODO: 
% - calculate total deviation in energy -> fine
% - get profiles per charging station (and standard deviation)
% - k-mean clustering
% - get capacity, state of charge,
% - get average profile per charging case
% - hand case over to Good.data





end

