function [number] = next_file_number(folder)
    % Read the numbers of all files *_NUMBER.* in the directory and return the next largest number
    
    % cd
    if exist('folder') && numel(folder)~=0
        old_loc = cd(folder);
    end
    
    % read file headers
    Files = dir;
    
    % Ignore directories
    for c=1:numel(Files)
        trash.isdir(c,1) = Files(c).isdir;
    end
    Files = Files(~trash.isdir);
    
    % get file numbers (Format: *_NUMBER.*)
    if numel(Files)>0
        for c=1:numel(Files)
            dot = max(strfind(Files(c).name, '.'));
            underscore = max(strfind(Files(c).name, '_'));
            if dot-underscore>1
                number(c,1) = str2num(Files(c).name(underscore+1:dot-1));
            else
                number(c,1) = 0;
            end
        end
    else
        number = 0;
    end
    
    number = max(number)+1;
    
    % return to old dir
    if exist('folder') && numel(folder)~=0
        cd(old_loc);
    end
end

