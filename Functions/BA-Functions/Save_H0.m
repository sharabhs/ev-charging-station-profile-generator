%% create H0 csv
RESOLUTION = 10;
trash.number = 365*24*60;
% Summertime or UTC?
trash.SUMMERTIME = false;

% accumulate phases
Raw_H0.all_phases = Raw_H0.PL1(1:trash.number,:) + Raw_H0.PL2(1:trash.number,:) + Raw_H0.PL3(1:trash.number,:);

% get info
Raw_H0.info.start = Raw_H0.datetime(1);
Raw_H0.info.end = Raw_H0.datetime(end);
Raw_H0.info.nu_of_days = datenum(Raw_H0.info.end)-datenum(Raw_H0.info.start);
Raw_H0.info.power_unit = '[W]';
Raw_H0.info.time_unit = '[min]';

% Summertime
if trash.SUMMERTIME
    Raw_H0.datetime.TimeZone = 'Europe/Berlin';
end

% [a,b] = max(diff(Raw_H0.datetime(:,1)))
% Raw_H0.datetime(b-10:b+10,1)

% % save to csv
Raw_H0.datestr = datestr(Raw_H0.datetime(1:trash.number), 'YYYY-mm-DD hh:MM:ss');
% for c=1:Raw_H0.nu_of_cols
%     trash.save_name = ['H0-accumulated-',num2str(c),'.txt'];
%     % save
%     csvwrite(trash.save_name, {Raw_H0.datestr(1:10,:), Raw_H0.all_phases(1:10,c)})
%     show_progress(c,1,Raw_H0.nu_of_cols)
% end
% 
% disp('done')

%% Create new profiles
trash.shift = 15; % min
% figure 
% hold on
% for c=5:5
% plot(Raw_H0.datetime(1:trash.number), Raw_H0.all_phases(1:trash.number,c), 'DisplayName', num2str(c))
% plot(Raw_H0.datetime(1:trash.number), [zeros(trash.shift,1); Raw_H0.all_phases(1:trash.number-trash.shift,c)], 'DisplayName', 'shifted')
% plot(Raw_H0.datetime(1:trash.number), Raw_H0.all_phases(1:trash.number,c)+[zeros(trash.shift,1); Raw_H0.all_phases(1:trash.number-trash.shift,c)], 'DisplayName', 'sum')
% end
% hold off
% legend show


for c=1:100-Raw_H0.nu_of_cols
    Raw_H0.all_phases(:,c+Raw_H0.nu_of_cols) = [zeros(trash.shift,1); Raw_H0.all_phases(1:end-trash.shift,c)];
end


[Raw_H0.nu_of_rows, Raw_H0.nu_of_cols] = size(Raw_H0.all_phases);

%% Mean 

for c=1:Raw_H0.nu_of_cols
Raw_H0.all_phases_mean(:,c) = mean(reshape(Raw_H0.all_phases(:,c),RESOLUTION, []))';
end
%% Save

tic
disp('Save profile ...')
% cd ../../H0/Accumulated/1day  
parfor c=1:Raw_H0.nu_of_cols
    save_name{c} = ['H0-day-accumulated-phases-',num2str(c),'.csv'];

    fid = fopen(save_name{c}, 'w') ;
    for d = 1:numel(Raw_H0.datestr(1:trash.number,1))/RESOLUTION % Loop through each time/value row
        fprintf(fid, '%s;', Raw_H0.datestr((d-1)*RESOLUTION+1,:)) ; % Print the time string
        fprintf(fid, '%12.6f \n', double(Raw_H0.all_phases_mean(d,c))*10^-6) ; % Print the data values
    end
    fclose(fid);
    show_progress(c, 1, Raw_H0.nu_of_cols)
end
cd ../../Datenverarbeitung/Skripte

%% Save only new
tic
disp('Save profile ...')
% cd ../../H0/Accumulated/1day  
cd Accumulated/1year
for c=75:Raw_H0.nu_of_cols
    save_name{c} = ['H0-day-accumulated-phases-',num2str(c),'.csv'];

    fid = fopen(save_name{c}, 'w') ;
    for d = 1:numel(Raw_H0.datestr(1:trash.number,1)) % Loop through each time/value row
        fprintf(fid, '%s;', Raw_H0.datestr(d,:)) ; % Print the time string
        fprintf(fid, '%12.6f \n', double(Raw_H0.all_phases(d,c))*10^-6) ; % Print the data values
    end
    fclose(fid);
    show_progress(c, 1, Raw_H0.nu_of_cols)
end
