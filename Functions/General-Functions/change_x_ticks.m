% change label ticks
function [] = change_x_ticks(TICKS, MAX_HOUR, STEPS_PER_HOUR, TYP) 
% Example data:
% TICKS = 6;
% MAX = 24;
% STEPS = 
% TYP ='week';

    if ~exist('TYP')
        
        for d=1:TICKS+1
            label{d} = [num2str((d-1)*MAX_HOUR/TICKS) ':00'];
        end
        
        set(gca, 'XTick', 0:MAX_HOUR/TICKS*STEPS_PER_HOUR:MAX_HOUR*STEPS_PER_HOUR, 'XTickLabel', label)
        
    elseif strcmp(TYP, 'week')
        
        label = {'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'};
        set(gca, 'XTick', STEPS_PER_HOUR/2:STEPS_PER_HOUR:6.5*STEPS_PER_HOUR, 'XTickLabel', label)
        
    end

end
