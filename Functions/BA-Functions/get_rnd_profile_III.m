function [Simu] = get_rnd_profile_III(Days, Resolution_m, ScaleFactor, Distro_1, Loc)
%   Generate statistical power profile for an examplary charging station   
    
%   Input:
%       - Duration [days] (full days only!)
%       - Resolution [days]
%       - Scale Factor [] (Scale amount of electric vehicles on street)
%     %%
%    clear Intern P_max P_mean P_min P_slope a LV_power LV_time Power_course el Simu Simu_profile

    %% Example Data                                                        
%     clear Intern Trash Simu
%     Days = 365;                                                           % [days]
%     Resolution_m = 1;                                                       % [min]
%     ScaleFactor = 10;
%     Loc = 2;
   

    %% Preparation                                                         
    
    set(0,'DefaultFigureVisible','off');
    disp('Generate random profile...')
    
    % start timer
    tic
    
    % Load data
    BA_Test_constants
    
    % Check duration input
    if ~(round(Days)==Days)
        disp('Error in get_rnd_profile: Duration is not in full days!')
        return
    end
    
    % Check Resolution Input
    if Resolution_m < 1 || Resolution_m > 24*60
        disp('ERROR: Resolution not suitable. Chose resolution in minutes [1...24*60 min]')
    end
    
    % Check Scale Input
    if ScaleFactor>Distro_1.location(Loc).max_scale
        disp(['ERROR! Choose a Scale smaller than: ', num2str(Distro_1.location(Loc).max_scale+1)])
    end
    
    % Convert Resoltuion to datenum
    Resolution = Resolution_m*1/24/60;
     
    % Time Array:
    Intern.start_date = datenum(datestr('01-01-2020, 00:00:00'));  % day (mm-dd-yyyy, .. )!!!
    Intern.datenum = [Intern.start_date:Resolution:Intern.start_date+Days-Resolution]'; % day, in Res steps
    Intern.datenum_h = [Intern.start_date:1/24:Intern.start_date+Days-1/24]'; % day, in hour steps
    
    % Hourly time array
    Intern.hours = repmat([0:23]', Days,1);
    
    % hourly array with number of rejected cars
    Intern.rejected = zeros(Days*24,1);
    
    % Current amount of LV and idle cars per second
    Intern.current_amount = zeros(Days*24*3600,1);
    Intern.idle_amount = Intern.current_amount;
    
    % Type of day array (7&1: Sa&So)
    Intern.start_weekday = weekday(Intern.start_date)-1;
    Trash2.daytype = mod(floor(Intern.datenum_h-Intern.datenum_h(1))+Intern.start_weekday,7)+1;
    % Weekday
    Intern.daytype(find(Trash2.daytype == 2 | ...
                        Trash2.daytype == 3 | ...
                        Trash2.daytype == 4 | ...
                        Trash2.daytype == 5 | ...
                        Trash2.daytype == 6)) = 1;
    % Weekend
    Intern.daytype(find(Trash2.daytype == 7)) = 2;
    Intern.daytype(find(Trash2.daytype == 1)) = 3;
    
    
    Trash.index = 0;
    Trash.rej = zeros(24,3);
    
    % Patience of EV in seconds -> cars will wait untill LP is free
    Intern.PATIENCE = 60*5;
    
    Intern.start_time = [];
   
    
    %% Prepare distribution matrices                                       
    % Amount
    for w=1:4
        for d=1:24
            Intern.amount_distr(:,d,w) = [0; cumsum(Distro_1.location(Loc).daytype(w).daytime(d).scale(ScaleFactor).new_lim_amount)];
        end
    end
    
    % Power Classes
    Intern.p_c_distr = [zeros(24,1), cumsum(Distro_1.pc_mat_dt_norm(:,2:end),1)'];
    Intern.p_c_distr = Intern.p_c_distr ./ repmat(Intern.p_c_distr(:,end),1,8);
    % TODO: weekday seperation
    
    Trash.amount_mat = [0;0;1;2];
    
    
    %% -------------------------- Generate LVs ----------------------------
    
    % Loop through every Hour
    tic
    for h=1:numel(Intern.hours)
        
        %% Preparation                                                     
        Trash.daytime = Intern.hours(h);
        w = Intern.daytype(h);
        
        %% Get number of cars that want to charge                          
        [Trash.rounded{h}, Trash.uel] = unique(Intern.amount_distr(:,Trash.daytime+1,w));
        Trash.rnd_amount(h) = sum(Trash.amount_mat(Trash.uel) .* (Trash.rounded{h}==roundtowardvec(rand(1), Trash.rounded{h}, 'ceil')));
        % Control result
        if Trash.rnd_amount(h)>2
            h
            Intern.hours(h)
            Intern.daytype(h)
            Intern.amount_distr(:,Trash.daytime+1,w)
        end

        % distribute their arrival over 1 h
        Trash.start_time = sort(h-1 + round(rand(Trash.rnd_amount(h),1)*3600)/3600);        % hours, rounded to sec, randomly distributed over one hour
        Trash.new_amount = 0;
        Trash.clear_el = [];
        
        % check if only 1 result
        if numel(Trash.start_time)>2 disp('error'); end
        
        %% Reject cars, if LPs occupied     
        % Loop through every requested LV
        for c=1:numel(Trash.start_time)
            % If LP free
            if Intern.current_amount(round(Trash.start_time(c)*3600))+ floor(c/2) < numel(Trash.start_time)
                Trash.new_amount = Trash.new_amount + 1; 
            % If LP free after short waiting time (PATIENCE)    
            elseif Intern.current_amount(round(Trash.start_time(c)*3600+Intern.PATIENCE))+ floor(c/2) < numel(Trash.start_time) && Trash.start_time(c)*3600+Intern.PATIENCE <1
                Trash.start_time(c) = Trash.start_time(c) + Intern.PATIENCE;
                Trash.new_amount = Trash.new_amount + 1; 
            % if all LP occupied    
            else
                Trash.clear_el = [Trash.clear_el,c];
            end
        end
        Trash.start_time(Trash.clear_el) = [];
        
        Intern.start_time =  [Intern.start_time; Trash.start_time]; 
        
        % count rejected cars
        if Trash.new_amount~=Trash.rnd_amount(h)
            Trash.rej(Trash.daytime+1,w) = Trash.rej(Trash.daytime+1,w) + Trash.rnd_amount(h) - Trash.new_amount;
            Intern.rejected(h) =  Trash.rnd_amount(h) - Trash.new_amount;
%             disp('LP occupied -> car rejected')
        end
        % TODO: get from duration
        
        % check if more than 2 cars
        if numel(Trash.index+1:Trash.index+Trash.new_amount)>2 disp('error2'); end
        
        %% Loop through all new LVs -> get Energy, Duration & Power class  
        
        for c=Trash.index+1:Trash.index+Trash.new_amount

            %% Get Power classes                                           
            Intern.power_classes(c,1) = find(Intern.p_c_distr(Trash.daytime+1,:)==...
                roundtowardvec(rand(1),Intern.p_c_distr(Trash.daytime+1,:), 'ceil'),1)-1;
          
            %% Get Duration and Energy                                     
            % Energy & Duration
            if max(Distro_1.daytime(Trash.daytime+1).power(Intern.power_classes(c)).e) > 0
                [Intern.energy(c,1), Intern.duration(c,1)] = pinky(...
                    Distro_1.daytime(Trash.daytime+1).power(Intern.power_classes(c)).e, ...
                    Distro_1.daytime(Trash.daytime+1).power(Intern.power_classes(c)).d, ...
                    Distro_1.daytime(Trash.daytime+1).power(Intern.power_classes(c)).e_d_distr');      % kWh, s
            else  
                Intern.energy(c,1) = 0;
                Intern.duration(c,1) = 0;
                disp('Warning! Energy & Duration Distribution = 0')
                Trash.daytime+1
                Intern.power_classes(c)
            end
            
            %% Get Idle time                                               
            % if charge case allows it
            if idle_or_not(Intern.duration(c,1), Intern.energy(c,1), Intern.power_classes(c,1))
                Intern.idle_time(c,1) = Distro_1.location(Loc).daytype(w).daytime(Trash.daytime+1).idle(rand(1));
            else
                Intern.idle_time(c,1) = 0;
            end
            
            %% Get current amount array                                    
            % get current amount according to LV's duration, adding idle time
            Trash.dur_el = ceil(Intern.start_time(c)*3600+Intern.duration(c)+Intern.idle_time(c));
            % prohibit elements outside cuurent amount array
            Trash.dur_el(Trash.dur_el>numel(Intern.current_amount)) = [];
            % Add car to current amount array
            trash.curr_el = round(Intern.start_time(c)*3600):Trash.dur_el;
            Intern.current_amount(trash.curr_el) = Intern.current_amount(trash.curr_el)+1;
            % Add idle car to idle_amount array
            if round(Intern.start_time(c)*3600+Intern.duration(c)) <= numel(Intern.idle_amount)
                trash.idle_el = round(Intern.start_time(c)*3600+Intern.duration(c)):Trash.dur_el;
                Intern.idle_amount(trash.idle_el) = Intern.idle_amount(trash.idle_el)+1;
            end
            
            % Start time array in Resolution, = sec/min/... till start_date
        %     Intern.start_time_res = round((Intern.start_time - Intern.start_date)/Resolution);  % Res

            % Check time
        %     datetime(datestr(Intern.start_hour, TIME_FORMAT), 'Format', TIME_FORMAT_DT)
%             Intern.start_time = [];

        end
        
        %% Get index of LV                                                 
        Trash.index = Trash.index + Trash.new_amount;
        
        show_progress(h, 100, numel(Intern.hours));
        
    end
    
    
    %% Remove zero energy and duration data                                
    if numel(find(Intern.energy==0))>0
        Trash.zero_elements = find(Intern.energy==0);
        Intern.start_time(Trash.zero_elements) = [];
        Intern.duration(Trash.zero_elements) = [];
        Intern.energy(Trash.zero_elements) = [];
        Intern.power_classes(Trash.zero_elements) = [];
        Intern.idle_time(Trash.zero_elements) = [];
    end
    if numel(find(Intern.duration==0))>0
        Trash.zero_elements = find(Intern.duration==0);
        Intern.start_time(Trash.zero_elements) = [];
        Intern.duration(Trash.zero_elements) = [];
        Intern.energy(Trash.zero_elements) = [];
        Intern.power_classes(Trash.zero_elements) = [];
        Intern.idle_time(Trash.zero_elements) = [];
    end
    
    
    %% Create actual profile                                               
    
    % prepare data set:
    Simu.data(:,3) = Intern.start_time/24+Intern.start_date;                % datenum
    Simu.data(:,4) = Simu.data(:,3) + Intern.duration/24/3600;              % datenum
    Simu.data(:,7) = Intern.energy;                                         % kWh 
    Simu.data(:,8) = Intern.duration+Intern.idle_time;                      % s
    Simu.data(:,12)= Intern.power_classes;                                  % 
    Simu.data(:,10)= Intern.energy*3600./Intern.duration;                   % kW
%     Simu.data(:,15)= Intern.duration;                                       % s
    [Simu.no_of_rows, Simu.no_of_cols] = size(Simu.data);
    
    
    % Get Actual Power
    [Actual_profile, Simu] = get_power_profiles(Simu, Resolution_m);                 % rounded to Res
    
    Simu.actual_power = Actual_profile;
    
    % Check actual duration
    Simu.act_d_dev = abs(Simu.data(:,15)-Intern.duration);
    Simu.idle_time = Intern.idle_time;
    
    
    %% Get Daytype and time of day                                         
    % Get datetime
    Simu.start_datetime = datetime(datestr(Simu.data(:,3), TIME_FORMAT), 'Format', TIME_FORMAT_DT);

    % Daytype (1:Weekday, 2: weekend)
    Simu.data(:,16) = ones(Simu.no_of_rows,1);
    Simu.headers{16} = '(16) Daytype';
    Simu.units{16} = '1:Weekday, 2:Saturday, 3:Sunday';
    Trash.sat_elm = find(weekday(Simu.start_datetime)==7);
    Trash.sun_elm = find(weekday(Simu.start_datetime)==1);
    Simu.data(Trash.sat_elm,16)=2*ones(numel(Trash.sat_elm),1);
    Simu.data(Trash.sun_elm,16)=3*ones(numel(Trash.sun_elm),1);

    % Time of day
    Simu.data(:,17) = mod(floor(Simu.data(:,3)*24),24);
    Simu.headers{17} = '(17) Daytime';
    Simu.units{17} = 'o`clock';
    
    
    %% Get Utilization                                                     
    
    Simu.info.mean_current_amount_s = mean(reshape(Intern.current_amount, 24*3600, Days),2);
    Simu.info.mean_current_amount_dt = mean(reshape(Simu.info.mean_current_amount_s, 3600, 24),1);
    Simu.info.mean_current_amount_hour = max(reshape(Intern.current_amount, 3600, 24*Days),[],1);
    Simu.info.mean_current_amount_dt_hour = mean(reshape(Simu.info.mean_current_amount_hour,24,Days),2);
    
    Simu.info.rejections_per_dt_w = Trash.rej ./ repmat([Days/7*5, Days/7, Days/7],24,1);
    Simu.info.total_rejections_dt_w = Trash.rej;
    Simu.info.total_rejections = sum(sum(Trash.rej));
    Simu.info.rejections_per_day_w = sum(Trash.rej) ./ [Days/7*5, Days/7, Days/7];
    
    Simu.info.rejections_per_dt = sum(Trash.rej,2) / Days;
    Simu.info.total_rejections_dt = sum(Trash.rej,2);
    Simu.info.rejections_per_day = Simu.info.total_rejections / Days;
    

    Simu.info.max_occupation_perc = 100*max(Simu.info.mean_current_amount_s)/2;
    Simu.info.max_rejection_perc = max(max(Simu.info.rejections_per_dt_w))*200;
    
    
    %% Get general Info                                                    
    
    % time
    Simu.info.earliest_start = datetime(datestr(min(Simu.data(:,3))));
    Simu.info.latest_end = datetime(datestr(max(Simu.data(:,4))));
    Simu.info.nu_of_days = days(Simu.info.latest_end-Simu.info.earliest_start);
    
    % amount
    Simu.info.nu_of_LVs = numel(Simu.data(:,1));
    Simu.info.nu_of_LSs = numel(unique(Simu.data(:,1)));
    for w=1:3
        Simu.info.daytype(w).mean_amount = numel(find(Simu.data(:,16)==w))/(Simu.info.nu_of_days/7);
    end
    Simu.info.daytype(1).mean_amount = Simu.info.daytype(1).mean_amount / 5;
    Simu.info.daytype(4).mean_amount = numel(Simu.data(:,16))/Simu.info.nu_of_days;
    
    % Get new size of data matrix
    [Simu.no_of_rows, Simu.no_of_cols] = size(Simu.data);
    
    
    %% Feedback:                                                           
    disp(['Cars rejected: ', num2str(sum(Trash.rej))])
    Simu.current_amount = Intern.current_amount;
    Simu.idle_amount = Intern.idle_amount;
    Simu.rnd_amount = Trash.rnd_amount';
    Simu.reject_amount = Intern.rejected;
    
    set(0,'DefaultFigureVisible','on');
    
    
end

    
    %% Discretize And get Periodic Profile                                 
    
%     Intern.D_hour = discretize_struct_fast(Intern.start_time/24+Intern.start_date, [1], 1/24, 0, 'days');
%     % peridoify:
%     Intern.D_hour.Daytime = periodic_course_III(Intern.D_hour, 'day');
%     
    
    %% Check Result                                                        
% % total amount of LV:
% Trash.index
% % rejected LVs:
% Trash.rej
    
    
%     %% Check Power profile
%     set(0,'DefaultFigureVisible','on');
%     figure
%     plot(Simu_profile.datenum, Simu_profile.data)
%     xlabel('Time')
%     ylabel('Power [kW]')
%     title('Random charger profile')
%     
%     

    %% Check daztime profile                                               
%     figure
%     hold on
%     plot(datetime(datestr(Daytime.datenum, CLOCK_FORMAT), 'Format', CLOCK_FORMAT_DT), All_charger.Daytime.amount)
%     plot(datetime(datestr(Intern.D_hour.Daytime.datenum, CLOCK_FORMAT), 'Format', CLOCK_FORMAT_DT), Intern.D_hour.Daytime.amount*255)
%     hold off
%     legend('measured', 'Simulated')
%     xlabel('Time')
%     ylabel('Mean #LV per day')
%     % Result: quite ok, just a littl bit less, and 9:00 peak not that
%     % intense...
%     
%     %% Check Case Distro
%     figure
%     pie(Simu.cases.distribution(:,2), Good.cases.names)
%     % Result: not correct zet: 5 too large, 3 too small
%     
%     %% Check power class distro
%     
%     Simu.power.distribution = count_same_integers(Simu.data(:,12), 1,7);
%     figure
%     pie( Simu.power.distribution(:,2), Power_classes.names)
%     %Result: fits verz well!
%     
%     %% Check Energy distribution
%     figure
%     plot(Intern.D_hour.Daytime.datetime, Intern.D_hour.Daytime.mean(:,2))
%     ylabel('Energz [kWh]')
%     % Result: Similar magnitude,sometimes large deviations due to small
%     % sample size
%     % Should be between 10 and 18 kWh
%     
%     %% Check Actual Duration Distribution
%     figure
%     plot(Intern.D_hour.Daytime.datetime, Intern.D_hour.Daytime.mean(:,3)/3600)
%     ylabel('Duration [h]')
%     % Result: Similar Magnitude, sometimes large deviations due to small
%     % sample size
%     % should be between 1 and 2 h
%     
%     %% Check results
%     close all
%     % Amount of LV
%     figure
%     hold on
%     bar(Intern.amount_per_hour)
%     plot(D_Hour.amount)
%     errorbar(All_charger.Daytime.mean_amount*ScaleFactor, All_charger.Daytime.std_dev_amount*ScaleFactor)
%     hold off
%     legend('Rnd', 'Exp', 'Exp+e')
%     % Result: Std Dev to small
%     
%     % Distribution of LV amounts per daytime for one charger
%     figure
%     histogram(my_nansum(Charger(200).D_hour.Three_d,1))
%     
%     % Distribution of Amount of LVs per daytime for all chargers
%     start_hour = hour(datetime(datestr(D_Hour.x(1))));
%     for c=1:24
%     figure
%     daytime=c;
%     Check.LV_distro.daytime(c).mean_amount = mean(D_Hour.amount(daytime+24-start_hour:24:end));
%     Check.LV_distro.daytime(c).mean_amount_dev = std(D_Hour.amount(daytime+24-start_hour:24:end));    
%     histogram(D_Hour.amount(daytime+24-start_hour:24:end))
%     xlabel('Amount of Lvs per daytime')
%     ylabel('Occurance of this amount')
%     title(['Distribution of LV amounts at ', num2str(c), ':00 o-clock'])
%     legend(['Mean: ', num2str(Check.LV_distro.daytime(c).mean_amount), ', dev: ', num2str(Check.LV_distro.daytime(c).mean_amount_dev)])
%     end
%     % Result: x*exp(-x), -x, or 1/x relation
%     
%     % Plot duration and energy distribution and occurance per PC and Cc
%     % Duration distribution:
%     d=0;
%     figure
%     hold on
%     histogram(Intern.duration/3600, round(max(Intern.duration/3600)*60))
%     histogram(Good.data(:,8)/3600, round(max(Good.data(:,8)/3600)*60))
%     hold off
%     xlabel('Duration[h]')
%     ylabel('Occurance')
%     legend('Total rnd', 'Total exp')
%     for c=1:5
%         for p=1:7
%             figure
%             Trash.elements = get_same_elements(find(Intern.charge_case==c), find(Intern.power_class==p));
%             hold on
%             histogram(Intern.duration(Trash.elements)/3600, round(max(Intern.duration(Trash.elements)/3600)*60))  
%             histogram(Good.data(Cluster.power(p).case(c).elements,8)/3600, round(max(Good.data(Cluster.power(p).case(c).elements,8)/3600)*60))
%             hold off
%             d=d+1;
%             title([Power_classes.names{p}, ', ' Good.cases.names{c}])
%             xlabel('Duration[h]')
%             ylabel('Occurance')
%             legend('Rnd', 'Exp')
%         end
%     end
%     
%     % Energy distribution
%     
%     d=0;
%     figure
%     hold on
%     histogram(Intern.energy/3600, round(max(Intern.energy/3600)*10))
%     histogram(Good.data(:,7), round(max(Good.data(:,7))*10))
%     hold off
%     xlabel('Energy [kWh]')
%     ylabel('Occurance')
%     legend('Total rnd', 'Total exp')
%     for c=1:5
%         for p=1:7
%             figure
%             Trash.elements = get_same_elements(find(Intern.charge_case==c), find(Intern.power_class==p));
%             hold on
%             histogram(Intern.energy(Trash.elements)/3600, round(max(Intern.duration(Trash.elements)/3600)*60))  
%             histogram(Good.data(Cluster.power(p).case(c).elements,8)/3600, round(max(Good.data(Cluster.power(p).case(c).elements,8)/3600)*60))
%             hold off
%             d=d+1;
%             title([Power_classes.names{p}, ', ' Good.cases.names{c}])
%             xlabel('Duration[h]')
%             ylabel('Occurance')
%             legend('Rnd', 'Exp')
%         end
%     end

% %% 
% figure
% hold on
% scatter([0:Days*24-1]', Trash.rnd_amount)
% plot([0:1/3600:Days*24-1/3600]', Intern.current_amount)
% Trash.ap_x = [minute(Simu.actual_power.datetime_start)/60:1/60:Simu.actual_power.nu_of_rows/60+minute(Simu.actual_power.datetime_start)/60-1/60]';
% plot(Trash.ap_x,Simu.actual_power.data/10) 
% hold off
% legend('rnd', 'current', 'power')




