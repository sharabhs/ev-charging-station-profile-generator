function [Simu, error] = get_rnd_profile_m(Days, Resolution_m, ScaleFactor, Distro_1, Loc, Year, MULTIPLEXING)
%   Generate statistical power profile for an examplary charging station   
    
%   Input:
%       - Duration [days] (full days only!)
%       - Resolution [days]
%       - Scale Factor [] (Scale amount of electric vehicles on street)
%     %%
%    clear Intern P_max P_mean P_min P_slope a LV_power LV_time Power_course el Simu Simu_profile

    %% Constants

    MAX_CONNECTED = 5;   % Attention: per LS, not per LP! 
    PATIENCE=120;
    error = false;
    RESET = false;

    set(0,'DefaultFigureVisible','off');

    %% Example Data                                                        
%     clear Intern trash Simu
%     Days = 10;                                                           % [days]
%     Resolution_m = 1;                                                       % [min]
%     ScaleFactor = 10;
%     Loc = 2;
%     Year = '2010';
%     MULTIPLEXING = true;
   

    %% Preparation                                                         
    
    set(0,'DefaultFigureVisible','off');
    disp('Generate random profile...')
    
    % start timer
    tic
    
    % Load data
    BA_Test_constants
    
    % Check duration input
    if ~(round(Days)==Days)
        disp('Error in get_rnd_profile: Duration is not in full days!')
        return
    end
    
    % Check Resolution Input
    if Resolution_m < 1 || Resolution_m > 24*60
        disp('ERROR: Resolution not suitable. Chose resolution in minutes [1...24*60 min]')
    end
    
    % Convert Resoltuion to datenum
    Resolution = Resolution_m*1/24/60;
     
    % Time Array:
    Intern.start_date = datenum(datestr(['01-01-' Year ', 00:00:00']));  % day (mm-dd-yyyy, .. )!!!
    Intern.datenum = [Intern.start_date:Resolution:Intern.start_date+Days-Resolution]'; % day, in Res steps
    Intern.datenum_h = [Intern.start_date:1/24:Intern.start_date+Days-1/24]'; % day, in hour steps
    
    % Hourly time array
    Intern.hours = repmat([0:23]', Days,1);
    
    % hourly array with number of rejected cars
    Intern.rejected = zeros(Days*24*60,1);
    
    % Current amount of LV and idle cars per minute
    Intern.current_amount = zeros(Days*24*60,1);
    Intern.idle_amount = Intern.current_amount;
    Intern.currently_charging = Intern.current_amount;
    
    % Type of day array (7&1: Sa&So)
    Intern.start_weekday = weekday(Intern.start_date)-1;
    trash2.daytype = mod(floor(Intern.datenum_h-Intern.datenum_h(1))+Intern.start_weekday,7)+1;
    % Weekday
    Intern.daytype(find(trash2.daytype == 2 | ...
                        trash2.daytype == 3 | ...
                        trash2.daytype == 4 | ...
                        trash2.daytype == 5 | ...
                        trash2.daytype == 6)) = 1;
    % Weekend
    Intern.daytype(find(trash2.daytype == 7)) = 2;
    Intern.daytype(find(trash2.daytype == 1)) = 3;
    
    
    trash.index = 0;
    trash.rej = zeros(24,3);
    
    % Patience of EV in seconds -> cars will wait untill LP is free
    Intern.PATIENCE = 60*5;
    
    Intern.start_time = [];
   
    
    %% Prepare distribution matrices                                       
    % Amount
    for w=1:4
        for d=1:24
            Intern.amount_distr(:,d,w) = [0; cumsum(Distro_1.location(Loc).daytype(w).daytime(d).scale(ScaleFactor).new_lim_amount_m)];
        end
    end
    
    % Power Classes
%     Intern.p_c_distr = [zeros(24,1), cumsum(Distro_1.pc_mat_dt_norm(:,2:end),1)'];
    Intern.p_c_distr = [0, cumsum(Distro_1.power_class_distr_norm(:,2))'];
%     Intern.p_c_distr = Intern.p_c_distr ./ repmat(Intern.p_c_distr(:,end),1,Power_classes.nu_of_classes+1);
    % TODO: weekday seperation
    
    trash.max_cars = numel(Distro_1.location(Loc).daytype(1).daytime(1).scale(ScaleFactor).new_lim_amount_m)-1;
    
    trash.amount_mat = [0;[0:trash.max_cars]'];
    
    
    
    %% -------------------------- Generate LVs ----------------------------
    
    % Loop through every Hour
    tic
    for h=1:numel(Intern.hours)
        
        %% Preparation                                                     
        trash.daytime = Intern.hours(h);
        w = Intern.daytype(h);
        
        %% Get number of cars that want to charge                          
        [trash.rounded{h,1}, trash.uel] = unique(Intern.amount_distr(:,trash.daytime+1,w));
        trash.rnd_amount(h,1) = sum(trash.amount_mat(trash.uel) .* (trash.rounded{h}==roundtowardvec(rand(1), trash.rounded{h}, 'ceil')));

        % distribute their arrival over 1 h
        Intern.start_time_demanded{h,1} = sort(h-1 + round(rand(trash.rnd_amount(h),1)*3600)/3600);        % hours, rounded to sec, randomly distributed over one hour
        
        
        %% Loop through all requested LVs -> get Rejection, Energy, Duration & Power class  
        
        for k = 1:numel(Intern.start_time_demanded{h})
            
            % get minute element for current amount
            trash.min_el = round(Intern.start_time_demanded{h}(k)*60)+1;
            
            % Free LP? -> allow car
            if Intern.current_amount(trash.min_el) < 2
                trash.free = true;
                trash.delay = 0;
                trash.multiplex = false;
            % Multiplexing: Free LP after PATIENCE time & not too many cars connected    
            elseif MULTIPLEXING && Intern.currently_charging(trash.min_el+PATIENCE) < 2 && ...
                    Intern.current_amount(trash.min_el)<MAX_CONNECTED                               % Attention: MAX_CONNECTED per LS, not LP -> should be per LP and even number!
                trash.free = true;  
                trash.multiplex = true;
                trash.delay = find(Intern.currently_charging(trash.min_el:trash.min_el+PATIENCE)<2,1,'first') - 1;
                % if still no space
                if numel(trash.delay) == 0
                    disp('ERROR')
                    return
                end
                trash.min_el = trash.min_el + trash.delay;
                Intern.start_time_demanded{h}(k) = Intern.start_time_demanded{h}(k) + trash.delay/60;
            else
                trash.free = false;
                trash.delay = 0;
                trash.multiplex = false;
            end
            
            % if car allowed
            if trash.free
                
                %% count up LV index                                       
                trash.index = trash.index+1;
                c = trash.index;

                %% Get Power classes                                       
%                 Intern.power_classes(c,1) = find(Intern.p_c_distr(trash.daytime+1,:)==...
%                     roundtowardvec(rand(1),Intern.p_c_distr(trash.daytime+1,:), 'ceil'),1)-1;
                Intern.power_classes(c,1) = find(Intern.p_c_distr==...
                    roundtowardvec(rand(1),Intern.p_c_distr, 'ceil'),1)-1;
          
                %% Get Duration and Energy                                 
                % Energy & Duration
                if max(Distro_1.location(Loc).daytime(trash.daytime+1).power(Intern.power_classes(c)).e) > 0
                    [Intern.energy(c,1), Intern.duration(c,1)] = pinky(...
                        Distro_1.location(Loc).daytime(trash.daytime+1).power(Intern.power_classes(c)).e, ...
                        Distro_1.location(Loc).daytime(trash.daytime+1).power(Intern.power_classes(c)).d, ...
                        Distro_1.location(Loc).daytime(trash.daytime+1).power(Intern.power_classes(c)).e_d_distr');      % kWh, s
                else  
                    Intern.energy(c,1) = 0;
                    Intern.duration(c,1) = 0;
                    % reset index              
                    RESET = true;
%                     disp('Warning! Energy & Duration Distribution = 0')
                end
            
                
                %% Get Idle time                                           
                % if charge case allows it
                if idle_or_not(Intern.duration(c,1), Intern.energy(c,1), Intern.power_classes(c,1))
                    Intern.idle_time(c,1) = Distro_1.location(Loc).daytype(w).daytime(trash.daytime+1).idle(rand(1));
                else
                    Intern.idle_time(c,1) = 0;
                end
                
                %% Substract delay time from idle time & duration
                if MULTIPLEXING 
                    Intern.idle_time(c,1) = Intern.idle_time(c,1) - trash.delay*60;
                    
                    % if delay longer than idle time
                    if Intern.idle_time(c,1) < 0 
                        Intern.energy(c,1) = Intern.energy(c,1) * (1+Intern.idle_time(c,1)/Intern.duration(c,1));
                        Intern.duration(c,1) = Intern.duration(c,1) +  Intern.idle_time(c,1) ;
                        Intern.idle_time(c,1) = 0;
                        % if delay longer than idle time and duration
                        if Intern.duration(c,1) < 0
                            Intern.energy(c,1) = 0;
                            Intern.duration(c,1) = 0;
                            % Count as rejected
                            Intern.rejected(trash.min_el) = Intern.rejected(trash.min_el) + 1;
                            % per daytime & daytype 
                            trash.rej(trash.daytime+1,w) = trash.rej(trash.daytime+1,w) + 1;
                        end
                            
                    end
                end
                % TODO: Substract from duration as well!
                
                %% Get current amount array                                
                % forward start time
                Intern.start_time(c,1) = Intern.start_time_demanded{h}(k);
                % get current amount according to LV's duration (per min)
                trash.charge_el = ceil(Intern.start_time(c)*60+round(Intern.duration(c)/60))+1;
                % get current amount according to LV's duration, adding idle time
                trash.dur_el = ceil(Intern.start_time(c)*60+round(Intern.duration(c)/60)+round(Intern.idle_time(c)/60))+1;
                % prohibit elements outside current amount array
                trash.dur_el(trash.dur_el>numel(Intern.current_amount)) = [];
                trash.charge_el(trash.charge_el>numel(Intern.current_amount)) = [];
                % Add car to current amount array
                trash.curr_el = round(Intern.start_time(c)*60)+1:trash.dur_el;
                Intern.current_amount(trash.curr_el) = Intern.current_amount(trash.curr_el)+1;
                % Add idle car to idle_amount array
                if round(Intern.start_time(c)*60+round(Intern.duration(c)/60)) <= numel(Intern.idle_amount)
                    trash.idle_el = round(Intern.start_time(c)*60+round(Intern.duration(c)/60)):trash.dur_el;
                    Intern.idle_amount(trash.idle_el) = Intern.idle_amount(trash.idle_el)+1;
                end
                % add car to array of amount of currently charging cars
                trash.charge_el = round(Intern.start_time(c)*60)+1:trash.charge_el;
                Intern.currently_charging(trash.charge_el) = Intern.currently_charging(trash.charge_el) + 1;
            
            
                % Reset index
                if RESET
                    trash.index = trash.index-1;
                    c = trash.index;
                    RESET = false;
                end
                
            % if car not allowed    
            else
                % Count rejected cars
                Intern.rejected(trash.min_el) = Intern.rejected(trash.min_el) + 1;
                % per daytime & daytype 
                trash.rej(trash.daytime+1,w) = trash.rej(trash.daytime+1,w) + 1;
            end
           
            % more than 2 cars connected while not multiplexing?
            if Intern.current_amount(trash.min_el) > 2 && ~MULTIPLEXING
                disp('Halt stop')
                return
            end
                
        end
        
        show_progress(h, 500, numel(Intern.hours));
        
    end
    
    
    %% Remove zero energy and duration data                                
    if numel(find(Intern.energy==0))>0
        trash.zero_elements = find(Intern.energy==0);
        Intern.start_time(trash.zero_elements) = [];
        Intern.duration(trash.zero_elements) = [];
        Intern.energy(trash.zero_elements) = [];
        Intern.power_classes(trash.zero_elements) = [];
        Intern.idle_time(trash.zero_elements) = [];
    end
    if numel(find(Intern.duration==0))>0
        trash.zero_elements = find(Intern.duration==0);
        Intern.start_time(trash.zero_elements) = [];
        Intern.duration(trash.zero_elements) = [];
        Intern.energy(trash.zero_elements) = [];
        Intern.power_classes(trash.zero_elements) = [];
        Intern.idle_time(trash.zero_elements) = [];
    end
    
    
    %% Create actual profile                                               
    
    % prepare data set:
    Simu.data(:,3) = Intern.start_time/24+Intern.start_date;                % datenum
    Simu.data(:,4) = Simu.data(:,3) + Intern.duration/24/3600;              % datenum
    Simu.data(:,7) = Intern.energy;                                         % kWh 
    Simu.data(:,8) = Intern.duration+Intern.idle_time;                      % s
    Simu.data(:,12)= Intern.power_classes;                                  % 
    Simu.data(:,10)= Intern.energy*3600./Intern.duration;                   % kW
%     Simu.data(:,15)= Intern.duration;                                     % s
    [Simu.no_of_rows, Simu.no_of_cols] = size(Simu.data);
    
    
    % Get Actual Power 
    [Actual_profile, Simu] = get_power_profiles(Simu, Resolution_m);                % rounded to Res
    
    
    
    if numel(Actual_profile.data)~=numel(Actual_profile.datetime)
        disp('ERror in get_poewr_profiles1!!!!!!!!!!!')
    end
    
    Actual_profile2 = Actual_profile;
%     Actual_profile = Actual_profile2;
    
    % Add start of Actual Power (starts at first LV, but should start at
    % beginning of generation)
    trash.act_start = datetime(datestr(Intern.start_date, TIME_FORMAT), 'Format', TIME_FORMAT_DT);
    trash.act_diff1_m = minutes(Actual_profile.datetime_start-trash.act_start)/Resolution_m;
    trash.act_end = datetime(datestr(Intern.start_date+Days-Resolution_m/60/24, TIME_FORMAT), 'Format', TIME_FORMAT_DT);
    trash.act_diff2_m = minutes(trash.act_end-Actual_profile.datetime_end)/Resolution_m;
    if trash.act_diff2_m <0
        Actual_profile.data(end+trash.act_diff2_m+1:end) = [];
    end
    Actual_profile.datetime_start = trash.act_start;
    Actual_profile.datetime_end = trash.act_end;
    Actual_profile.datetime = [Actual_profile.datetime_start:minutes(Resolution_m):Actual_profile.datetime_end]';
    Actual_profile.nu_of_rows = numel(Actual_profile.datetime);
    Actual_profile.datenum = datenum(Actual_profile.datetime);
    Actual_profile.datenum_start = Actual_profile.datenum(1);
    if round(trash.act_diff1_m)~=trash.act_diff1_m || round(trash.act_diff2_m)~=trash.act_diff2_m
%         trash.act_diff1_m<0 || trash.act_diff2_m <0 || 
       disp('Hey!') 
    end
    Actual_profile.data = [zeros(trash.act_diff1_m,1); Actual_profile.data; zeros(trash.act_diff2_m,1)];
    
    if numel(Actual_profile.data)~=numel(Actual_profile.datetime)
        disp('ERror in get_poewr_profiles2!!!!!!!!!!!')
    end
%     figure
%     plot(Actual_profile.datetime, Actual_profile.data)
    Simu.actual_power = Actual_profile;
    
    % Check actual duration
    Simu.act_d_dev = abs(Simu.data(:,15)-Intern.duration);
    Simu.idle_time = Intern.idle_time;
    
    
    %% Get Daytype and time of day                                         
    % Get datetime
    Simu.start_datetime = datetime(datestr(Simu.data(:,3), TIME_FORMAT), 'Format', TIME_FORMAT_DT);

    % Daytype (1:Weekday, 2: weekend)
    Simu.data(:,16) = ones(Simu.no_of_rows,1);
    Simu.headers{16} = '(16) Daytype';
    Simu.units{16} = '1:Weekday, 2:Saturday, 3:Sunday';
    trash.sat_elm = find(weekday(Simu.start_datetime)==7);
    trash.sun_elm = find(weekday(Simu.start_datetime)==1);
    Simu.data(trash.sat_elm,16)=2*ones(numel(trash.sat_elm),1);
    Simu.data(trash.sun_elm,16)=3*ones(numel(trash.sun_elm),1);

    % Time of day
    Simu.data(:,17) = mod(floor(Simu.data(:,3)*24),24);
    Simu.headers{17} = '(17) Daytime';
    Simu.units{17} = 'o`clock';
    
    
    %% Get Utilization                                                     
    
    % General
    Simu.info.connected = Intern.current_amount;                            % in minutes
    Simu.info.idle = Intern.idle_amount;                                    % in minutes
    Simu.info.requested = trash.rnd_amount;                                 % in hours
    Simu.info.rejected = Intern.rejected;                                   % in minutes
    Simu.info.start_time_demanded = cell2mat(Intern.start_time_demanded(:));   % continuous
    
    trash.mean_current_amount_h = mean(reshape(Simu.info.connected, 60, Days*24),1);
    trash.max_current_amount_h = max(reshape(Simu.info.connected, 60, Days*24),[],1);
    
    % per daytype
    % full week elements
    trash.days_before_first_week = mod(9-weekday(datetime(datestr(Intern.start_date))),7);
    trash.days_after_last_week = mod(Days-1-trash.days_before_first_week,7)+1;
    trash.full_weeks = (Days-trash.days_after_last_week-trash.days_before_first_week)/7;
    
    % over daytype averaged
    
    % connected
    Simu.info.mean_connected_w = nanmean(reshape(Simu.info.connected(trash.days_before_first_week*24*60+1:end-trash.days_after_last_week*24*60), 24*7*60, trash.full_weeks),2);
    Simu.info.mean_connected_w = [nanmean(reshape(Simu.info.mean_connected_w(1:60*24*5),24*60,5),2); Simu.info.mean_connected_w(60*24*5+1:end)];
    % mean over hour
    Simu.info.mean_connected_hour_w = mean(reshape(Simu.info.mean_connected_w, 60,24*3),1)';
    % max within hour
    Simu.info.max_connected_hour_w = mean(reshape(trash.max_current_amount_h(trash.days_before_first_week*24+1:end-trash.days_after_last_week*24), 24*7, trash.full_weeks),2);
    Simu.info.max_connected_hour_w = [mean(reshape(Simu.info.max_connected_hour_w(1:24*5),24,5),2); ...
        Simu.info.max_connected_hour_w(24*5+1:end)];
    
    % Accepted per hour
    trash.acc_mat  = count_same_values_2D([Simu.data(:,16), Simu.data(:,17)],[1,1],[1 0], [3 23]);
    trash.acc_mat.matrix = trash.acc_mat.matrix'./repmat([Days/7*5 Days/7 Days/7],24,1);
    Simu.info.mean_accepted_hour_w = trash.acc_mat.matrix(:);
    
    % Idle    
    Simu.info.mean_idle_w = mean(reshape(Simu.info.idle(trash.days_before_first_week*24*60+1:end-trash.days_after_last_week*24*60), 24*7*60, trash.full_weeks),2);
    Simu.info.mean_idle_w = [mean(reshape(Simu.info.mean_idle_w(1:60*24*5),24*60,5),2); Simu.info.mean_idle_w(60*24*5+1:end)];
    % mean over hour
    Simu.info.mean_idle_hour_w = mean(reshape(Simu.info.mean_idle_w, 60,24*3),1)';
 
    % Requested 
    Simu.info.mean_requested_hour_w = mean(reshape(Simu.info.requested(trash.days_before_first_week*24+1:end-trash.days_after_last_week*24), 24*7, trash.full_weeks),2);
    Simu.info.mean_requested_hour_w = [mean(reshape(Simu.info.mean_requested_hour_w(1:24*5),24,5),2); Simu.info.mean_requested_hour_w(24*5+1:end)];
   
    % Rejected
    Simu.info.mean_rejected_w = mean(reshape(Simu.info.rejected(trash.days_before_first_week*24*60+1:end-trash.days_after_last_week*24*60), 24*7*60, trash.full_weeks),2);
    Simu.info.mean_rejected_w = [mean(reshape(Simu.info.mean_rejected_w(1:60*24*5),24*60,5),2); Simu.info.mean_rejected_w(60*24*5+1:end)];
    % mean over hour
    Simu.info.mean_rejected_hour_w = mean(reshape(Simu.info.mean_rejected_w, 60,24*3),1)';
    

    Simu.info.rejections_per_dt_w = trash.rej ./ repmat([Days/7*5, Days/7, Days/7],24,1);
    Simu.info.total_rejections_dt_w = trash.rej;
    Simu.info.total_rejections = sum(sum(trash.rej));
    Simu.info.rejections_per_day_w = sum(trash.rej) ./ [Days/7*5, Days/7, Days/7];
    
    Simu.info.rejections_per_dt = sum(trash.rej,2) / Days;
    Simu.info.total_rejections_dt = sum(trash.rej,2);
    Simu.info.rejections_per_day = Simu.info.total_rejections / Days;
    

    Simu.info.max_occupation_perc = 100*max(Simu.info.mean_connected_w)/2;
    Simu.info.max_rejection_perc = max(max(Simu.info.rejections_per_dt_w))*200;
    
    
    %% Get general Info                                                    
    
    % time
    Simu.info.earliest_start = datetime(datestr(min(Simu.data(:,3))));
    Simu.info.latest_end = datetime(datestr(max(Simu.data(:,4))));
    Simu.info.nu_of_days = days(Simu.info.latest_end-Simu.info.earliest_start);
    
    % amount
    Simu.info.nu_of_LVs = numel(Simu.data(:,1));
    Simu.info.nu_of_LSs = numel(unique(Simu.data(:,1)));
    for w=1:3
        Simu.info.daytype(w).mean_amount = numel(find(Simu.data(:,16)==w))/(Simu.info.nu_of_days/7);
    end
    Simu.info.daytype(1).mean_amount = Simu.info.daytype(1).mean_amount / 5;
    Simu.info.daytype(4).mean_amount = numel(Simu.data(:,16))/Simu.info.nu_of_days;
    
    % Get new size of data matrix
    [Simu.no_of_rows, Simu.no_of_cols] = size(Simu.data);
    
    
    %% Feedback:                                                           
    
    set(0,'DefaultFigureVisible','on');
    
    
end

    
%% quick plot                                                              
%     set(0,'DefaultFigureVisible','on');
% figure
% plot(Simu.actual_power.data)
% 
% Intern.start_time_demanded = cell2mat(Intern.start_time_demanded(:))*60;
% 
% figure
% hold on
% plot(Intern.current_amount)
% plot(Intern.rejected)
% scatter(Intern.start_time_demanded, ones(numel(Intern.start_time_demanded),1),  'x')
% hold off
% legend('Connected', 'Rejected', 'Demanded')

