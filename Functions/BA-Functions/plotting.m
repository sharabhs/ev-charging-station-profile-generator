%% Script to plot figures for BA-test-data-evaluation
% 08.01.19, Linus Kemme


%% ---------------------- Plot Data ---------------------------------------

close all

%% Constants
Marker_matrix = {'o', '+', '*','.','x','s','d','^'};
Color_matrix_bad = {'y', 'm','c','r','g','b','k'};
Line_matrix = {'-', '--', ':', '-.'};
Color_matrix = {
            [0, 0.4470, 0.7410], 	          	
          	[0.8500, 0.3250, 0.0980], 	         
          	[0.9290, 0.6940, 0.1250], 	          	
          	[0.4940, 0.1840, 0.5560], 	         
          	[0.4660, 0.6740, 0.1880], 	          	
          	[0.3010, 0.7450, 0.9330], 	         
          	[0.6350, 0.0780, 0.1840] };

%% Conection time
% takes extremely long!!
% figure
% hold on
% for c = 1:Charge_time.nu_of_user
%     scatter(datetime(datestr(Charge_time.user(c).charge_time)), Charge_time.user(c).number, 'square', 'filled')
% end
% hold off
% xlabel('Time')
% ylabel('User number')
% title('course of charging times per user')
% grid on

%% Actual power
% Actual power over year

% Actual power per day - Comparison
figure
hold on
yyaxis left
plot(Daytime.time, Daytime.mean_amount) 
ylabel('Mean amount of LV')
yyaxis right
plot((Actual_P_Daytime.time), Actual_P_Daytime.mean) 
ylabel('Mean Actual Power [kW]')
hold off
xlabel('Time')
title('Actual power per day - Comparison')
grid on

% Actual power per week - Comparison
figure
hold on
yyaxis left
plot(Weektime.time, Weektime.mean_amount) 
ylabel('Mean amount of LV')
yyaxis right
plot(Period.Actual_P_Day_m.datetime , Period.Actual_P_Day_m.mean) 
ylabel('Mean actual Power [kW]')
hold off
xlabel('Time')
title('Actual power per week - Comparison')
grid on

% Actual power per month - Comparison
figure
hold on
yyaxis left
plot(Monthtime.time, Monthtime.mean_amount) 
ylabel('Mean amount of LV')
yyaxis right
plot((Actual_P_Monthtime.datenum), Actual_P_Monthtime.mean) 
ylabel('Mean actual Power [kW]')
hold off
xlabel('Time')
title('Actual power per month - Comparison')
grid on


% Actual power over year
figure 
hold on
yyaxis left
bar(D_Hour.datetime, D_Hour.amount)
ylabel('Nu of LV starts per hour')
yyaxis right
plot(Actual_power.datetime, Actual_power.data)
ylabel('Power [kW]')
hold off
title('Actual Power')
xlabel('Time')


% single profiles
am =  [0,0];
for c=1:255
    am = [am; c, Charger(c).amount];
end
c_max = find(am(:,2)==max(am(:,2)))-1;

figure
hold on

TTime = Actual_power.single_profiles_time(Charger(c_max).elements);
PPower = Actual_power.single_profiles_power(Charger(c_max).elements);
for c=1:numel(Actual_power.single_profiles_time(Charger(c_max).elements))
    plot(datetime(datestr(TTime{c},TIME_FORMAT),'Format', TIME_FORMAT_DT), PPower{c})
end
hold off
title('Actual power for single charger')
xlabel('Time')
ylabel('Power [kW]')

%% Clustering
% over Charger
figure
surf(Cluster.matrix)
ylabel('Energy, [1 kWh]')
xlabel('Duration [h]')
zlabel('occurance')

% per Charger
figure

for c=1:255
    surf(Charger(c).cluster.matrix)
    ylabel('Energy, [1 kWh]')
    xlabel('Duration [h]')
    zlabel('occurance')
    axis([0 48 0 100 0 10])
    c
    pause(3)
    %clf
end
% -> no clusters detected

% over daytime
figure

for c=1:numel(ToD)
    surf(ToD(c).cluster.matrix)
    ylabel('Energy, [1 kWh]')
    xlabel('Duration [h/4]')
    zlabel('occurance')
    axis([0 48*4 0 100 0 10])
    legend([num2str(c-1) ':00 - ' num2str(c) ':00'])
    c
    pause(4)
    %clf
end

% Over Power class
figure
for c=1:Power_classes.nu_of_classes
    surf(Power_classes.cluster(c).matrix)
    ylabel('Energy, [1 kWh]')
    xlabel('Duration [h/4]')
    zlabel('occurance')
    axis([0 48*4 0 100 0 10])
    legend(Power_classes.names{c})
    c
    pause(10)
end

% colored over daytime
figure
scatter(timeofday(Good.start_datetime), Good.data(:,8), [], Good.data(:,13), 'filled')
xlabel('Start Time')
ylabel('Duration [s]')
title('Duration and case over daytime')

% daytime amount per case
figure
hold on
for c=1:Good.cases.nu_of_cases
   plot(datetime(datestr(Daytime_Cases(c).datenum, CLOCK_FORMAT),'Format', CLOCK_FORMAT_DT), Daytime_Cases(c).amount) 
end
hold off
xlabel('Start Time')
ylabel('# LVs')
title('Daily profile per charging case')
legend(Good.cases.names)

% daytime amount per power class
figure
hold on
for c=1:Power_classes.nu_of_classes
   plot(datetime(datestr(Daytime_Classes(c).datenum, CLOCK_FORMAT),'Format', CLOCK_FORMAT_DT), Daytime_Classes(c).amount) 
end
hold off
xlabel('Start Time')
ylabel('# LVs')
title('Daily profile per power class')
legend(Power_classes.names)

% Mean Energy per charge case over daytime
figure
hold on
for c=1:Good.cases.nu_of_cases
   plot(datetime(datestr(Daytime_Cases(c).datenum, CLOCK_FORMAT),'Format', CLOCK_FORMAT_DT), Daytime_Cases(c).mean(:,7)) 
end
hold off
xlabel('Start Time')
ylabel('Mean Energy per LV [kWh]')
title('Daily Charged Energy per charging case')
legend(Good.cases.names)
% Results:
% - No change over daytime
% - Except Cons + slope + wait - case: higher energy over night
% - Energy depending on charge case (factor 4)

% Mean Energy per power class over daytime
figure
hold on
for c=1:Power_classes.nu_of_classes
   plot(datetime(datestr(Daytime_Classes(c).datenum, CLOCK_FORMAT),'Format', CLOCK_FORMAT_DT), Daytime_Classes(c).mean(:,7)) 
end
hold off
xlabel('Start Time')
ylabel('Mean Energy per LV [kWh]')
title('Daily Charged Energy per power class')
legend(Power_classes.names)
% Result:
% - Charged Energy increasing with max power (factor 4)
% - Charged Energy higher during night

% Mean Energy per charge case and power class over daytime
figure
d=0;
hold on
for p=1:Power_classes.nu_of_classes
    for c=1:Good.cases.nu_of_cases
        plot(datetime(datestr(Cluster.daily.power(p).case(c).Daytime.datenum, CLOCK_FORMAT),'Format', CLOCK_FORMAT_DT), ...
            Cluster.daily.power(p).case(c).Daytime.mean(:,7), 'Marker', Marker_matrix{c}, 'Color', Color_matrix{p})
        d=d+1;
        Name{d} = [Power_classes.names{p}, ', ' Good.cases.names{c}];
    end
end
hold off
xlabel('Time')
ylabel('Mean Energy / LV [kWh]')
legend(Name)
title('Daily charged energy per charge case and power class')

% Energy/Duration clusters Per Daytime, Power class & Charge case
d=8; % daytime
p=1; % power class
c=3; % charge case
Selected_elements = get_same_elements(ToD(d).elements, Power_classes.elements{p});
Selected_elements = get_same_elements(Selected_elements, Good.cases.elements{c});
% [km_cluster.mapping, km_cluster.centers] = kmeans(Good.data(Selected_elements, [7,8]), 10);

figure
hold on
scatter(Good.data(Selected_elements, 8)/3600, Good.data(Selected_elements, 7), [], km_cluster.mapping, '.')
plot([0 max(Good.data(Selected_elements, 8)/3600)], [0 , max(Good.data(Selected_elements, 8)/3600)*Power_classes.max(p)])
% scatter(km_cluster.centers(:,2)/3600, km_cluster.centers(:,1), 'x', 'LineWidth', 2)
hold off
ylabel('Energy, [kWh]')
xlabel('Duration [h]')
zlabel('occurance')
legend([Power_classes.names{p}, ', ', Good.cases.names{c}, ' at ', num2str(d), ':00' ], 'Max power', 'Cluster centers')
title('k-means cluster per charge case for 3.68 kW')

% Energy occurance per Daytime, Power class & Charge case
daytimes = {'0:00','1:00','2:00','3:00','4:00','5:00','6:00','7:00','8:00','9:00','10:00','11:00','12:00',...
           '13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00'};
       
Step = 5; % kWh
Max_energy = ceil(max(Good.data(:,7))/Step)*Step;
Min_energy = floor(min(Good.data(:,7)));

for p=1:Power_classes.nu_of_classes    
   for c=1:Good.cases.nu_of_cases      
       figure
       hold on
       energy = [];
       amount = [];
       for d=1:24
           if numel(Cluster.daytime(d).power(p).case(c).elements)>0
            [energy(:,d), amount(:,d)] = interpolate_with_zeros(...
                Cluster.daytime(d).power(p).case(c).D_energy.x, ...
                Cluster.daytime(d).power(p).case(c).D_energy.amount/ToD(d).nu_of_days,...
                Min_energy, Max_energy, Step);
           else
               energy(:,d) = [0:Step:Max_energy]';
           end
       end
       
       bar3(amount) 
       hold off
       title(['Charge amount occurance per daytime', newline, 'for: ', Power_classes.names{p}, ' - ', Good.cases.names{c}])
       xlabel('Daytime (h)')
       ylabel('Energy [kWh]')
       zlabel('mean #LVs per day')
   end
end

% Results:
% - no clear clusters
% - observation: for const P + full slop cases: 8:00 start time -> longer
% duration & more energy
% close all

% Occurance of charge amount per case and class
figure
hold on
d=0;
for p=1:Power_classes.nu_of_classes
    for c=1:Good.cases.nu_of_cases
        plot(Cluster.power(p).case(c).D_energy.x, Cluster.power(p).case(c).D_energy.amount,...
            'Marker', Marker_matrix{c}, 'Color', Color_matrix{p})
        d=d+1;
        Name{d} = [Power_classes.names{p}, ', ' Good.cases.names{c}];
    end
end
hold off
legend(Name)
xlabel('Energy [kWh]')
ylabel('# LVs')
title('Distribution over charged energy amounts per Charge case and power class')


% Distribution of Cases per power class
figure
bar([1:Power_classes.nu_of_classes], Power_classes.case_distr_rel', 'stacked')
legend(Good.cases.names)
xlabel('Power class')
ylabel('Percentage [%]')
set(gca, 'XTickLabel', Power_classes.names)
% Result: 
% - high power classes charge less const P + full sope

% Distribution of Duration over Pc and Cc
figure
plot(Cluster.power(1).case(5).D_duration.x, Cluster.power(1).case(5).D_duration.amount)

%% Good & Bad power classes

% Energy: -> power lines
figure 
hold on
for c = 1:Power.nu_of_classes
    plot([0 Power.max_power_duration(c,2)], [0 Power.max_power_duration(c,1)])
end
scatter(Bad_class.data(:,8)/3.600, Bad_class.data(:,7), '.r', 'LineWidth', 0.1)
%scatter(Good_class.data(:,8)/3.600, Good_class.data(:,7), '.b', 'LineWidth', 0.1)
hold off
xlabel('Duration [h]')
ylabel('Energy')
legend(Power.legend)

% power classes
figure
hold on
plot([0 D_User.nu_of_discrete_rows], [Power.classes Power.classes])
%scatter(D_User.x, D_User.max(:,10), '.')
scatter(Bad_class.data(:,2), Bad_class.data(:,11), '.r', 'LineWidth', 0.1)
scatter(Good_class.data(:,2), Good_class.data(:,11), '.b', 'LineWidth', 0.1)
hold off
title('Power classes of users')
xlabel(['User [' D_User.unit ']'])
ylabel(['Power ' Good.units{10} ']'])
legend(Power.legend)
% TODO: why some good far away from classes?

% bad classes
figure 
scatter(datetime(datestr(Bad_class.data(:,3))), Bad_class.data(:,8)/3600, '.')
xlabel('Start time [date]')
ylabel('Duration [h]')

% maxP per user
figure
hold on
scatter(Good_class.data(:,2), Good_class.data(:,10),'.b')
scatter(Good_class.data(:,2), Good_class.data(:,11),'.r')
scatter(Bad_class.data(:,2), Bad_class.data(:,10),'.g')
scatter(Bad_class.data(:,2), Bad_class.data(:,11),'.m')
plot([0 D_User.nu_of_discrete_rows], [Power.classes Power.classes])
hold off
xlabel('User')
ylabel('Mean Power [kW]')

% figure
% hold on
% scatter(Good.data(:,2), Good.data(:,11),'.')
% title('Max Power per user ID')
% 
% figure
% scatter(Good.data(:,2), Good.data(:,12),'.')
% title('Power class per user ID')
% %legend(Power.legend)
% 
% figure
% hold on
% scatter([0:numel(Good_classes)-1], Good_classes,'.')
% scatter([0:numel(Bad_classes)-1], Bad_classes,'.')
% hold off
% legend('Good', 'Bad')

%% Per Charger
% TODO: 3 bars per LP: #LV, Energy, Power

% #LV pro LP:
figure
hold on
bar([Charger.no_of_data; Charger.mean_energy; Charger.mean_power]' )
hold off
xlabel('Charger')
ylabel('No_of LV, mean E per LV, mean P per LV')
legend('#LV', 'E_m_e_a_n', 'P_m_e_a_n')

% Data of one LS:
figure
hold on
for c=1:50:251
bar(Charger(c).D_hour.datetime, Charger(c).D_hour.amount)
end
hold off

%% Accumulated
% Plot over time
figure 
hold on
for c=1:1
scatter(datetime(datestr(Charger(c).data(:,3))), Charger(c).data(:,7), '.')
end
hold off
xlabel('Start Time [d]')
ylabel('Energy')

% mean power:
figure 
scatter(datetime(datestr(Good.data(:,3))), Good.power, '.')
xlabel('Start Time [date]')
ylabel('Mean Power')

% Energy: -> power lines
figure 
hold on
for c = 1:Power.nu_of_classes
    plot([0 Power.max_power_duration(c,2)], [0 Power.max_power_duration(c,1)])
end
scatter(Good.data(:,8)/3.600, Good.data(:,7), '.', 'LineWidth', 0.1)
hold off
xlabel('Duration [h]')
ylabel('Energy')
legend(Power.legend)
fit_duration = Good.data(:,8)/3600;
fit_energy =  Good.data(:,7);

% Energy over start time
figure 
scatter(datetime(datestr(Good.data(:,3))), Good.data(:,7), '.')
xlabel('Start time [date]')
ylabel('Energy')

%% Check discretization
% random = randn(100000,1)+3;
% Discr = discretize_struct(random, 1, 0.2, 0, '');
% figure 
% bar(Discr.x, Discr.amount)
% title('Normal distribution')

%% User ID
figure
plot([0:Good.no_of_rows-1], Good.data(:,2))

figure
plot(User.amount(:,1), User.amount(:,2))
xlable('User ID')
ylabel('#LVs')

figure
bar(D_User.x,[D_User.amount, D_User.sum(:,7), D_User.power(:,7)])

% Max Power per user
% figure
% hold on
% plot([0 D_User.nu_of_discrete_rows], [Power.classes Power.classes])
% scatter(D_User.x, D_User.max(:,10), '.')
% hold off
% title('Power classes of users')
% xlabel(['User [' D_User.unit ']'])
% ylabel(['Power ' Good.units{10} ']'])
% legend(Power.legend)

% - X: user, Y: #LVs & PP & sum Energy, sorted by #LVs

%% Discrete max power
% not working:
% figure
% bar(D_Max_power.x/1000, D_Max_power.amount)
% xlabel('Power [kW]')
% ylabel('#User')
% xticks(0:.5:25)

% %% Peak power
% power_width = 100;
% D_Power = discretize_struct(Good.peak_power, Good.data, power_width, 0, 'Wh');
% nan users:
% nan_user = find(isnan(Good.data(:,2)))
% figure
% plot(datetime(datestr(Good.data(nan_user, 3))), Good.data(nan_user, 7))

%% per hour
figure
plot(datetime(datestr(D_Hour.x, TIME_FORMAT), 'Format', TIME_FORMAT_DT),...
    D_Hour.amount)
xlabel('Time')
ylabel('#LVs')
title('Number of LVs over discrete time')
grid on


% discretized over duration or energy
%plot:
figure
bar(D_Duration.x, D_Duration.amount)
xlabel('duration [h]')
ylabel('#LVs')

figure
bar(D_Duration.x, D_Duration.mean(:,7))
xlabel(['duration [' D_Duration.unit ']'])
ylabel('Energy [Wh?]')
title('Mean Energy over duration')

figure
bar(D_Duration.x, D_Duration.sum(:,7))
xlabel(['duration [' D_Duration.unit ']'])
ylabel('Energy [Wh?]')
title('Sum of Energy over duration')

figure;
scatter([1:numel(Good.data(:,8))], Good.data(:,8)/3600, '.', 'LineWidth', 0.1)
xlabel('LV')
ylabel('duration [h]')

%% Per Charge amount
figure
bar(D_Charge.x, D_Charge.amount)
xlabel(['Charge [' D_Charge.unit ']'])
ylabel('#LVs')
title('Charged Energy')

figure
bar(D_Charge.x, D_Charge.mean(:,8))
xlabel(['Charge [' D_Charge.unit ']'])
ylabel('Duration [s?]')
title('Charged Energy and Duration')

%% Per period
figure
plot(Q_Daytime.datetime, Q_Daytime.mean_amount) 
xlabel(['Time'])
ylabel('#LVs')
title('Daytime profile, mean #LV per quarterhour for all chargers')
grid on

figure
plot(Daytime.time, Daytime.amount) 
xlabel(['Time [' Daytime.unit ']'])
ylabel('#LVs')
title('Daytime profile')
grid on

figure
errorbar(Daytime.datenum, Daytime.amount, Daytime.std_dev_amount) 
xlabel(['Time [days]'])
ylabel('#LVs')
title('Daytime profile')
grid on

figure
hold on
for c=1:Daytime.nu_of_periods
scatter(Daytime.datenum, Daytime.three_d_amount(:,:,c), '.') 
end
hold off
xlabel(['Time [days]'])
ylabel('#LVs')
title('Daytime scatter profile')
grid on
% TODO: size of dot proportional to amount of datas with same hour and #
% LVs
figure
plot(Weektime.time, Weektime.mean_amount/255) 
xlabel(['Time [' Weektime.unit ']'])
ylabel('#LVs')
title('Weektime profile')
grid on

figure
plot(Monthtime.time, Monthtime.amount) 
xlabel(['Time [' Monthtime.unit ']'])
ylabel('#LVs')
title('Month profile')
grid on

figure
plot(Yeartime.time, Yeartime.amount) 
xlabel(['Time [' Yeartime.unit ']'])
ylabel('#LVs')
title('Yeartime profile')
grid on

figure
errorbar(Yeartime.time, Yeartime.amount, Yeartime.std_dev_amount) 
xlabel(['Time [' Yeartime.unit ']'])
ylabel('#LVs')
title('Yeartime profile')
grid on

figure
plot(Daytime.time, Daytime.amount) 
xlabel(['Time [' Daytime.unit ']'])
ylabel('#LVs')
title('Daytime profile')
grid on

figure
errorbar(Daytime.datenum, Daytime.amount, Daytime.std_dev_amount) 
xlabel(['Time [days]'])
ylabel('#LVs')
title('Daytime profile')
grid on

figure
hold on
for c=1:Daytime.nu_of_periods
scatter(Daytime.datenum, Daytime.three_d_amount(:,:,c), '.') 
end
hold off
xlabel(['Time [days]'])
ylabel('#LVs')
title('Daytime scatter profile')
grid on
% TODO: size of dot proportional to amount of datas with same hour and #
% LVs
figure
plot(Weektime.time, Weektime.amount) 
xlabel(['Time [' Weektime.unit ']'])
ylabel('#LVs')
title('Weektime profile')
grid on

figure
plot(Monthtime.time, Monthtime.amount) 
xlabel(['Time [' Monthtime.unit ']'])
ylabel('#LVs')
title('Month profile')
grid on

figure
plot(Yeartime.time, Yeartime.amount) 
xlabel(['Time [' Yeartime.unit ']'])
ylabel('#LVs')
title('Yeartime profile')
grid on

figure
errorbar(Yeartime.time, Yeartime.amount, Yeartime.std_dev_amount) 
xlabel(['Time [' Yeartime.unit ']'])
ylabel('#LVs')
title('Yeartime profile')
grid on

% over week

%% zero duration
% e=find(Good.data(:,8)==0);
% without= Good.data;
% without(e,:) = [];
% with = Good.data(e,:);
% 
% 
% figure
% hold on
% bar([1:numel(with(:,7))], (without(1:numel(with(:,4)),4)-without(1:numel(with(:,3)),3))*24)
% bar([1:numel(with(:,7))], (with(:,4)-with(:,3))*24)
% hold off
% legend('without 0', '0 duration')
% title('check zero duration')
% xlabel('index')
% ylabel('duration')

%% Daztime profiles per Charger
for c=1:1:40
    figure
    plot(Charger(c).Daytime.amount)
    title(c)
end

A=0
for c=1:255
    A=A+Charger(c).Daytime.amount;
end
figure
plot(A)
figure
plot(All_charger.Daytime.mean_amount)

close all
for c=1:255
figure
plot(Charger(c).Daytime.amount)
Name{c} = ['Charger ', num2str(c)];
xlabel('Time [h]')
ylabel('#LVs')
title(Name{c})
legend(['#Days: ', num2str(Charger(c).Daytime.nu_of_periods)])
end
close all
for c=1:255
figure
plot(Charger(c).Daytime.amount)
Name{c} = ['Charger ', num2str(c)];
xlabel('Time [h]')
ylabel('#LVs')
title(Name{c})
legend(['#Days: ', num2str(Charger(c).Daytime.nu_of_periods)])
end

% sort Charger:
for c=1:255
    Charger(c).Daytime.norm_amount = Charger(c).Daytime.amount/sum(Charger(c).Daytime.amount);
    Charger(c).Daytime.peak_time = find(Charger(c).Daytime.amount==max(Charger(c).Daytime.amount));
    if Charger(c).Daytime.peak_time(1)>=6 && Charger(c).Daytime.peak_time(1) <=11 && numel(Charger(c).Daytime.peak_time)==1
        Charger_Category(c) = 1;
    else
        Charger_Category(c) = 0;
    end
end
work_charger = find(Charger_Category);
figure
hold on
for c=1:numel(work_charger)
    plot(Charger(work_charger(c)).Daytime.norm_amount)
end
hold off

%% Day profile weekday and weekend

figure
hold on
plot(Weekday.time, Weekday.mean_amount)
plot(Weekend.time, Weekend.mean_amount)
plot(Weekend.time, Daytime.mean_amount)
% plot(Weekend.time, Allday.mean_amount)
hold off
legend('Weekday', 'Weekend', 'Total', 'Allday')
xlabel('Time[h]')
ylabel('#LVs per day')
title('Mean Daytime profile for Weekday and Weekend for all chargers')
% Result: Previous Daytime profile not absolutely correct
% After flooring instead of roundin: exactly the same!

%% Check Test_Simu
    Test_simu.D_hour = discretize_struct_fast(Test_simu.data(:,3), Test_simu.data, 1/24, 0, 'days');
    % peridoify:
    Test_simu.D_hour.Daytime = periodic_course_III(Test_simu.D_hour, 'day');
    
%% Check Test simu daztime profile
    set(0,'DefaultFigureVisible','on');
    figure
    hold on
    plot(Period.Day_h.datetime, All_charger.Daytime.amount)
    plot(Period.Day_h.datetime, Test_simu.D_hour.Daytime.amount*25.5)
    plot(Period.Day_h.datetime, Test_simu.rejected*25.5)   
    plot(Period.Day_h.datetime, (Test_simu.rejected+Test_simu.D_hour.Daytime.amount)*25.5)
    plot(Period.Day_h.datetime, Period.Weekday_h.amount)
    plot(Period.Day_h.datetime, Period.Weekend_h.amount)
    plot(Period.Day_h.datetime, Period.Day_h.amount)
    hold off
    legend('measured', 'Simulated', 'rejected', 'corrected', 'weekday', 'Weekend', 'Allday')
    xlabel('Time')
    ylabel('Mean #LV per day')
    % Result: less than actual, but similar course
    
% Check high amount of EVs
figure
hold on
bar([Test_simu_full.current_amount, Test_simu_full.rnd_amount'])
hold off
legend('current', 'rnd')
xlabel('Time [h]')
ylabel('#LVs')
 
% Actual power
figure
plot(Test_simu.actual_power.datetime, Test_simu.actual_power.data)
ylabel('Power [kW]')
xlabel('Time')
grid on
g=get(gca, 'XAxis')
set(g, 'TickValues', g.TickValues(1):2/24:g.TickValues(end))

% Actual power daytime
Test_simu.actual_power.D_hour = discretize_struct_fast(Test_simu.actual_power.datenum, Test_simu.actual_power.data, 1/24/4, 0, 'days');
% peridoify:
Test_simu.actual_power.D_hour.Daytime = periodic_course_III(Test_simu.actual_power.D_hour, 'day');
Test_simu.actual_power.D_hour.Weektime = periodic_course_III(Test_simu.actual_power.D_hour, 'week');

% Compare daily actual power of simulation to measurement
figure
hold on
plot(Test_simu.actual_power.D_hour.Daytime.datetime, Test_simu.actual_power.D_hour.Daytime.mean)
plot((Actual_P_Daytime.datetime), Actual_P_Daytime.mean/255) 
hold off
xlabel('Time')
ylabel('Mean Power [kW]')
legend('Simulated', 'Measured')
title('Simulated Daily Actual Power Profile')

% Compare weekly actual power of simulation to measurement
figure
hold on
plot(Test_simu.actual_power.D_hour.Weektime.datenum, Test_simu.actual_power.D_hour.Weektime.mean)
plot(Actual_P_Weektime.datenum, Actual_P_Weektime.mean/255) 
hold off
xlabel('Time [days]')
ylabel('Mean Power [kW]')
legend('Simulated', 'Measured')
title('Simulated Weekly Actual Power Profile')
% Result: fits well. Sa and Su could be seperated


% charger pie
figure
pie(Test_simu_full.cases.distribution(:,2), Good.cases.names)
    
%% Check amount distro
% Weekday vs Weekend
set(0,'DefaultFigureVisible','on');
for d=1:24
    Distro_Weekend(d) = sum(prod(Distro_ad_2.weekend_dt(d).amount,2))/sum(Distro_ad_2.weekend_dt(d).amount(:,2));
    Distro_Weekday(d) = sum(prod(Distro_ad_2.weekday_dt(d).amount,2))/sum(Distro_ad_2.weekday_dt(d).amount(:,2));
end
figure
hold on
plot([0:23], Distro_Weekend)
plot([0:23], Distro_Weekday)
hold off
legend('Weekend', 'weekday')
sum(Distro_Weekend)
sum(Distro_Weekday)
sum(Distro_Weekend)*2/7+sum(Distro_Weekday)*5/7

%% Check scaled amount distro

figure
hold on
for s=1:255
    plot(Distro_1.scale(s).weekday_dt(9).amount(:,1), Distro_1.scale(s).weekday_dt(9).amount(:,2))
end
hold off


figure
hold on
for s=1:255
    plot([0:23],Scale(s).wd_lim_mean_amount)
end
hold off

%% Some general info
for c=1:255
Charger(c).max_power = max(Charger(c).data(:,11));
Trash.max(c,1) = Charger(c).max_power;
end

figure
histogram(Trash.max, round(max(Trash.max)))
xlabel('Max power [kW]')
ylabel('#Charger')
title('Max power of chargers')

%% Chronological development
% #LSs
for c=1:255
    Trash.c_start(c,1) = Charger(c).D_h.min_x;
end

Trash.thirty_d =discretize_struct_fast(Good.data(:,3), 1, 30, 0, 'day');

set(0,'DefaultFigureVisible','off');
H=histogram(Trash.c_start-min(Trash.c_start),round(max(Trash.c_start-min(Trash.c_start))/30 ));
set(0,'DefaultFigureVisible','on');
Trash.date = datetime(datestr(min(Trash.c_start))):days(30):datetime(datestr(max(Trash.c_start)));

figure 
plot(Trash.date, cumsum(H.Values))
title('Development of Charger amount')
xlabel('Time [days] ')
ylabel('#LSs')
grid on


figure 
plot(Trash.thirty_d.datetime(1:end-1), Trash.thirty_d.amount(1:end-1))
title('Development of LV amount')
xlabel('Time [days] ')
ylabel('#LVs')
grid on

figure 
plot(Trash.thirty_d.datetime(1:end-1), Trash.thirty_d.amount(1:end-1)./cumsum(H.Values)')
title('Development of LVs/LS')
xlabel('Time [days] ')
ylabel('#LV/LS')
grid on

%% Daytype profile per Location
close all
figure
hold on
for l=1:numel(Location)
plot([0:71]', Location(l).daytype_mean, 'Color', Color_matrix{l}, 'Linewidth', 2, 'DisplayName', [Location(l).name, ',  ' num2str(numel(Location(l).charger_el)), ' LS'])   
end
hold off
legend('show')
ylabel('mean #LVs per day and -time')
title('Mean LV amount per location')

% change xlabel
for d=1:72
    day_label{d} = [num2str((mod(d-1,24))) ':00'];
end
set(gca, 'XTick', 0:6:72, 'XTickLabel', day_label(1:6:end))

% Add daytype label
trash.distance = -0.002;
text('Units', 'Data', 'Position', [12,trash.distance], 'HorizontalAlignment', 'center', 'String', 'Workday')
text('Units', 'Data', 'Position', [12+24,trash.distance], 'HorizontalAlignment', 'center', 'String', 'Saturday')
text('Units', 'Data', 'Position', [12+24*2,trash.distance], 'HorizontalAlignment', 'center', 'String', 'Sunday')
text('Units', 'Data', 'Position', [0,trash.distance], 'HorizontalAlignment', 'center', 'String', '|')
text('Units', 'Data', 'Position', [24*3,trash.distance], 'HorizontalAlignment', 'center', 'String', '|')
text('Units', 'Data', 'Position', [24,trash.distance], 'HorizontalAlignment', 'center', 'String', '|')
text('Units', 'Data', 'Position', [24*2,trash.distance], 'HorizontalAlignment', 'center', 'String', '|')

%% Waiting time

% get distribution
set(0,'DefaultFigureVisible','off'); 
clear trash trash2
trash.res = 1/3600;                   % h
close all
e=0;
tic
trash.idle_case_el = find(Good.data(:,13) == 2 | Good.data(:,13) == 3); 
for l=1:6
    for w=1:4
        for d=1:24
            % get wait times of this time 
            trash.els = get_same_elements(trash.idle_case_el, Distro_1.location(l).daytype(w).elem_dt{d});
            trash.wait = Good.data(trash.els ,8) - ...
                Good.data(trash.els ,15);
            if numel(trash.wait)~=0
                % remove negative values (0 ... -30 s)
                trash.wait(trash.wait<0)=0;
                % get normalized distribution (1/10th h)
                trash.trash = histogram(trash.wait/3600, ...
                    max([1 round(max(trash.wait/3600)/trash.res)]), 'Normalization', 'probability');
                trash2(d,w,l).wait_dis = trash.trash.Values;
                % get time 
                trash2(d,w,l).wait_t = 3600*[0:trash.res:numel(trash2(d,w,l).wait_dis)*trash.res];
                % cumsum distribution
                trash2(d,w,l).wait_cum = [0 cumsum(trash2(d,w,l).wait_dis)];
                % remove same elements
                [trash2(d,w,l).wait_cum, trash.ue] = unique(trash2(d,w,l).wait_cum);
                trash2(d,w,l).wait_t = trash2(d,w,l).wait_t(trash.ue);
                % do inverse interpolation
                Distro_1.location(l).daytype(w).daytime(d).interp = griddedInterpolant(trash2(d,w,l).wait_cum',trash2(d,w,l).wait_t');
                % test
    %             for c=1:10000
    %                 trash.test(c,1) = Distro_1.location(l).daytype(w).daytime(d).interp(rand(1));
    %             end
    %             figure
    %             histogram(trash.test, 42*60)
    %             figure
    %             plot(trash.interp.Values)
            else
                Distro_1.location(l).daytype(w).daytime(d).interp = @(x) 0;
            end

        end
        e=e+1;
        show_progress(e,1,4*6)
    end
end

set(0,'DefaultFigureVisible','on');

% Check distribution
trash.whole_test = [];
for l=1:6
    for w=1:4
        for d=1:24
            % Amount corresponding to ecpected occurance
            trash.am = round(Distro_1.location(l).daytype(w).daytime(d).scale(1).exp_amount*10000);  
            trash.whole_test =[trash.whole_test; Distro_1.location(l).daytype(w).daytime(d).interp(rand(trash.am,1))];
        end
    end
end
% plot
figure 
hold on
histogram(trash.whole_test/3600, round(max(trash.whole_test/3600)*60), 'Normalization', 'probability', 'DisplayStyle', 'stairs')
trash.diff=(Good.data(trash.idle_case_el,8)-Good.data(trash.idle_case_el,15))/3600;
trash.diff(trash.diff<0)=0;
histogram(trash.diff, round(max(trash.diff)*60), 'Normalization', 'probability', 'DisplayStyle', 'stairs')
hold off
legend('Distribution', 'Expected')
xlabel('Idle time [h]')
ylabel('Normed occurance [100%]')
title('Idle time distribution comparison')
% Result: perfect match



% per daytype & location
close all
e=0;
tic
for l=1:6
    for w=1:4
%         figure('Units', 'normalized', 'OuterPosition', [0 0 1 1])
        trash.max_num = 0;
        for d=1:24
            trash.max_num = max([trash.max_num numel(trash2(d,w,l).stand_dis)]);
        end
        for d=1:24
            [~, interp(:,d)] = interpolate_with_zeros_fast([1:numel(trash2(d,w,l).stand_dis)], trash2(d,w,l).stand_dis, 1, trash.max_num, 1);
        end
%         bar3(interp)
%         ylabel('Standzeit [h]')
%         xlabel('Daytime [h]')
%         zlabel('Occurance')
%         title([Location(l).name, ' ', Distro_1.daytype(w).name, ' Standzeit Distro'])
        e=e+1;
        show_progress(e,1,4*6)
        clear interp
    end
end

% per power class
close all
figure
hold on
for p=1:7
   
    trash.e = find(Good.data(:,12)==p);
    trash.diff = (Good.data(trash.e,8)-Good.data(trash.e,15))/3600;
    trash.h = histogram(trash.diff, round(max(trash.diff)), 'Normalization', 'probability', 'DisplayStyle', 'stairs', 'DisplayName', Power_classes.names{p});
end
hold off
legend('show')
xlabel('Wait time [h]')
ylabel('probability')
title('Wait time distribution per power class')
% Result: no clear dependance on Power class




%% Idle time per Loc
d=3;
t=10;
trash.max = 0;
for l=1:numel(Location)
    trash2(l).idle = Distro_1.location(l).daytype(d).daytime(t).idle(rand(10000,1))/3600;
    trash.max = max([trash.max max(trash2(l).idle)]);
end


trash.max = 24;
figure
for l=1:numel(Location)
    subplot(2,3,l)
    hold on
    histogram(trash2(l).idle, round(max(trash2(l).idle)*4), 'Normalization', 'probability', 'DisplayStyle', 'stairs')
%     trash.el = get_same_elements(find(Good.data(:,17)==t), find(Good.data(:,16)==d));
%     trash.el = get_same_elements(trash.el, Location(l).LV_el);
    trash.el2 = Distro_1.location(l).daytype(d).elem_dt{t};
%     get_same_elements(trash.el, trash.el2)
%     [numel(get_same_elements(trash.el, trash.el2)), numel(trash.el), numel(trash.el2)]
%     
%     trash.el3 = get_same_elements(Location(l).LV_el, find(Good.data(Intern.daytype(d).elem,17)==t-1));
%     trash.el4 =  get_same_elements(find(Good.data(:,17)==t), find(Good.data(:,16)==d));
%     [Good.data(trash.el4,16), Good.data(trash.el4,17)]
%     
%     
%             trash.el5 = get_same_elements(Location(l).LV_el, find(Input.data(:,17)==t));
%             trash.el5 = get_same_elements(trash.el5, Intern.daytype(d).elem);
%             
%     [Good.data(trash.el5,16), Good.data(trash.el5,17)]
%     [min(ans); max(ans)]
% end
% 
% for l=1:numel(Location)
    
    trash.check_idle = Good.data(trash.el2,8)/3600-Good.data(trash.el2,15)/3600;
    histogram(trash.check_idle, round(max(trash.check_idle*4)),'Normalization','probability', 'DisplayStyle','stairs')
    hold off
    legend('Distro', 'Good')
    set(gca, 'XLim', [0 ceil(trash.max)])
    xlabel('Standzeit [h]')
    title(Location(l).name)
end

%%
for l=1:numel(Location)
    for d=1:4
        for t=1:24
            Distro_1.location(l).daytype(d).elem_dt{t} = get_same_elements(Location(l).LV_el, find(Input.data(:,17)==t-1));
            Distro_1.location(l).daytype(d).elem_dt{t} = get_same_elements(Distro_1.location(l).daytype(d).elem_dt{t}, Intern.daytype(d).elem);
        end
    end
end

[Good.data(Distro_1.location(l).daytype(d).elem_dt{t},16), Good.data(Distro_1.location(l).daytype(d).elem_dt{t},17)]
    [min(ans); max(ans)]
Good.data(Intern.daytype(d).elem,16)

                                        

%% Correcting Idle time distro


% get idle time elements
trash.idle_case_el = find(Input.data(:,13) == 2 | Input.data(:,13) == 3); 

for d = 1:24

    %% Get daytime elements                                                
    Distro_1.elem_dt{d} = find(Input.data(:,17)==d-1);
    %% Loop Through daytype & Location -> amount distribution              
    for w = 1:4
        % Loop through Location
        for l=1:numel(Location)
 %% Idle time                                                   
            % get idle times of this time and charge case 2 | 3
            trash.idle_elements = get_same_elements(trash.idle_case_el, Distro_1.location(l).daytype(w).elem_dt{d});
            trash.wait = Input.data(trash.idle_elements  ,8) - ...
                Input.data(trash.idle_elements ,15);
            % if data exists
            if numel(trash.wait)~=0
                % remove negative values (0 ... -30 s)
                trash.wait(trash.wait<0)=0;
                % get normalized distribution (1/10th h)
                trash.trash = histogram(trash.wait/3600, ...
                    max([1 round(max(trash.wait/3600)/trash.res)]), 'Normalization', 'probability');
                trash2(d,w,l).wait_dis = trash.trash.Values;
                % get time 
                trash2(d,w,l).wait_t = 3600*[0:trash.res:numel(trash2(d,w,l).wait_dis)*trash.res];
                % cumsum distribution
                trash2(d,w,l).wait_cum = [0 cumsum(trash2(d,w,l).wait_dis)];
                % remove same elements
                [trash2(d,w,l).wait_cum, trash.ue] = unique(trash2(d,w,l).wait_cum);
                trash2(d,w,l).wait_t = trash2(d,w,l).wait_t(trash.ue);
                % do inverse interpolation
                Distro_1.location(l).daytype(w).daytime(d).idle = griddedInterpolant(trash2(d,w,l).wait_cum',trash2(d,w,l).wait_t');
                % test
    %             for c=1:10000
    %                 trash.test(c,1) = Distro_1.location(l).daytype(w).daytime(d).interp(rand(1));
    %             end
    %             figure
    %             histogram(trash.test, 42*60)
    %             figure
    %             plot(trash.interp.Values)
            % if no data exists
            else
                Distro_1.location(l).daytype(w).daytime(d).idle = @(x) 0;
            end
        end
    end
end

