function [Files] = get_filenames(PATH)
    
    old_location = cd(PATH);

    
    Files = dir;

    % Ignore directories
    for c=1:numel(Files)
        trash.isdir(c,1) = Files(c).isdir;
    end
    Files = Files(~trash.isdir);

    
    cd(old_location)
    
end