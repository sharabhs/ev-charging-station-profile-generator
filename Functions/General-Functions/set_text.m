function [] = set_text(position, string, color, Interpreter, alignement_h, alignement_v, font_size, background)

    BA_Test_constants
    % example data
    
%     position = [15,3.5];
%     string = 'Ladezeit';
    
    if ~exist('background') || numel(background)==0
        background = 'none';
    end
    if ~exist('Interpreter') || numel(Interpreter)==0
        Interpreter = 'tex';
    end
    if ~exist('alignement_h') || numel(alignement_h)==0
        alignement_h = 'center';
    end
    if ~exist('alignement_v') || numel(alignement_v)==0
        alignement_v = 'middle';
    end
    if ~exist('font_size') || numel(font_size)==0
        font_size = 20;
    end
    if ~exist('color') || numel(color)==0
        color = Color_matrix{1};
    end
    t=text('Units', 'Data', 'Position', position, 'HorizontalAlignment', alignement_h, ...
    'VerticalAlignment', alignement_v, 'String', string); 
    t.FontName = 'Courier New';
    t.Color = color;
    t.FontSize = font_size;
    t.Interpreter = Interpreter;
    if strcmp(Interpreter, 'Latex')
        t.FontWeight = 'bold';
        t.FontName = 'Courier New';
    end
    t.BackgroundColor = background;
    
   % 'Fontweight', 'bold')%, 'Parent', ax)

end