function [Results] = load_mat(PATH)

% cd to directory
old_location = cd(PATH);

% read files
Files = dir;

% Ignore directories
for c=1:numel(Files)
    trash.isdir(c,1) = Files(c).isdir;
end
Files = Files(~trash.isdir);

% get csv or txt file
for c=1:numel(Files)
    trash.mat(c,1) = numel(strfind(Files(c).name, '.mat'));
end
Files = Files(find(trash.mat));

% load if only 1 file
if numel(Files)>1
    disp('Error: Multiple *.mat files found')
else
    load(Files.name);
end



% return to old location
cd(old_location)

end