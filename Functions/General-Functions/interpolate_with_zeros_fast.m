function [Output_x,Output_y] = interpolate_with_zeros_fast(Input_x, Input_y , Start, End, Resolution)
%% fast
Intern_x = [Start:Resolution:End]'/Resolution; % make integer elements out of double input_x
Output_y = zeros(numel(Intern_x), 1);
elements = round((Input_x-Start)/Resolution)+1;
Output_y(elements) = Input_y;
Output_x =Intern_x*Resolution;

% 750 times faster. 10 times less code. 1 Mio times less stupid