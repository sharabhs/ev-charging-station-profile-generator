function [Out_x, Out_y, length] = interpolate_sequence(In_x, In_y, Resol)
%     start;
%     Discrete.max_x;
%     Resol = Discrete.width;
%     In_x = rounded_x;
%     In_y = continous_y;

%     In_x = [0; .3; .3; .3; .6];
%     In_y = [2*In_x, In_x+2];
%     Resol = 0.1;
    % Ziel: [0 0 1 1 2 2 3 3 4 4 5 5 6  6]
    %       [0 n n n n n 6 6 8 n n n 12 n]
    
    [In_nu_of_rows, In_nu_of_cols] = size(In_y);
    [~, u_el] = unique(In_x);
    length = max(diff([u_el;In_nu_of_rows+1]));
    Min = min(In_x);
    Max = max(In_x);
    % create x array with repeating x numbers
    Out_x = repmat(Min:Resol:Max, length, 1);
    Out_x = Out_x(:);
    % create y array of nan
    Out_y = 0/0*ones(numel(Out_x),In_nu_of_cols);
    
    % find same input x numbers and increment them -> continously increasing array
    same=1;
    while numel(same)>0
        same = find(diff(In_x)==0);
        In_x(same)=In_x(same)+1/length*Resol;
    end
    
    % get elements of increasing x array
    el = round((In_x-Min)*length/Resol+1);
    Out_y(el,:) = In_y;
%     [Out.x, Out.y]
    
end

