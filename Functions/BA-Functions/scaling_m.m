%% get new scaled amount distro from 0 to 60 

set(0,'DefaultFigureVisible','on');
load Scale_fit.mat
clear trash

trash.number = 10000;
trash.max_scale = 300;
trash.max_cars = 120;

% get success probability and fit 
set(0,'DefaultFigureVisible','off');
eq = @(a,x) 1-exp(-a*x);
e=0;
hold on
tic
for l=1:6
    for w=1:4
        for d=1:24
            s=1;
            while s <= numel(Scale) && numel(Scale(s).location(l).daytype)>0 
                trash.rows = numel(Scale(s).location(l).daytype(w).distr(:,d));
                data = repelem([0:trash.rows-1], round(Scale(s).location(l).daytype(w).distr(:,d)*trash.number));
                [P_fix{d,w,l}(s),~] = mle(data,'distribution','binomial','ntrials',16*s);
                P_mean{d,w,l} = mean(P_fix{d,w,l});
                P_first{d,w,l} = P_fix{d,w,l}(1);
                s=s+1;
            end
            e=e+1;
            show_progress(e,10,24*4*6)
        end
    end
end
set(0,'DefaultFigureVisible','on');


% get distribution from fit
tic
e=0;
for l=1:6
    for w=1:4
        for d=1:24
            for s=1:trash.max_scale
                %trash.d = count_same_integers(binornd(2,Fit{d,w,l}(s),trash.number,1),0,2);
%                 Scale_fit.distr(:, d, s, w, l) = trash.d(:,2)/trash.number;
                trash.fix_distr(:, d, s, w, l) = binopdf([0:120], 16*s, P_mean{d,w,l})';
%                 Scale_fit.unlim_distr(:, d, s, w, l) = [(1-Fit_unlim{d,w,l}(s))^2; 2*Fit_unlim{d,w,l}(s)*(1-Fit_unlim{d,w,l}(s)); (Fit_unlim{d,w,l}(s))^2];
                Scale_fit.fix_exp_amount(d, s, w, l) = sum(trash.fix_distr(:, d, s, w, l) .* [0:numel(trash.fix_distr(:, d, s, w, l))-1]');
                if s<=numel(Distro_1.location(l).daytype(w).daytime(d).scale) && s<numel(Scale) && numel(Scale(s).location(l).daytype)>0 
                    trash.fix_exp_am(d, s, w, l) = Scale(s).location(l).daytype(w).mean_amount(:,d);
                end
            end
            e=e+1;
            show_progress(e,1,6*4*24)
        end
    end
end

 Scale_fit.new_lim_distr_m = [];
% Limit unlimited now
tic
e=0;
for l=1:6
    for w=1:4
        for d=1:24
            for s=1:trash.max_scale
                Scale_fit.new_lim_distr_m(:, d, s, w, l) =  trash.fix_distr(1:trash.max_cars+1, d, s, w, l);
                Scale_fit.new_lim_distr_m(61, d, s, w, l) = Scale_fit.new_lim_distr_m(trash.max_cars+1, d, s, w, l) + sum(trash.fix_distr(trash.max_cars+2:end, d, s, w, l));
                Scale_fit.new_lim_exp_am_m(d, s, w, l) = sum([0:trash.max_cars]' .* Scale_fit.new_lim_distr_m(:, d, s, w, l));
            end
            e=e+1;
            show_progress(e,10,4*6*24)
        end
    end
end



%% Pass to distro 
for l=1:6
    for w=1:4
        for d=1:24
            for s=1:trash.max_scale
                Distro_1.location(l).daytype(w).daytime(d).scale(s).new_lim_amount_m = Scale_fit.new_lim_distr_m(:, d, s, w, l);     
                Distro_1.location(l).daytype(w).daytime(d).scale(s).new_exp_amount_m = Scale_fit.new_lim_exp_am_m(d, s, w, l);        
            end
        end
    end
end

%% save
save('Scale_fit.mat', 'Scale_fit')
save('Distro_1.mat', 'Distro_1')

%% ---------------------------- Check result ------------------------------


%% Plot exemplary distributions
close all
w=1;
d=10;
l=1;
trash.max=20;
for l=[6]
    for s=[1 10 50 255]
        figure
        trash.distr = [Scale(s).location(l).daytype(w).distr(:,d); zeros(trash.max-numel(Scale(s).location(l).daytype(w).distr(:,d)),1)];
        bar([Scale_fit.new_lim_distr_m(1:trash.max, d, s, w, l) trash.distr(1:trash.max)])
        legend('Binomial Fit', 'Measured')
    title(['LV request distribution for scale: ', num2str(s), ' ', num2str(d-1), ':00 ', Location(l).name, ' ' Distro_1.daytype(w).name])
    xlabel('# cars requesting to charge')
    ylabel('Occurance [%]')

        set(gca, 'XTick', 1:trash.max, 'XTickLabel', 0:trash.max-1)
        trash.ytick = get(gca, 'YTick');
        set(gca, 'YTickLabel', trash.ytick*100)
    end
end

%% Check exp amount
close all
figure
hold on
for w=1:3
    e=0;
    for l=[6]
        e=e+1;
        for d= [11]
            plot(Scale_fit.new_lim_exp_am_m(d, :, w, l), ...
                'DisplayName', [Location(l).name, ', ', Distro_1.daytype(w).name, ', ', num2str(d), ':00 Fit'], ...
                'Color', Color_matrix{w}, 'LineStyle',  Line_matrix{4})
            c=1;
            while c<=numel(Scale) && numel(Scale(c).location(l).daytype(w).mean_amount)>0
%                 trash.exp(c) = Distro_1.location(l).daytype(w).daytime(d).scale(c).new_exp_amount;
                trash.exp(c) = Scale(c).location(l).daytype(w).mean_amount(d);
                c=c+1;
            end
            plot(trash.exp, ...
                'DisplayName', [Location(l).name, ', ', Distro_1.daytype(w).name, ', ', num2str(d), ':00'], ...
                'Color', Color_matrix{w}, 'LineStyle',  Line_matrix{e})
            trash.exp = [];
        end
    end
end
hold off
legend('show')
xlabel('Scaling of EV amount')
ylabel('#LVs/day/daytime')
title('scaled  expected limited amount')
% Result: perfect match!




