function [Output] = periodic_course_III(Input, period_unit)

%% Exemplary data
% close all
% % 
% Input = Charger(242).D_h;
% period_unit = 'week';
% c=243;
% Input = Charger(c).D_hour;
% TODO: 
% (- period amount)
% clear Intern Input

%% Constants
period_factor_matrix = [365; 31; 7; 1; 1/24; 1/24/60; 1/24/60/60];
UNITS = {'years'; 'months'; 'weeks'; 'days'; 'hours'; 'minutes'; 'seconds'};
UNIT  = {'year'; 'month'; 'week'; 'day'; 'hour'; 'minute'; 'second'; 'second'};
UNIT_F  = {'get_years'; 'get_months'; 'get_days'; 'get_weeks'; 'get_hours'; 'get_minutes'; 'get_seconds'};

START = [1; 1 ; 1; 1; 0 ; 0; 0; 0];
END = [Inf; 12 ; 53; 31; 23 ; 59; 59; 59];          % TODO: not necessarily 53 weeks, 31 days, ... ! ->? (eomday)

BA_Test_constants

%% Preparation
Intern.period_unit = [period_unit 's'];
Intern.unit_index = find(ismember(UNITS, Intern.period_unit));
if isfield(Input, 'datetime')
    Intern.datetime = Input.datetime;
else
    Intern.datetime = datetime(datestr(Input.x)); % takes long!
end
Intern.previous_unit = UNIT{Intern.unit_index+1};

% special case weeks
if strcmp(Intern.period_unit, 'weeks')
    Intern.datetime = Intern.datetime - days(1);
end

% elements per period:
Intern.elements_per_period = period_factor_matrix(Intern.unit_index)/Input.width;

%% Get Periods
Intern.period_unit = UNIT{Intern.unit_index};
Intern.periods = feval(Intern.period_unit, Intern.datetime);

% special case weeks
if strcmp(Intern.period_unit, 'weeks')
    Intern.datetime = Intern.datetime + days(1);
end

%% Filter out incomplete periods
% elements with change of period
Intern.transition_elements = [1; find(diff(Intern.periods))+1; numel(Intern.periods)];

%Check beginning of period
% Intern.datetime(transition_elements(end-1))
% periods(transition_elements(end-1))

% first and last
Intern.first_period_elements = [Intern.transition_elements(1):Intern.transition_elements(2)-1]';
Intern.last_period_elements = [Intern.transition_elements(end-1):Intern.transition_elements(end)]';

% if first date is not starting at 
if feval(Intern.previous_unit,min(Intern.datetime)) ~= START(Intern.unit_index+1)
    Intern.periods(Intern.first_period_elements) = [];
    Input.x(Intern.first_period_elements) = [];
    Input.sum(Intern.first_period_elements,:) = [];
    Input.mean(Intern.first_period_elements,:) = [];
    Input.amount(Intern.first_period_elements,:) = [];
%     Input.density(first_period_elements,:) = [];
    Intern.transition_elements = Intern.transition_elements - numel(Intern.first_period_elements);
    Intern.transition_elements(1) = [];
    Intern.last_period_elements = Intern.last_period_elements - numel(Intern.first_period_elements);
    Intern.last_period_elements(Intern.last_period_elements<1)=[];
end
if feval(Intern.previous_unit,max(Intern.datetime)) ~= END(Intern.unit_index+1)
    Intern.periods(Intern.last_period_elements) = [];
    Input.x(Intern.last_period_elements) = [];
    Input.sum(Intern.last_period_elements,:) = [];
    Input.mean(Intern.last_period_elements,:) = [];
    Input.amount(Intern.last_period_elements,:) = [];
%     Input.density(last_period_elements,:) = [];
    Intern.transition_elements(end) = [];
    if numel(Intern.transition_elements)>0
        Intern.transition_elements(end) = numel(Intern.periods);
    end
end
% TODO: UPDATE iNPUT.NUOFROWS ETC

%% Create 3d Matrix with layer per period

Intern.nu_of_periods = numel(Intern.transition_elements)-1;
Intern.max_steps_per_period = max(diff(Intern.transition_elements));

% create time matrices in datenum unit
Output.datenum = [0:Intern.max_steps_per_period-1]'*Input.width;
% Output.three_d_datenum = Output.datenum .* (ones(Intern.max_steps_per_period,1,Intern.nu_of_periods));

% loop through periods
if Intern.nu_of_periods>0
    for c = 1:Intern.nu_of_periods
        Intern.period_elements = [Intern.transition_elements(c):Intern.transition_elements(c+1)-1]';
        if max(Intern.period_elements)>numel(Input.sum(:,1))
            disp('ERROR')
        end
        Output.three_d_sum(:,:,c) = [Input.sum(Intern.period_elements,:); zeros(Intern.max_steps_per_period-numel(Intern.period_elements),Input.nu_of_cols)];
        Output.three_d_amount(:,:,c) = [Input.amount(Intern.period_elements,:); zeros(Intern.max_steps_per_period-numel(Intern.period_elements),1)];
        Output.three_d_mean(:,:,c) = [Input.mean(Intern.period_elements,:); 0/0*ones(Intern.max_steps_per_period-numel(Intern.period_elements),Input.nu_of_cols)];
        Output.three_d_std(:,:,c) = [Input.std_dev(Intern.period_elements,:); 0/0*ones(Intern.max_steps_per_period-numel(Intern.period_elements),Input.nu_of_cols)];
    end   
%% Merge to one period
    Output.period_amount = sum(Output.three_d_amount>0,3);
    Output.std_dev_amount = std(Output.three_d_amount,0, 3, 'omitnan');
    Output.mean_amount = my_nanmean(Output.three_d_amount, 3);
    Output.sum = sum(Output.three_d_sum, 3);
    Output.mean = my_nanmean(Output.three_d_mean, 3);
    Output.amount = sum(Output.three_d_amount,3);
    Output.nu_of_periods = Intern.nu_of_periods;
    Output.pooled_std = my_nanmean(repmat(Output.three_d_amount-1, 1, Input.nu_of_cols) .* Output.three_d_std,3);
    % Output.datetime = datetime(datestr(Output.datenum, TIME_FORMAT),
    % 'Format', TIME_FORMAT_DT); TODO: get this running
else 
    Output.period_amount = zeros(Intern.elements_per_period, Input.nu_of_cols);
    Output.std_dev_amount = nan*ones(Intern.elements_per_period, Input.nu_of_cols);
    Output.mean_amount = zeros(Intern.elements_per_period, Input.nu_of_cols);
    Output.sum = nan*ones(Intern.elements_per_period, Input.nu_of_cols);
    Output.mean = nan*ones(Intern.elements_per_period, Input.nu_of_cols);
    Output.amount = zeros(Intern.elements_per_period, 1);
    Output.nu_of_periods = 0;
    Output.pooled_std = nan*ones(Intern.elements_per_period, Input.nu_of_cols);
    Output.datenum = [0:Intern.elements_per_period-1]'*Input.width;
end
%% Convert Output time
% so far, Output.time is a datenum

if strcmp(period_unit, 'day')
    Output.datetime = datetime(datestr(Output.datenum, CLOCK_FORMAT), 'Format', CLOCK_FORMAT_DT);
end

% input_unit_index = find(ismember(UNITS, Input.unit));
% input_unit_factor = period_factor_matrix(input_unit_index);
% 
% numeric_function = UNIT_F{Input.unit};
% Output.unit_time = feval(numeric_function, (Output.time/input_unit_factor));

