function [] = show_progress(c, period, amount)

if round(c/period)==c/period
    percentage = c/amount*100;
    t=toc;
    disp(['Second ', num2str(t,4), ', Progress: ', num2str(percentage,3), ' %.'])
end

end

