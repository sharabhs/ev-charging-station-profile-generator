function [Distro] = get_distribution(Input, Charger, Scale, Location, Scale_fit)
%% Get Distribution of Pcs, #LVs, Energy and Duration for WD, Sa & Su      
disp('Get distribution...')
set(0,'DefaultFigureVisible','off');

%% Constants                                                               
BA_Test_constants
Trash1.e_res = 1;                                                           % [kWh]
Trash1.d_res = 60;                                                          % [s]
trash.res = 1/3600;                                                         % h    
trash.max_scale = 300;
if numel(Scale_fit)==0
    load Scale_fit
end

%% example data                                                            
% Input = Good;
% Input = Test_simu(S);
% Charger = Test_simu(S);
% clear Distro_1 Trash

%% Get weekday and weekenddays elements                                    
Distro.daytype(1).name = 'Weekday';
Distro.daytype(2).name = 'Saturday';
Distro.daytype(3).name = 'Sunday';
Distro.daytype(4).name = 'Allday';

for w=1:3
Intern.daytype(w).elem = find(Input.data(:,16)==w);
end

Intern.daytype(4).elem = find(Input.data(:,16));

%% Preparation                                                             
Trash2.max_amount=0;
for c=1:numel(Charger)
    Trash2.max_amount = max([max(Charger(c).D_h.amount), Trash2.max_amount]);
end
counter = 0;
tic

% get idle time elements
trash.idle_case_el = find(Input.data(:,13) == 2 | Input.data(:,13) == 3); 

%% Power Class Distro                                                      
Distro.power_class_distr = count_same_integers(Input.data(:,12), 1, Power_classes.nu_of_classes);
% normalized
Distro.power_class_distr_norm = [Distro.power_class_distr(:,1) Distro.power_class_distr(:,2)./sum(Distro.power_class_distr(:,2))];

%% Loop through daytime                                                    
for d = 1:24

    %% Get daytime elements                                                
    Distro.elem_dt{d} = find(Input.data(:,17)==d-1);

    %% Power class distribution                                            
    Distro.pc_mat_dt(:,[1,d+1]) = count_same_integers(Input.data(Distro.elem_dt{d},12), 1, Power_classes.nu_of_classes);
    % Normalize distribution           
    Distro.pc_mat_dt_norm = ...
        [Distro.pc_mat_dt(:,1), Distro.pc_mat_dt(:,2:end)./...
        repmat(sum(Distro.pc_mat_dt(:,2:end)),Power_classes.nu_of_classes,1)];

    %% Charge case distribution                                            
    Distro.cc_mat_dt(:,[1,d+1]) = count_same_integers(Input.data(Distro.elem_dt{d},13), 1, 5);
    % Normalize distribution                                          
    Distro.cc_mat_dt_norm = ...
        [Distro.cc_mat_dt(:,1), Distro.cc_mat_dt(:,2:end)./...
        repmat(sum(Distro.cc_mat_dt(:,2:end)),5,1)];

    %% Energy & Duration Distribution per power class & Location           

        % loop through power classes 
        for p=1:Power_classes.nu_of_classes
            for l=1:numel(Location)
                
                trash.el = get_same_elements(find(Input.data(:,12)==p), Location(l).LV_el);

                % Get Input elements of this daytime and power class combination
                Trash(d,p,l).elm = get_same_elements(Distro.elem_dt{d}, trash.el);

                % Energy (7) & Actual duration (15, rounded to min)  or measured duration (8)
                Trash(d,p,l).e = Input.data(Trash(d,p,l).elm,7);                                % kWh
                Trash(d,p,l).d = Input.data(Trash(d,p,l).elm,15);                               % s

                % If LVs exist in this combination
                if numel(Trash(d,p,l).elm)>0
                    % get amount of bins for histogram
                    Trash(d,p,l).hist_amount = round(max([(max(Trash(d,p,l).e)-min(Trash(d,p,l).e))/Trash1.e_res, ...
                        (max(Trash(d,p,l).d)-min(Trash(d,p,l).d))/Trash1.d_res]));
                    % have at least one bin
                    if  Trash(d,p,l).hist_amount==0  Trash(d,p,l).hist_amount=1; end
                    % create 3D distribution with histogram 
                    Trash(d,p,l).hist=histogram2(Trash(d,p,l).e, Trash(d,p,l).d, Trash(d,p,l).hist_amount);
                    Distro.location(l).daytime(d).power(p).e_d_distr = Trash(d,p,l).hist.Values;
                    % get X and Y vectors
                    Distro.location(l).daytime(d).power(p).d = linspace(min(Trash(d,p,l).d), max(Trash(d,p,l).d), numel(Distro.location(l).daytime(d).power(p).e_d_distr(1,:)));
                    Distro.location(l).daytime(d).power(p).e = linspace(min(Trash(d,p,l).e), max(Trash(d,p,l).e), numel(Distro.location(l).daytime(d).power(p).e_d_distr(:,1)));
                % If this combination doesnt exist -> zero distr.
                else
                    Distro.location(l).daytime(d).power(p).e_d_distr = 0;
                    Distro.location(l).daytime(d).power(p).e = 0;
                    Distro.location(l).daytime(d).power(p).d = 0;
                end
            end
        end
  
    %% Loop Through daytype & Location -> amount distribution              
    for w = 1:4
        % Loop through Location
        for l=1:numel(Location)
            %% Get Daytype & - time & Location elements                    
            Distro.location(l).daytype(w).elem_dt{d} = get_same_elements(Location(l).LV_el, find(Input.data(:,17)==d-1));
            Distro.location(l).daytype(w).elem_dt{d} = get_same_elements(Distro.location(l).daytype(w).elem_dt{d}, Intern.daytype(w).elem);

  
            %% Scaled fitted amount distribution                           
            for s=1:trash.max_scale
                Distro.location(l).daytype(w).daytime(d).scale(s).new_lim_amount_m = Scale_fit.new_lim_distr_m(:, d, s, w, l);
                Distro.location(l).daytype(w).daytime(d).scale(s).new_exp_amount_m = Scale_fit.new_lim_exp_am_m(d, s, w, l);
            end
            
            %% Idle time                                                   
            % get idle times of this time and charge case 2 | 3
            trash.idle_elements = get_same_elements(trash.idle_case_el, Distro.location(l).daytype(w).elem_dt{d});
            trash.wait = Input.data(trash.idle_elements  ,8) - ...
                Input.data(trash.idle_elements ,15);
            % if data exists
            if numel(trash.wait)~=0
                % remove negative values (0 ... -30 s)
                trash.wait(trash.wait<0)=0;
                % get normalized distribution (1/10th h)
                trash.trash = histogram(trash.wait/3600, ...
                    max([1 round(max(trash.wait/3600)/trash.res)]), 'Normalization', 'probability');
                trash2(d,w,l).wait_dis = trash.trash.Values;
                % get time 
                trash2(d,w,l).wait_t = 3600*[0:trash.res:numel(trash2(d,w,l).wait_dis)*trash.res];
                % cumsum distribution
                trash2(d,w,l).wait_cum = [0 cumsum(trash2(d,w,l).wait_dis)];
                % remove same elements
                [trash2(d,w,l).wait_cum, trash.ue] = unique(trash2(d,w,l).wait_cum);
                trash2(d,w,l).wait_t = trash2(d,w,l).wait_t(trash.ue);
                % do inverse interpolation
                Distro.location(l).daytype(w).daytime(d).idle = griddedInterpolant(trash2(d,w,l).wait_cum',trash2(d,w,l).wait_t');
                % test
    %             for c=1:10000
    %                 trash.test(c,1) = Distro_1.location(l).daytype(w).daytime(d).interp(rand(1));
    %             end
    %             figure
    %             histogram(trash.test, 42*60)
    %             figure
    %             plot(trash.interp.Values)
            % if no data exists
            else
                Distro.location(l).daytype(w).daytime(d).idle = @(x) 0;
            end
        end


    end
    
    %% Track progress                                                      
    counter = counter + 1;
    show_progress(counter, 1, 24);

end
    
%% Create Matrices                                                         
for l=1:numel(Location)
    for w=1:4
        for s=1:numel(Location(l).charger_el)
            for d=1:24
                Trash2.amount_mat{d,w,s,l} = Distro.location(l).daytype(w).daytime(d).scale(s).new_lim_amount_m;
                Trash2.exp_amount_mat(d,w,s,l) = Distro.location(l).daytype(w).daytime(d).scale(s).new_exp_amount_m;
            end
        end
    end
    Distro.pc_mat = sum(Distro.pc_mat_dt(:,2:end),2);
    Distro.pc_mat_norm = Distro.pc_mat/sum(Distro.pc_mat);
    Distro.cc_mat = sum(Distro.cc_mat_dt(:,2:end),2);
    Distro.cc_mat_norm = Distro.cc_mat/sum(Distro.cc_mat);
    Trash2.pc_mat_norm(:,l) = Distro.pc_mat_norm;
    Trash2.cc_mat_norm(:,l) = Distro.cc_mat_norm;
    Distro.location(l).max_scale = numel(Location(l).charger_el);
end
        
%% Get general info                                                        
for w=1:4
    for s=1:255
        Distro.daytype(w).scale(s).amount_per_day = sum(Trash2.exp_amount_mat(:,w,s));
    end
end

% Location name
for l=1:numel(Location)
    Distro.location(l).name = Location(l).name;
end

%% Finish                                                                  
set(0,'DefaultFigureVisible','on');

end

%% Check amount distro                                                     
% for d=1:24
%     Weekend(d) = sum(prod(Distro.weekend_dt(d).amount,2));
%     Weekday(d) = sum(prod(Distro.weekday_dt(d).amount,2));
% end
% figure
% hold on
% plot(Weekend)
% plot(Weekend*5/2)
% plot(Weekday)
% hold off
% legend('Weekend', 'Weekend-scaled', 'weekday')
