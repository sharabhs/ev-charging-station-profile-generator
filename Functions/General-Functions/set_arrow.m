function [] = set_arrow(PositionX, PositionY, Size, Color, Style)
    BA_Test_constants
    if ~exist('Size') || numel(Size)==0
        Size = 0.5;
    end
    if ~exist('Color') || numel(Color)==0
        Color = Color_matrix{1};
    end
    if ~exist('Style') || numel(Style) == 0
        Style = 'doublearrow';
    end
    ar = annotation(gcf, Style, PositionX, PositionY);
    ar.Color = Color;
    ar.LineWidth = Size;
    if strcmp(Style, 'doublearrow')
        ar.Head1Width =  Size*4;
        ar.Head2Width =  Size*4;
        ar.Head1Length =  Size*4;
        ar.Head2Length =  Size*4;
    else
        ar.HeadWidth =  Size*4;
        ar.HeadLength =  Size*4;
    end
    ar.HeadStyle = 'plain';

end






