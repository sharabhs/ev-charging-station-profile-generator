function [Size] = mbyte(VARIABLE)
a = whos('VARIABLE');
Size = a.bytes * 10^-6;
end
