function [full_D_h] = fill_discretized_h(D_h)
    
%% Example INput
% D_h = Test_simu(S).D_h;

clear Trash full_D_h

% Fill full week
full_D_h.miss_d_start = mod(7-2+weekday(datetime(datestr(D_h.min_x))),7);
full_D_h.miss_d_end = mod(7+2-1-weekday(datetime(datestr(D_h.max_x))),7);
full_D_h.miss_h_start = round((D_h.min_x - floor(D_h.min_x))*24)+24*full_D_h.miss_d_start;
full_D_h.miss_h_end = round((ceil(D_h.max_x) - D_h.max_x)*24)-1 +24*full_D_h.miss_d_end;
full_D_h.x = [(D_h.min_x-full_D_h.miss_h_start/24:1/24:D_h.min_x-1/24)';...
    D_h.x; (D_h.max_x+1/24:1/24:D_h.max_x+full_D_h.miss_h_end/24)' ]; 
full_D_h.amount = [zeros(full_D_h.miss_h_start,1);...
    D_h.amount; zeros(full_D_h.miss_h_end,1) ]; 
full_D_h.min_x = min(full_D_h.x);
full_D_h.max_x = max(full_D_h.x);


full_D_h.min_x = round(min(full_D_h.min_x));
full_D_h.max_x = round(max(full_D_h.max_x)*24)/24;


[full_D_h.nu_of_rows, full_D_h.nu_of_cols] = size(full_D_h.amount);

full_D_h.width = D_h.width;
full_D_h.datetime = datetime(datestr(D_h.x));

end

