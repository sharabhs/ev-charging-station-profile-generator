%% Import H0 profiles by HTWB
load MAT_74_Loadprofiles_1min_W_var 
Raw_H0.PL1 = PL1;
Raw_H0.PL2 = PL2;
Raw_H0.PL3 = PL3;
Raw_H0.time = time_datenum_MEZ;
Raw_H0.datetime = datetime(datestr(time_datenum_MEZ));
clear PL1 PL2 PL3 time_datenum_MEZ QL1 QL2 QL3 time_datevec_MEZ

figure 
plot(Raw_H0.datetime, Raw_H0.PL1(:,1))
xlabel('Time')
ylabel('Power [W]')

[Raw_H0.nu_of_rows, Raw_H0.nu_of_cols] = size(Raw_H0.PL1);