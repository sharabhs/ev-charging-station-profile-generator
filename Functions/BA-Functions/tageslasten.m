%% Lasten im Tagesverlauf

%% Clean
clear 
close all
clc

%% Constants
LOCATION = 4;
SCALE1 = 1;
SCALE2 = 1;
SCALE3 = 1;
SCALE4 = 1;
SERVER = false;

BA_Test_constants

%% Load Data
% H0
import_h0

% PV
Simu_result = load_mat('../../PowerFactory/Ergebnisse/Simu_EV1_10_min');

% EV
for x=1
if SERVER
    EV=load('Results\Profiles\Home-scale42-10min-MULTIPLEX-365d-11-Apr-2019\Home-scale42-10min-MULTIPLEX-365d-11-Apr-2019_1.mat');

    EV(2)=load('Results\Profiles\Home-scale17-10min-365d-13-Apr-2019\Home-scale17-10min-365d-13-Apr-2019_1.mat');

    EV(3)=load('Results\Profiles\Home-scale1-10min-365d-12-Apr-2019\Home-scale1-10min-365d-12-Apr-2019_1.mat');
else
    EV=load('Results/Profiles/Home-scale42-10min-MULTIPLEX-365d-11-Apr-2019/Home-scale42-10min-MULTIPLEX-365d-11-Apr-2019_1.mat');

    EV(2)=load('Results/Profiles/Home-scale17-10min-365d-13-Apr-2019/Home-scale17-10min-365d-13-Apr-2019_1.mat');

    EV(3)=load('Results/Profiles/Home-scale1-10min-365d-12-Apr-2019/Home-scale1-10min-365d-12-Apr-2019_1.mat'); 
end

EV(1).scale = 42;
EV(1).multiplexing = 1;
EV(1).name = 'Home S42 Multiplex';
EV(1).anzahl = 10;

EV(2).scale = 17;
EV(2).multiplexing = 0;
EV(2).name = 'Home S17';
EV(2).anzahl = 3;

EV(3).scale = 1;
EV(3).multiplexing = 0;
EV(3).name = 'Home S1';
EV(3).anzahl = 1;
trash.space = {''; '                   ';'                     '};
end

%% Prepare data

% H0
for x=1
    trash.number = 365*24*60;

    % accumulate phases
    Raw_H0.all_phases = Raw_H0.PL1(1:trash.number,:) + Raw_H0.PL2(1:trash.number,:) + Raw_H0.PL3(1:trash.number,:);
    
    % Create new profiles
    trash.shift = 15; % min
    for c=1:100-Raw_H0.nu_of_cols
        Raw_H0.all_phases(:,c+Raw_H0.nu_of_cols) = [zeros(trash.shift,1); Raw_H0.all_phases(1:end-trash.shift,c)];
    end
    [Raw_H0.nu_of_rows, Raw_H0.nu_of_cols] = size(Raw_H0.all_phases);
    
    trash.mean = mean(Raw_H0.all_phases/1000,2);
    
    % daily profile
    Last.Tag.H0 = mean(reshape(trash.mean, 24*60, []),2)';
    
    % Weekly profile
    Last.Woche.H0 =  mean(reshape(trash.mean(24*60*3+1:end-7200), 24*60*7, []),2);
    Last.Woche.H0 = mean(reshape(Last.Woche.H0, 60, []));
    
end


% PV
for x=1
    Last.Tag.PV = mean(Simu_result.Power_wirk.day_mean_m,2);
    Last.Woche.PV = mean(Simu_result.Power_wirk.week_mean_m,2);
    Last.Woche.PV = mean(reshape(Last.Woche.PV, 6, []))';
end



% EV
for x=1
    Last.Woche.EV = [];
for c=1:numel(EV)
    trash.min = minutes(EV(c).Test_simu.actual_power.datetime_step);
    Last.Tag.EV(:,c) = mean(reshape(mean(reshape(EV(c).Test_simu.actual_power.data, 24*60/trash.min, []),2), ...
    10/trash.min, []),1)';
    trash.Woche.EV(:,c) = mean(reshape(mean(reshape(EV(c).Test_simu.actual_power.data(24*60/trash.min*3+1:end-7200/trash.min), 7*24*60/trash.min, []),2), ...
    10/trash.min, []),1)';
    Last.Woche.EV(:,c) = mean(reshape(trash.Woche.EV(:,c), 6, []))';
end
end

%% Plot 

% Relative Lasten im Tagesverlauf
for x=1
figure
hold on
plot([0:1/6:24-1/6], Last.Tag.PV*14/90, 'Displayname', ['14 PV-Anlagen,                      [', num2str(round(sum(Last.Tag.PV*14/90)/6,2)),' kWh/Tag/HH]'], 'Linewidth', 4)
for c=1:numel(EV)
    trash.EV_per_HH = Last.Tag.EV(:,c)*EV(c).anzahl/90;
    plot([0:1/6:24-1/6], trash.EV_per_HH, 'Displayname', ...
        [num2str(EV(c).anzahl), ' LS, ', EV(c).name, ',', trash.space{c},' [', num2str(round(sum(trash.EV_per_HH)/6, 2)), ' kWh/Tag/HH]'],...
        'Linewidth', 4)
end
plot([0:1/60:24-1/60], Last.Tag.H0, 'Displayname', ['90 Haushalte,                      [', num2str(round(sum(Last.Tag.H0)/60,2)),' kWh/Tag/HH]'], 'Linewidth', 4)
hold off
change_x_ticks(6, 24, 1)
l=legend('show', 'Location', 'Northwest');
l.EdgeColor = Color_matrix{8};
l.TextColor = Color_matrix{8};
xlabel('Uhrzeit')
y=ylabel('Mittlere Leistung pro Haushalt [kW]');
y.Position(1) = y.Position(1)*1.01;
title('Relative Lasten im Tagesverlauf')
set_color
set(gca, 'XLim', [0 24])
end

mean(Last.Tag.H0*24*365)


% Relative Lasten im Wochenverlauf
for x=1
figure
hold on
plot([0:1:24*7-0.1], Last.Woche.PV*14/90, 'Displayname', '14 PV-Anlagen', 'Linewidth', 4)
for c=1:numel(EV)
    plot([0:1:24*7-1], Last.Woche.EV(:,c)*EV(c).anzahl/90, 'Displayname', [num2str(EV(c).anzahl), ' LS, ', EV(c).name], 'Linewidth', 4)
end
plot([0:1:24*7-1], Last.Woche.H0, 'Displayname', '90 Haushalte', 'Linewidth', 4)
hold off
set(gca, 'YLim', [0 2.3])
week_label([],9)
l=legend('show', 'Location', 'Northwest');
l.EdgeColor = Color_matrix{8};
l.TextColor = Color_matrix{8};
y=ylabel('Mittlere Leistung pro Haushalt [kW]');
y.Position(1) = y.Position(1)*1.01;
title('Relative Lasten im Wochenverlauf')

set(gca, 'XLim', [0 24*7])

set_color
end
