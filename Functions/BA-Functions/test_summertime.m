%% Convert UTC+1 files to summertime (Europe/Berlin)

%% Clean up
clear 

%% Constants
tic
TIME_FORMAT_S = 'yyyy-MM-dd HH:mm:ss';
TIME_FORMAT_S_Str = 'yyyy-mm-dd HH:MM:SS';
% time_start = datetime('2010-03-28 00:00:00', 'Format',TIME_FORMAT_S , 'TimeZone', 'UTC+1');
% time_end = datetime('2010-10-31 04:00:00', 'Format', TIME_FORMAT_S, 'TimeZone', 'UTC+1');



SERVER = true;
DELIMITER = ';';

if SERVER
    location = '..\..\Summertime_Problem\Summertime3';
else
    location = '../../Summertime_Problem\Summertime3';
end

%% Import files in the directory
disp('Import files...')
old_location = cd(location);

Files = dir;
% Ignore directories
for c=1:numel(Files)
    trash.isdir(c,1) = Files(c).isdir;
end
Files = Files(~trash.isdir);
Files.name

% Import every file in directory
for c=1:numel(Files)
    disp(['File ', num2str(c), ' of ' num2str(numel(Files))])
    if ~SERVER
        fullname = fullfile(Files(c).folder, Files(c).name);
    else
        fullname = Files(c).name;
    end
    Data = readtable(fullname,'ReadVariableNames',false, 'Delimiter', DELIMITER);
    
 %% Convert time
     toc
     disp('Convert time...')
 
    New_time = datetime(Data{:,1}, 'TimeZone', 'UTC+1');
    New_time.TimeZone = 'Europe/Berlin';
    New_time = datestr(New_time,TIME_FORMAT_S_Str);
    
   
%% save Files
    toc
    disp('Save profile ...')
    
    period = round(numel(New_time(:,1))/100);
    
    cd Conversion
    fid = fopen(['ST_' fullname], 'w');
    for d = 1:numel(New_time(:,1)) % Loop through each time/value row
        fprintf(fid, '%s', New_time(d,:)); % Print the time string
        for e=2:numel(Data(1,:))
            fprintf(fid, ';%12.6f', double(Data{d,e})); % Print the data values
        end
        fprintf(fid, '\n');
        show_progress(d,period, numel(New_time(:,1)))
    end
    fclose(fid);
    
    
    cd ..
end
    
%% Return to dir

cd(old_location)
toc
disp('done.')



