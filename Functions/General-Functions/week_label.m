function [] = week_label(STEPS_PER_DAY, DISTANCE, FONTSIZE, LANGUAGE, SHORT)

BA_Test_constants

if ~exist('STEPS_PER_DAY') || numel(STEPS_PER_DAY)~=1
    STEPS_PER_DAY = 24;
end

if ~exist('DISTANCE') || numel(DISTANCE)~=1
    DISTANCE = 7;
end

if ~exist('FONTSIZE') || numel(FONTSIZE)~=1
    FONTSIZE = 18;
end

if ~exist('LANGUAGE') || numel(LANGUAGE)==0
    LANGUAGE = 'DE';
end

if ~exist('SHORT') || numel(SHORT)~=1
    SHORT = true;
end

trash.ca=gca;
trash.distance = min(trash.ca.YLim)-diff(trash.ca.YLim)*DISTANCE/100; 
if strcmp(LANGUAGE, 'EN') 
    trash.names =  {'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'};
elseif strcmp(LANGUAGE, 'DE')
    trash.names =  {'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag'};
end

if SHORT
    for c=1:numel(trash.names)
        trash.names{c} = trash.names{c}(1:2);
    end
end

% Daytime label
for d=1:24*7
    label{d} = [num2str((mod(d-1,24))) ':00'];
end
set(trash.ca, 'XTick', 0:STEPS_PER_DAY/2:STEPS_PER_DAY*7, 'XTickLabel', label(1:12:end))

% Day label
for c=1:7
    text('Units', 'Data', 'Position', [STEPS_PER_DAY/2+STEPS_PER_DAY*(c-1),trash.distance], ...
        'HorizontalAlignment', 'center', 'String', trash.names{c}, 'Fontweight', 'bold',...
        'Color', Color_matrix{8}, 'FontSize', FONTSIZE)
    text('Units', 'Data', 'Position', [STEPS_PER_DAY*(c-1),trash.distance],...
        'HorizontalAlignment', 'center', 'String', '|', 'Fontweight', 'bold',...
        'Color', Color_matrix{8}, 'FontSize', FONTSIZE)
end
 
text('Units', 'Data', 'Position', [STEPS_PER_DAY*(c),trash.distance],...
    'HorizontalAlignment', 'center', 'String', '|', 'Fontweight', 'bold',...
    'Color', Color_matrix{8}, 'FontSize', FONTSIZE)
 end