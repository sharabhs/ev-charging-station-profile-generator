function [Idle] = idle_or_not(Duration, Energy, Power_class)
% Get boolean that indicates, whether the car will stay idle after charging
% or drive off immediately

% Input: single row
    
%% Example Input
% Duration = 3600;
% Energy = 3.6;
% Power_class = 1;
% clear Intern
BA_Test_constants
    
%% Settings
TOLERANCE =  0.05;    % * max power -> kW


% below full slope time
if Duration <= Power_classes.full_slope_time(Power_class)
    % Energy smaller than slope energy
    if Energy <= (Power_classes.min(Power_class).*Duration + 1/2*Power_classes.slope(Power_class).*Duration.^2)/3600*(1+TOLERANCE)
        Idle = true;
    else
        Idle = false;
    end
% Above full slope time
else
    % Energy smaller than  Max Energy - only full slope energy
    if Energy <= Duration/3600*Power_classes.max(Power_class) - Power_classes.full_only_slope_energy(Power_class)*(1-TOLERANCE)
        Idle = true;
    else
        Idle = false;
    end
end

end



    