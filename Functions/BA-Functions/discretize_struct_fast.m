
% Discretize values, accumulate them over a certain width
function[Discrete] = discretize_struct_fast(continous_x, continous_y, width, start, unit)
    
%% test
% continous_x=Actual_power.datenum;
% continous_y=Actual_power.data;
% width=1/24/60;
% unit='minutes';
% start = 0;
% clear Discrete
% tic
% width = 1/24; % 1/24 = 1 h, 'cause it's in days, actually :|
% start = 0;
% continous_x = Good.data(:,3);
% continous_y = Good.data;
% unit ='hours';
% clear Discrete
% clear Intern
% p=3;
% c=133;
% continous_x=Good.data(Charger(c).elements,3);
% continous_y = 1;
% width=1/24;
% start=0;
% unit='hour';

%%

    Discrete.width = width;
    rounded_x = floor(continous_x/Discrete.width)*Discrete.width;
    start = max([start, min(rounded_x)]);
    
    % fill alibi y vector, if there is none
    if numel(continous_y) == 1
        continous_y = ones(numel(rounded_x),1);
    end
    
    % remove rows smaller than start value
    ignored_elements = find(rounded_x < start);
    rounded_x([ignored_elements]) = [];
    continous_y([ignored_elements],:) = [];
    
    % get size
    Discrete.min_x = max([round(start/Discrete.width)*Discrete.width, min(rounded_x)]); 
    Discrete.max_x = max(rounded_x);
    [Discrete.nu_of_rows, Discrete.nu_of_cols] = size(continous_y);
    Discrete.nu_of_discrete_rows = round((max(rounded_x)-Discrete.min_x)/Discrete.width)+1;
    
    % Init variables
    Discrete.x = Discrete.min_x:Discrete.width:Discrete.nu_of_discrete_rows*width+Discrete.min_x-Discrete.width;
    Discrete.x = Discrete.x.';

    if min(continous_x)>367 && Discrete.nu_of_discrete_rows < 1000000
        Discrete.datetime = datetime(datestr(Discrete.x));
    end
    
    %% fast
    
    % fill with 0
    
%     for c=1:Discrete.nu_of_cols
%         [Discrete.x, Discrete.y(:,c)] = interpolate_with_zeros_fast(rounded_x, continous_y(:,c), start, Discrete.max_x, Discrete.width);
%     end
%     Discrete.x=unique(Discrete.x);
    % fill with nan
    [Intern.x_seq, Intern.y_seq, Discrete.length] = interpolate_sequence(rounded_x, continous_y, Discrete.width);

    % reshape into 3D:
    % Col: data variables, Row: Same period, 3rd: Period

    Discrete.Three_d = permute(reshape(Intern.y_seq', Discrete.nu_of_cols, Discrete.length, Discrete.nu_of_discrete_rows), [2 1 3]);
      
    Discrete.sum = my_nansum(Discrete.Three_d, 1);
    Discrete.mean = my_nanmean(Discrete.Three_d, 1);
    Discrete.std_dev = std(Discrete.Three_d, 0, 1, 'omitnan');
    Discrete.amount = sum(~isnan(Discrete.Three_d(:,1,:)),1);
    Discrete.max = max(Discrete.Three_d,[],1);
    Discrete.min = min(Discrete.Three_d,[],1);
         
%     figure
%     errorbar(Discrete.mean(:,7), Discrete.std_dev(:,7))
    
    % reshape into 2D
 
    Discrete.sum = permute(reshape(Discrete.sum, Discrete.nu_of_cols, Discrete.nu_of_discrete_rows, 1), [2 1 3]);
    Discrete.mean = permute(reshape(Discrete.mean, Discrete.nu_of_cols, Discrete.nu_of_discrete_rows,1), [2 1 3]);
    Discrete.std_dev = permute(reshape(Discrete.std_dev, Discrete.nu_of_cols, Discrete.nu_of_discrete_rows,1), [2 1 3]);
    Discrete.amount = reshape(Discrete.amount, Discrete.nu_of_discrete_rows,1);
    Discrete.max = permute(reshape(Discrete.max, Discrete.nu_of_cols, Discrete.nu_of_discrete_rows,1), [2 1 3]);
    Discrete.min = permute(reshape(Discrete.min, Discrete.nu_of_cols, Discrete.nu_of_discrete_rows,1), [2 1 3]);
      
    % TODO: make reshape in one step -> faster  
      
    %% lame
    
    % Get nonzero elements
    Discrete.nonzero_e = find(Discrete.amount);
    Discrete.nu_of_nonzero = numel(Discrete.nonzero_e);
    
    % pass unit
    Discrete.unit = unit;
end