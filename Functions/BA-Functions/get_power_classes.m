function [Good, Power_classes] = get_power_classes(Good, Power_classes)
% get power classes per user
%% Constants
BA_Test_constants

%% max power lines as reference [already done]

% from energy over duration graph: (max power lines)
Intern.values = [ 
% duration [h]  energy [kWh]
   3.4556e+00   7.5005e+01
   4.4778e+00   7.4664e+01
   3.9031e+00   4.4041e+01
   4.3047e+00   3.0563e+01
   1.6464e+00   7.2050e+00
   2.1136e+00   7.4130e+00];

[Intern.nu_of_classes, ~] = size(Intern.values);

% calculate power
Intern.values(:,3) = Intern.values(:,2) ./ Intern.values(:,1);              % kW

Intern.line_headers = {'Duration [h]', 'Charge Amount [kWh]', 'Power [kWh/h]'};


%% Allocate peak power & power class          
trash.u_ID = unique(Good.data(:,2));
for c=1:numel(trash.u_ID)
    trash.ID_el = find(Good.data(:,2)==trash.u_ID(c));
    Good.data(trash.ID_el,11) = max(Good.data(trash.ID_el,10));
end

Good.headers{11} = '(11) Peak Power [kW]';
Good.units{11} = '[kW]';

% round:
rounded = roundtowardvec(Good.data(:,11), Power_classes.upper, 'ceil');
for c=1:Good.no_of_rows
    Good.data(c,12) = find(rounded(c)==Power_classes.upper);
end

Good.headers{12} = '(12) Power_class';
Good.units{12} = '[]';
[Good.no_of_rows, Good.no_of_cols] = size(Good.data);

for c = 1:Power_classes.nu_of_classes
    Power_classes.elements{c} = find(Good.data(:,12)==c);
end
Power_classes.elements = Power_classes.elements';
Power_classes.distribution = count_same_integers(Good.data(:,12));

for d=1:24
    Trash2.d = count_same_integers(Good.data(Good.data(:,17)==d-1,12));
    Power_classes.dt_distro(d,:) = Trash2.d(:,2)';
end

 Power_classes.dt_norm_distro =  Power_classes.dt_distro./sum(Power_classes.dt_distro(:));
 %% TODO: is different from All_ahrger distro!
end

