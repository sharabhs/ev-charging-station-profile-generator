function [Scale] = get_scaled_amount(Charger, Location, Period)

%% Settings                                                                
SAVE = false;
PLOT = false;

%% Preparation                                                             
% clc
disp('Get scaled amount distribution...')
% clear Trash Pair Scale

% Get full_D_h.x daytime and daytype elements
for d=1:24
    % Weekday
    Trash.discr_elem{1,d} = sort([d:24*7:numel(Charger(1).full_D_h.x), d+24*1:24*7:numel(Charger(1).full_D_h.x), ...
        d+24*2:24*7:numel(Charger(1).full_D_h.x), d+24*3:24*7:numel(Charger(1).full_D_h.x), ...
         d+24*4:24*7:numel(Charger(1).full_D_h.x)]');
    % Saturday 
    Trash.discr_elem{2,d} = [d+24*5:24*7:numel(Charger(1).full_D_h.x)]';
    % Sunday
    Trash.discr_elem{3,d} = [d+24*6:24*7:numel(Charger(1).full_D_h.x)]';
    % All days
    Trash.discr_elem{4,d} = [d:24:numel(Charger(1).full_D_h.x)]';
end

% Max number of days
Trash.max_amount = max(Period.Day_h.amount);


%% ----------------- Get scaled Distro from filled arrays -----------------
tic
counter = 0;

%% Loop through every Location                                             
for l = 1:numel(Location)
    disp(Location(l).name);
    
    %% Loop through every scale
    for s=1:numel(Location(l).charger_el)    
        % Allocate location name                                           
        Scale(s).location(l).name = Location(l).name;      
        % allocate daytype names
        Scale(s).location(l).daytype(1).name = 'Weekday';
        Scale(s).location(l).daytype(2).name = 'Saturday';
        Scale(s).location(l).daytype(3).name = 'Sunday';
        Scale(s).location(l).daytype(4).name = 'Allday';
        % init variables
        Scale(s).full_D_h.amount = zeros(numel(Charger(1).full_D_h.x),1);
        for w=1:4
            Trash.daytype(w).distro_dt_mat = [];
        end
        
        %% loop through every group                                        
        for p=1:floor(numel(Location(l).charger_el)/s)
            Pair(p).full_D_h.amount = zeros(numel(Charger(1).full_D_h.x),1);
            % Loop through every group member (step: s)
            for e=p*s-s+1:p*s
                % add D_h 
                Pair(p).full_D_h.amount = Pair(p).full_D_h.amount + Charger(Location(l).charger_el(e)).full_D_h.amount;
            end
            % get Distribution per daytime & daytype       
            for dt=1:24
                for w=1:4
                    Trash.daytype(w).distro_dt = count_same_integers(Pair(p).full_D_h.amount(Trash.discr_elem{w,dt}),0,Trash.max_amount);
                    Trash.daytype(w).distro_dt_mat(:,dt, p) = Trash.daytype(w).distro_dt(:,2);
                end
            end
        end
        Trash.P(s)=p;
        Trash.E(s)=e;
        
        %% Loop through every weekday type                                 
        for w=1:4
            % get mean distr per charger and mean amount 
            Trash.mean = mean(Trash.daytype(w).distro_dt_mat,3);
            Scale(s).location(l).daytype(w).distr = Trash.mean./repmat(sum(Trash.mean),numel(Trash.mean(:,1)),1);
            Scale(s).location(l).daytype(w).mean_amount = sum(Scale(s).location(l).daytype(w).distr.*repmat([0:Trash.max_amount]',1,24));
            Scale(s).location(l).daytype(w).daily_mean = sum(Scale(s).location(l).daytype(w).mean_amount);
      end
        counter = counter+1;
        show_progress(counter, 1, numel(Charger)*2)
        
    end
end
disp('done.')

%% Plot                                                                    
if PLOT                                   
set(0,'DefaultFigureVisible','on');

% Plot daily mean development
figure
hold on
for c=1:1:numel(Location(l).charger_el)      
    scatter(c,Scale(c).location(l).daytype(4).daily_mean,'.b')
    scatter(c,Scale(c).location(l).daytype(4).lim_daily_mean, '.r')
end
% plot(Trash.P)
% plot(Trash.E)
hold off
legend('unlimited', 'limited')
title('Scaling of daily mean amount')
xlabel('Scale Factor')
ylabel('Daily Mean amount')
    
% plot 8:00 distro
% figure
% hold on
% for c=1:1:255
% %     plot([0:19], Scale(c).distr(1:20,9), 'b')
%     plot([0:2], Scale(c).ad_lim_dis(:,9), '--')
% end
% hold off
% title('Limited Distribution of daily amount at 8:00')
% xlabel('Amount')
% ylabel('Occurance')

figure
hold on
for c=1:1:255
    plot([0:19], Scale(c).distr(1:20,9))
end
hold off
title('Distribution of daily amount at 8:00')
xlabel('Amount')
ylabel('Occurance')

figure
bar([Scale(1).lim_dis_norm(:,9), Scale(70).lim_dis_norm(:,9), Scale(255).lim_dis_norm(:,9)])
legend('Scale 1', 'Scale 2', 'Scale 3')
xlabel('Nu of Cars')
ylabel('Probability [100%]')


figure
bar([Scale(1).location(6).daytype(4).distr     distr(1:20,9), Scale(70).distr(1:20,9), Scale(255).distr(1:20,9)])
legend('Scale 1', 'Scale 2', 'Scale 3')
xlabel('Nu of Cars')
ylabel('Probability [100%]')

% plot mean amount over daytime
figure
hold on
d=0;
e=0;
f=0;
for l=1:5
    e=e+1;
    d=0;
    for  s=[1:3:10]
        d=d+1;
        for w=4
            f=f+1;
            plot(Scale(s).location(l).daytype(w).lim_mean_amount, 'LineStyle', Line_matrix{d}, 'Color', Color_matrix{e})
            Name{f} = [Scale(s).location(l).name, ', ' Scale(s).location(l).daytype(w).name, ', Scale: ', num2str(s)];
            Scale(s).location(l).daytype(w).name;
        end
    end
end
hold off
legend(Name)
xlabel('Time [h]')
ylabel('Mean #LVs per day')
title('Scaled daily LV amount limited')

for l=1:6
    numel(Location(l).charger_el)
end

end

%% remove unnecessary zero data from scale distr                           
tic
e=0;
for l=1:6
    for w=1:4
        for s=1:numel(Scale)
            if numel(Scale(s).location(l).daytype)>0
                trash.cum = cumsum(sum(Scale(s).location(l).daytype(w).distr~=0,2));
                trash.same = find(trash.cum == max(trash.cum));
                trash.same(1) = [];
                Scale(s).location(l).daytype(w).distr(trash.same,:) = [];
            end
        end
        e=e+1;
    end
end

%% Save results                                                            
if SAVE
save('Scale.mat', 'Scale')
end


end