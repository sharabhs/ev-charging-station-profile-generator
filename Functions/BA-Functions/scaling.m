%% Estimate distribution changed by scale factor 


%% Preparation                                                             
clc
disp('Get scaled amount distribution...')
clear Trash Pair Scale
Trash.min_x = 1/0;
Trash.max_x = 0;    

% Fill full day
for c=1:255
%     datetime(datestr(Charger(c).D_h.min_x))
%     datetime(datestr(max(Charger(c).full_D_h.x)))
    Trash.miss_d_start = mod(7-2+weekday(datetime(datestr(Charger(c).D_h.min_x))),7);
    Trash.miss_d_end = mod(7+2-1-weekday(datetime(datestr(Charger(c).D_h.max_x))),7);
    Trash.miss_h_start = round((Charger(c).D_h.min_x - floor(Charger(c).D_h.min_x))*24)+24*Trash.miss_d_start;
    Trash.miss_h_end = round((ceil(Charger(c).D_h.max_x) - Charger(c).D_h.max_x)*24)-1 +24*Trash.miss_d_end;
    Charger(c).full_D_h.x = [(Charger(c).D_h.min_x-Trash.miss_h_start/24:1/24:Charger(c).D_h.min_x-1/24)';...
        Charger(c).D_h.x; (Charger(c).D_h.max_x+1/24:1/24:Charger(c).D_h.max_x+Trash.miss_h_end/24)' ]; 
    Charger(c).full_D_h.amount = [zeros(Trash.miss_h_start,1);...
        Charger(c).D_h.amount; zeros(Trash.miss_h_end,1) ]; 
    Charger(c).full_D_h.min_x = min(Charger(c).full_D_h.x);
    Charger(c).full_D_h.max_x = max(Charger(c).full_D_h.x);
end

for c=1:255
    Trash.min_x = min(Trash.min_x, Charger(c).full_D_h.min_x);
    Trash.max_x = max(Trash.max_x, Charger(c).full_D_h.max_x);
end
Trash.min_x = round(Trash.min_x);
Trash.max_x = round(Trash.max_x*24)/24;

Trash.nu_of_rows = round((Trash.max_x - Trash.min_x)*24)+1;
% Trash.daytime = hour(datetime(..
Trash.max_amount = max(Period.Day_h.amount);

% Get weekday and daytime elements for full discrete array:     
for d=1:24
    Trash.discr_elem{1,d} = sort([d:24*7:numel(Charger(1).full_D_h.x), d+24*1:24*7:numel(Charger(1).full_D_h.x), ...
        d+24*2:24*7:numel(Charger(1).full_D_h.x), d+24*3:24*7:numel(Charger(1).full_D_h.x), ...
         d+24*4:24*7:numel(Charger(1).full_D_h.x)]');
    Trash.discr_elem{2,d} = [d+24*5:24*7:numel(Charger(1).full_D_h.x)]';
    Trash.discr_elem{3,d} = [d+24*6:24*7:numel(Charger(1).full_D_h.x)]';
    Trash.discr_elem{4,d} = [d:24:numel(Charger(1).full_D_h.x)]';
end

%% Repeat Charger arrays -> All same length                                
for c=1:255
    % repeat vector to fit max length
    Trash.start_rep_factor = ceil((Charger(c).full_D_h.min_x-Trash.min_x)/numel(Charger(c).full_D_h.x));
    Trash.rep_factor = ceil(Trash.nu_of_rows/numel(Charger(c).full_D_h.x));
    Charger(c).full_D_h.amount = repmat(Charger(c).full_D_h.amount, Trash.rep_factor,1);
    Charger(c).full_D_h.amount = Charger(c).full_D_h.amount(1:Trash.nu_of_rows);
    Charger(c).full_D_h.x = [Trash.min_x:1/24:Trash.max_x]';
end

% %% Get scaled Distro with conservative interpolation
% tic
% % Loop through every scale
% for s=1:255
%     Scale(s).D_h.amount = zeros(Trash.nu_of_rows,1);
%     Trash.distr_dt_mat = [];
%     % loop through every group
%     for p=1:floor(255/s)
%         Pair(p).D_h.amount = zeros(Trash.nu_of_rows,1);
%         Trash.added_zeros = 0;
%         Trash.group_min_x = 1/0;
%         Trash.group_max_x = 0;
%         % Loop through every group member
%         for e=p*s-s+1:p*s
%             % add D_h 
%             [Trash.interpolated_x,Trash.interpolated_y]  = interpolate_with_zeros_fast(Charger(e).D_h.x, Charger(e).D_h.amount, Trash.min_x, Trash.max_x, 1/24);
%             Pair(p).D_h.amount = Pair(p).D_h.amount + Trash.interpolated_y;
%             Trash.group_min_x = min([Trash.group_min_x, Charger(e).D_h.min_x]); 
%             Trash.group_max_x = max([Trash.group_max_x, Charger(e).D_h.max_x]);
%         end
%         Trash.added_zeros = round(Trash.group_min_x-Trash.min_x) + round(Trash.max_x-Trash.group_max_x);
%         % get Distribution per daytime
%         for dt=1:24
%             Trash.distro_dt = count_same_integers(Pair(p).D_h.amount(dt:24:end),0,Trash.max_amount);
%             Trash.distro_dt(1:10,:,1);
%             Trash.distro_dt(1,2) = Trash.distro_dt(1,2)- Trash.added_zeros;
%             Trash.distr_dt_mat(:,dt, p) = Trash.distro_dt(:,2);
%         end
%     end
%     Trash.P(s)=p;
%     Trash.E(s)=e;
%     show_progress(s, 1, 255)
%     % get mean
%     Scale(s).distr = mean(Trash.distr_dt_mat,3);
%     Scale(s).mean_amount = sum(Scale(s).distr.*repmat([0:Trash.max_amount]',1,24))./sum(Scale(s).distr);
%     Scale(s).daily_mean = sum(Scale(s).mean_amount);
% end

%% ----------------- Get scaled Distro from filled arrays -----------------
tic
counter = 0;

%% Loop through every Location                                             
for l = 1:numel(Location)
    disp(Location(l).name);
    
    %% Loop through every scale
    for s=1:numel(Location(l).charger_el)    
        % Allocate location name                                           
        Scale(s).location(l).name = Location(l).name;      
        % allocate daytype names
        Scale(s).location(l).daytype(1).name = 'Weekday';
        Scale(s).location(l).daytype(2).name = 'Saturday';
        Scale(s).location(l).daytype(3).name = 'Sunday';
        Scale(s).location(l).daytype(4).name = 'Allday';
        % init variables
        Scale(s).full_D_h.amount = zeros(Trash.nu_of_rows,1);
        for w=1:4
            Trash.daytype(w).distro_dt_mat = [];
        end
        
        %% loop through every group                                        
        for p=1:floor(numel(Location(l).charger_el)/s)
            Pair(p).full_D_h.amount = zeros(Trash.nu_of_rows,1);
            % Loop through every group member (step: s)
            for e=p*s-s+1:p*s
                % add D_h 
                Pair(p).full_D_h.amount = Pair(p).full_D_h.amount + Charger(Location(l).charger_el(e)).full_D_h.amount;
            end
            % get Distribution per daytime & daytype       
            for dt=1:24
                for w=1:4
                    Trash.daytype(w).distro_dt = count_same_integers(Pair(p).full_D_h.amount(Trash.discr_elem{w,dt}),0,Trash.max_amount);
                    Trash.daytype(w).distro_dt_mat(:,dt, p) = Trash.daytype(w).distro_dt(:,2);
                end
            end
        end
        Trash.P(s)=p;
        Trash.E(s)=e;
        
        %% Loop through every weekday type                                 
        for w=1:4
            % get mean distr per charger and mean amount 
            Trash.mean = mean(Trash.daytype(w).distro_dt_mat,3);
            Scale(s).location(l).daytype(w).distr = Trash.mean./repmat(sum(Trash.mean),numel(Trash.mean(:,1)),1);
            Scale(s).location(l).daytype(w).mean_amount = sum(Scale(s).location(l).daytype(w).distr.*repmat([0:Trash.max_amount]',1,24));
            Scale(s).location(l).daytype(w).daily_mean = sum(Scale(s).location(l).daytype(w).mean_amount);
            % shift to max 2
            Scale(s).location(l).daytype(w).lim_distr = Scale(s).location(l).daytype(w).distr(1:3,:);
            Scale(s).location(l).daytype(w).lim_distr(3,:) = Scale(s).location(l).daytype(w).lim_distr(3,:)+sum(Scale(s).location(l).daytype(w).distr(4:end,:));
            Scale(s).location(l).daytype(w).lim_mean_amount = sum(repmat([0:2]',1,24).*Scale(s).location(l).daytype(w).lim_distr);
            Scale(s).location(l).daytype(w).lim_daily_mean = sum(Scale(s).location(l).daytype(w).lim_mean_amount);
        end
        counter = counter+1;
        show_progress(counter, 1, numel(Charger)*2)
        
    end
end
disp('done.')

%% Plot                                                                    
                                   
set(0,'DefaultFigureVisible','on');

% Plot daily mean development
figure
hold on
for c=1:1:numel(Location(l).charger_el)      
    scatter(c,Scale(c).location(l).daytype(4).daily_mean,'.b')
    scatter(c,Scale(c).location(l).daytype(4).lim_daily_mean, '.r')
end
% plot(Trash.P)
% plot(Trash.E)
hold off
legend('unlimited', 'limited')
title('Scaling of daily mean amount')
xlabel('Scale Factor')
ylabel('Daily Mean amount')
    
% plot 8:00 distro
figure
hold on
for c=1:1:255
%     plot([0:19], Scale(c).distr(1:20,9), 'b')
    plot([0:2], Scale(c).ad_lim_dis(:,9), '--')
end
hold off
title('Limited Distribution of daily amount at 8:00')
xlabel('Amount')
ylabel('Occurance')

figure
hold on
for c=1:1:255
    plot([0:19], Scale(c).distr(1:20,9))
end
hold off
title('Distribution of daily amount at 8:00')
xlabel('Amount')
ylabel('Occurance')

figure
bar([Scale(1).lim_dis_norm(:,9), Scale(70).lim_dis_norm(:,9), Scale(255).lim_dis_norm(:,9)])
legend('Scale 1', 'Scale 2', 'Scale 3')
xlabel('Nu of Cars')
ylabel('Probability [100%]')


figure
bar([Scale(1).location(6).daytype(4).distr     distr(1:20,9), Scale(70).distr(1:20,9), Scale(255).distr(1:20,9)])
legend('Scale 1', 'Scale 2', 'Scale 3')
xlabel('Nu of Cars')
ylabel('Probability [100%]')

% plot mean amount over daytime
figure
hold on
d=0;
e=0;
f=0;
for l=1:5
    e=e+1;
    d=0;
    for  s=[1:3:10]
        d=d+1;
        for w=4
            f=f+1;
            plot(Scale(s).location(l).daytype(w).lim_mean_amount, 'LineStyle', Line_matrix{d}, 'Color', Color_matrix{e})
            Name{f} = [Scale(s).location(l).name, ', ' Scale(s).location(l).daytype(w).name, ', Scale: ', num2str(s)];
            Scale(s).location(l).daytype(w).name;
        end
    end
end
hold off
legend(Name)
xlabel('Time [h]')
ylabel('Mean #LVs per day')
title('Scaled daily LV amount limited')

for l=1:6
    numel(Location(l).charger_el)
end

%% make unlimited Scale smaller                                            
tic
e=0;
for l=1:6
    for w=1:4
        for s=1:numel(Scale)
            if numel(Scale(s).location(l).daytype)>0
            trash.cum = cumsum(sum(Scale(s).location(l).daytype(w).distr~=0,2));
            trash.same = find(trash.cum == max(trash.cum));
            trash.same(1) = [];
            Scale(s).location(l).daytype(w).distr(trash.same,:) = [];
            end
        end
        e=e+1;
        show_progress(e,1,4*6)
    end
end

%% Save results                                                            
save('Scale.mat', 'Scale')


%% Check sum                                                               
for c=1:255
    Scale(c).lim_sum = sum(Scale(c).lim_dis);    
    Scale(c).sum = sum(Scale(c).distr);
end

%% Get Transmission coefficients                                           

for c=2:255
    Scale(c).trans_coeff = Scale(c).lim_dis./Scale(c-1).lim_dis;
end

figure
hold on
for c=2:255
    scatter(c, Scale(c).trans_coeff(1,8), '.b')
    scatter(c, Scale(c).trans_coeff(2,8), '.r')
    scatter(c, Scale(c).trans_coeff(3,8), '.g')
end
hold off

% get transision matrix
for c=1:255
Scale(c).lim_dis_norm = Scale(c).lim_dis./Scale(c).lim_sum;
end

Scale(1).lim_dis_norm


%% Markov Chain                                                            
TRANS = [.9 .1; .05 .95];

EMIS = [1/6, 1/6, 1/6, 1/6, 1/6, 1/6;...
7/12, 1/12, 1/12, 1/12, 1/12, 1/12];

[seq,states] = hmmgenerate(10,TRANS,EMIS);

for c=1:255
    seq(1:3,c) = round(Scale(c).lim_dis(:,8)/sum(Scale(c).lim_dis(:,8))*1000);
    
end
state = ones(3,255);
[a,b]=hmmestimate(seq, state)

%% Exponential Fit                                                         
% P=exp(
cftool([0:2]',Scale(1).lim_dis(:,9))

%% Binomial Fitting                                                        
l=6;
w=4;
d=8;
for d=1:24
tic
for s=1:255
P1(s,1) = sqrt(Distro_1.location(l).daytype(w).daytime(d).scale(s).lim_amount(1));
P2(s,1) = sqrt(Distro_1.location(l).daytype(w).daytime(d).scale(s).lim_amount(3));
Sum(s) = sum(Distro_1.location(l).daytype(w).daytime(d).scale(s).lim_amount);
end


show_progress(d,1,24)
figure
hold on
plot(P1)
plot(P2)
plot(P1+P2)
plot(Sum)
hold off
legend('P1', 'P2', 'P1+P2', 'Sum')
title([num2str(d), ':00'])
end

%% most likelyhood estimate                                                

% get success probability and fit 
eq = @(a,x) 1-exp(-a*x);
e=0;
hold on
for l=1:6
    for w=1:4
        for d=1:24
            for s=1:numel(Distro_1.location(l).daytype(w).daytime(d).scale)
                data = repelem([0:2], round(Distro_1.location(l).daytype(w).daytime(d).scale(s).lim_amount*10000));
                [P{d,w,l}(s),~] = mle(data,'distribution','binomial','alpha',.05,'ntrials',2);
            end
            
            f= fit([1:s]',P{d,w,l}', eq, 'StartPoint', 0);
            Fit{d,w,l}=f;
            e=e+1;
            show_progress(e,10,24*4*6)
        end
    end
end

% Check success probability fit
% e=1;
for l=1:6
    figure
    hold on
    for w=1:4
        c=0;
        for d=[4 9 12 18 22]
            c=c+1;
            plot(Fit{d,w,l})
            plot(P{d,w,l}, 'DisplayName', [Distro_1.daytype(w).name, ', ', num2str(d), ':00'], 'Color', Color_matrix{c}, 'LineStyle',  Line_matrix{w})
%             Name{e}=[Distro_1.daytype(w).name, ', ', num2str(d), ':00'];
%             e=e+1;
        end
    end
    hold off
    title(Location(l).name)
    legend('show')
    xlabel('Scaling of EV amount')
    ylabel('Probability')
end






% get distribution from fit

trash.number = 10000;
% 
tic
e=0;
for l=1:6
    for w=1:4
        for d=1:24
            for s=1:500
                %trash.d = count_same_integers(binornd(2,Fit{d,w,l}(s),trash.number,1),0,2);
%                 Scale_fit.distr(:, d, s, w, l) = trash.d(:,2)/trash.number;
                Scale_fit.distr(:, d, s, w, l) = [(1-Fit{d,w,l}(s))^2; 2*Fit{d,w,l}(s)*(1-Fit{d,w,l}(s)); (Fit{d,w,l}(s))^2];
                Scale_fit.exp_amount(d, s, w, l) = sum(Scale_fit.distr(:, d, s, w, l) .* [0:2]');
                if s<=numel(Distro_1.location(l).daytype(w).daytime(d).scale)
                    Scale_fit.mean_distr_error(d, s, w, l) = (mean(Scale_fit.distr(:, d, s, w, l) ./ ...
                        Distro_1.location(l).daytype(w).daytime(d).scale(s).lim_amount)-1)*100;
                    trash.exp_am(d, s, w, l) = Distro_1.location(l).daytype(w).daytime(d).scale(s).exp_amount;
                end
            end
            e=e+1;
            show_progress(e,1,6*4*24)
        end
    end
end

% check deviation from distribution
for l=1:6
    figure
    hold on
    for w=1:4
        c=0;
        for d=[4 9 12 18 22]
            c=c+1;
            plot(Scale_fit.mean_distr_error(d, 1:numel(Distro_1.location(l).daytype(w).daytime(d).scale), w, l), 'DisplayName', [Distro_1.daytype(w).name, ', ', num2str(d), ':00'], ...
                'Color', Color_matrix{c}, 'LineStyle',  Line_matrix{w})
        end
    end
    hold off
    title([Location(l).name, ': Deviation from original distribution'])
    legend('show')
    xlabel('Scaling of EV amount')
    ylabel('Deviation [%]')
end

% check expected amount
for l=1:6
    figure('Units', 'normalized', 'OuterPosition', [0 0 1 1])
    hold on
    for w=1:4
        c=0;
        for d=[9 22]
            c=c+1;
            plot(Scale_fit.exp_amount(d, 1:500, w, l), ...
                'DisplayName', [Distro_1.daytype(w).name, ', ', num2str(d), ':00 Fit'], ...
                'Color', Color_matrix{c}, 'LineStyle',  Line_matrix{w}, 'LineWidth', 2)
            plot(trash.exp_am(d, :, w, l), ...
                'DisplayName', [Distro_1.daytype(w).name, ', ', num2str(d), ':00 Actual'], ...
                'Color', 'black', 'LineStyle',  Line_matrix{w})
        end
    end
    hold off
    title([Location(l).name, ': Expected amount comparison'])
    legend('show')
    xlabel('Scaling of EV amount')
    ylabel('#LVs/day/daytime')
    
end

save_figures(6)

% exemplary distr bar diagrams
close all
d=9;
for s= [1, 51, numel(Distro_1.location(l).daytype(w).daytime(d).scale)]
figure
bar([Scale_fit.distr(:, d, s, w, l), Distro_1.location(l).daytype(w).daytime(d).scale(s).lim_amount])
legend('Fit', 'Actual')
title(['Scale: ', num2str(s)])
end

%% unlimited Binomial fit                                                  
trash.max_scale = 300;
trash.max_cars = 100;
trash.number = 10000;
trash.n = 3600;

set(0,'DefaultFigureVisible','off');
% get success probability and fit 
eq = @(a,x) 1-exp(-a*x);
e=0;
hold on
tic
for l=1:6
    for w=1:4
        for d=1:24
            s=1;
            while s <= numel(Scale) && numel(Scale(s).location(l).daytype)>0 
                trash.rows = numel(Scale(s).location(l).daytype(w).distr(:,d));
                data = repelem([0:trash.rows-1], round(Scale(s).location(l).daytype(w).distr(:,d)*trash.number));
                [P_unlim{d,w,l}(s),~] = mle(data,'distribution','binomial','ntrials',trash.n);
                s=s+1;
            end
            s=s-1;
            f= fit([1:s]',P_unlim{d,w,l}', eq, 'StartPoint', 0);
            Scale_fit.unlim_fit{d,w,l}=f;
            e=e+1;
            show_progress(e,10,24*4*6)
        end
    end
end

set(0,'DefaultFigureVisible','on');

% Check succes probability fit
for l=1:6
    figure
    hold on
    for w=1:4
        c=0;
        for d=[4 9 19]
            c=c+1;
            plot(Scale_fit.unlim_fit{d,w,l})
            plot(P_unlim{d,w,l}, 'DisplayName', [Distro_1.daytype(w).name, ', ', num2str(d), ':00'], 'Color', Color_matrix{c}, 'LineStyle',  Line_matrix{w})
%             Name{e}=[Distro_1.daytype(w).name, ', ', num2str(d), ':00'];
%             e=e+1;
        end
    end
    hold off
    title(Location(l).name)
    legend('show')
    xlabel('Scaling of EV amount')
    ylabel('Probability')
end


% get distribution from fit
trash.number = 10000;
tic
e=0;
for l=1:6
    for w=1:4
        for d=1:24
            for s=1:trash.max_scale
                %trash.d = count_same_integers(binornd(2,Fit{d,w,l}(s),trash.number,1),0,2);
%                 Scale_fit.distr(:, d, s, w, l) = trash.d(:,2)/trash.number;
                Scale_fit.unlim_distr(:, d, s, w, l) = binopdf([0:trash.n], trash.n, Scale_fit.unlim_fit{d,w,l}(s))';
%                 Scale_fit.unlim_distr(:, d, s, w, l) = [(1-Fit_unlim{d,w,l}(s))^2; 2*Fit_unlim{d,w,l}(s)*(1-Fit_unlim{d,w,l}(s)); (Fit_unlim{d,w,l}(s))^2];
                Scale_fit.unlim_exp_amount(d, s, w, l) = sum(Scale_fit.unlim_distr(:, d, s, w, l) .* [0:numel(Scale_fit.unlim_distr(:, d, s, w, l))-1]');
                if s<=numel(Distro_1.location(l).daytype(w).daytime(d).scale)
%                     trash.dev = ...
%                           ((Scale_fit.unlim_distr(1:trash.n, d, s, w, l) ./ ...
%                         Scale(s).location(l).daytype(w).distr(1:trash.n,d))-1)*100;
% %                         ((Scale_fit.unlim_distr(1:numel(Scale(s).location(l).daytype(w).distr(:,1)), d, s, w, l) ./ ...
%                     trash.dev(trash.dev==Inf) = 100;
                    Scale_fit.mean_unlim_distr_error(d, s, w, l) = mean(trash.dev);
                    trash.unlim_exp_am(d, s, w, l) = Scale(s).location(l).daytype(w).mean_amount(:,d);
                end
            end
            e=e+1;
            show_progress(e,1,6*4*24)
        end
    end
end


% check deviation from distribution
for l=1:6
    figure
    hold on
    for w=1:4
        c=0;
        for d=[4 9 12 18 22]
            c=c+1;
            plot(Scale_fit.mean_unlim_distr_error(d, 1:numel(Distro_1.location(l).daytype(w).daytime(d).scale), w, l), 'DisplayName', [Distro_1.daytype(w).name, ', ', num2str(d), ':00'], ...
                'Color', Color_matrix{c}, 'LineStyle',  Line_matrix{w})
        end
    end
    hold off
    title([Location(l).name, ': Deviation from original distribution'])
    legend('show')
    xlabel('Scaling of EV amount')
    ylabel('Deviation [%]')
end

% check expected amount
for l=1:6
    figure('Units', 'normalized', 'OuterPosition', [0 0 1 1])
    hold on
    for w=1:4
        c=0;
        for d=[9 22]
            c=c+1;
            plot(Scale_fit.unlim_exp_amount(d, 1:trash.max_scale, w, l), ...
                'DisplayName', [Distro_1.daytype(w).name, ', ', num2str(d), ':00 Fit'], ...
                'Color', Color_matrix{c}, 'LineStyle',  Line_matrix{w}, 'LineWidth', 2)
            plot(trash.unlim_exp_am(d, :, w, l), ...
                'DisplayName', [Distro_1.daytype(w).name, ', ', num2str(d), ':00 Actual'], ...
                'Color', 'black', 'LineStyle',  Line_matrix{w})
        end
    end
    hold off
    title([Location(l).name, ': Expected amount comparison'])
    legend('show')
    xlabel('Scaling of EV amount')
    ylabel('#LVs/day/daytime')
    
end


% exemplary distr bar diagrams
close all
d=9;
l=6;
w=4;
for s= [1, 20,50,100, numel(Distro_1.location(l).daytype(w).daytime(d).scale)]
figure
bar([Scale_fit.unlim_distr(1:17, d, s, w, l), Scale(s).location(l).daytype(w).distr(1:17,d)])
legend('Fit', 'Actual')
title(['Scale: ', num2str(s)])
end

%% Limit unlimited now                                                     

l=6;
w=4;
d=9;
for s=1:trash.max_scale
    Scale_fit.new_lim_distr(:, d, s, w, l) =  Scale_fit.unlim_distr(1:3, d, s, w, l);
    Scale_fit.new_lim_distr(3, d, s, w, l) = Scale_fit.new_lim_distr(3, d, s, w, l) + sum(Scale_fit.unlim_distr(4:end, d, s, w, l));
    Scale_fit.new_lim_exp_am(d, s, w, l) = sum([0:2]' .* Scale_fit.new_lim_distr(:, d, s, w, l));
end


figure
hold on
plot(Scale_fit.new_lim_exp_am(d, :, w, l))
for c=1:255
    trash.exp(c) = Distro_1.location(l).daytype(w).daytime(d).scale(c).exp_amount;
end
plot(trash.exp)
hold off
legend('Fit', 'actual')

%% Fixed P, changing  N                                                    


% get success probability and fit 
set(0,'DefaultFigureVisible','off');
eq = @(a,x) 1-exp(-a*x);
e=0;
hold on
tic
for l=1:6
    for w=1:4
        for d=1:24
            s=1;
            while s <= numel(Scale) && numel(Scale(s).location(l).daytype)>0 
                trash.rows = numel(Scale(s).location(l).daytype(w).distr(:,d));
                data = repelem([0:trash.rows-1], round(Scale(s).location(l).daytype(w).distr(:,d)*trash.number));
                [P_fix{d,w,l}(s),~] = mle(data,'distribution','binomial','ntrials',16*s);
                P_mean{d,w,l} = mean(P_fix{d,w,l});
                P_first{d,w,l} = P_fix{d,w,l}(1);
                s=s+1;
            end
            e=e+1;
            show_progress(e,10,24*4*6)
        end
    end
end
set(0,'DefaultFigureVisible','on');


% Check succes probability
figure
hold on
for l=1:5
    for w=1:3
        for d=[9]
            c=c+1;
            plot(P_fix{d,w,l}, 'DisplayName', [Location(l).name, ', ', Distro_1.daytype(w).name, ', ', num2str(d-1), ':00'], 'Color', Color_matrix{l}, 'LineStyle',  Line_matrix{w})
            plot([1, numel(P_fix{d,w,l})], [P_mean{d,w,l}, P_mean{d,w,l}], 'DisplayName', 'Fit', 'Color', Color_matrix{l}, 'LineStyle',  Line_matrix{4})
        end
    end
end
hold off
title('succes probability')
legend('show')
xlabel('Scaling of EV amount')
ylabel('Probability')


% get distribution from fit
tic
e=0;
for l=1:6
    for w=1:4
        for d=1:24
            for s=1:trash.max_scale
                %trash.d = count_same_integers(binornd(2,Fit{d,w,l}(s),trash.number,1),0,2);
%                 Scale_fit.distr(:, d, s, w, l) = trash.d(:,2)/trash.number;
                Scale_fit.fix_distr(:, d, s, w, l) = binopdf([0:100], 16*s, P_mean{d,w,l})';
%                 Scale_fit.unlim_distr(:, d, s, w, l) = [(1-Fit_unlim{d,w,l}(s))^2; 2*Fit_unlim{d,w,l}(s)*(1-Fit_unlim{d,w,l}(s)); (Fit_unlim{d,w,l}(s))^2];
                Scale_fit.fix_exp_amount(d, s, w, l) = sum(Scale_fit.fix_distr(:, d, s, w, l) .* [0:numel(Scale_fit.fix_distr(:, d, s, w, l))-1]');
                if s<=numel(Distro_2.location(l).daytype(w).daytime(d).scale)
                    trash.fix_exp_am(d, s, w, l) = Scale(s).location(l).daytype(w).mean_amount(:,d);
                end
            end
            e=e+1;
            show_progress(e,1,6*4*24)
        end
    end
end



% check expected amount
for l=1:6
    figure('Units', 'normalized', 'OuterPosition', [0 0 1 1])
    hold on
    for w=1:4
        c=0;
        for d=[9 22]
            c=c+1;
            plot(Scale_fit.fix_exp_amount(d, :, w, l), ...
                'DisplayName', [Distro_1.daytype(w).name, ', ', num2str(d), ':00 Fit'], ...
                'Color', Color_matrix{c}, 'LineStyle',  Line_matrix{w}, 'LineWidth', 2)
            plot(trash.fix_exp_am(d, :, w, l), ...
                'DisplayName', [Distro_1.daytype(w).name, ', ', num2str(d), ':00 Actual'], ...
                'Color', 'black', 'LineStyle',  Line_matrix{w})
        end
    end
    hold off
    title([Location(l).name, ': Expected unlimited amount comparison'])
    legend('show')
    xlabel('Scaling of EV amount')
    ylabel('#LVs/day/daytime')
end

% Check exemplary distro
close all
w=1;
d=9;
l=1;
for s=[1 10 round(numel(Distro_1.location(l).daytype(w).daytime(d).scale)/2) numel(Distro_1.location(l).daytype(w).daytime(d).scale)]
    figure
    bar([Scale_fit.fix_distr(1:numel(Scale(s).location(l).daytype(w).distr(:,d)), d, s, w, l) Scale(s).location(l).daytype(w).distr(:,d)])
    legend('Fit', 'Actual')
title(['Distro for scale: ', num2str(s), ' ', num2str(d-1), ':00 ', Location(l).name, ' ' Distro_1.daytype(w).name])
xlabel('# cars requesting to charge')
ylabel('Occurance')
end

% check limited distro (done later)
close all
w=2;
d=11;
l=3;
for s=[1 10 round(numel(Distro_1.location(l).daytype(w).daytime(d).scale)/2) numel(Distro_1.location(l).daytype(w).daytime(d).scale)]
    figure
    bar([Scale_fit.new_lim_distr(:, d, s, w, l) Distro_1.location(l).daytype(w).daytime(d).scale(s).lim_amount])
    legend('Fit', 'Actual')
title(['Limited distro for scale: ', num2str(s), ' ', num2str(d-1), ':00 ', Location(l).name, ' ' Distro_1.daytype(w).name])
xlabel('# cars requesting to charge')
ylabel('Occurance')
end


% Limit unlimited now
tic
e=0;
for l=1:6
    for w=1:4
        for d=1:24
            for s=1:trash.max_scale
                Scale_fit.new_lim_distr(:, d, s, w, l) =  Scale_fit.fix_distr(1:3, d, s, w, l);
                Scale_fit.new_lim_distr(3, d, s, w, l) = Scale_fit.new_lim_distr(3, d, s, w, l) + sum(Scale_fit.fix_distr(4:end, d, s, w, l));
                Scale_fit.new_lim_exp_am(d, s, w, l) = sum([0:2]' .* Scale_fit.new_lim_distr(:, d, s, w, l));
            end
            e=e+1;
            show_progress(e,10,4*6*24)
        end
    end
end


close all
figure
hold on
for w=1:3
    e=0;
    for l=[4 6]
        e=e+1;
        for d= [11 22]
            plot(Scale_fit.new_lim_exp_am(d, :, w, l), ...
                'DisplayName', [Location(l).name, ', ', Distro_1.daytype(w).name, ', ', num2str(d), ':00 Fit'], ...
                'Color', Color_matrix{w}, 'LineStyle',  Line_matrix{4})
            for c=1:numel(Distro_1.location(l).daytype(w).daytime(d).scale)-1
                trash.exp(c) = Distro_1.location(l).daytype(w).daytime(d).scale(c).exp_amount;
            end
            plot(trash.exp, ...
                'DisplayName', [Location(l).name, ', ', Distro_1.daytype(w).name, ', ', num2str(d), ':00'], ...
                'Color', Color_matrix{w}, 'LineStyle',  Line_matrix{e})
            trash.exp = [];
        end
    end
end
hold off
legend('show')
xlabel('Scaling of EV amount')
ylabel('#LVs/day/daytime')
title('scaled  expected limited amount')

%% Check exemplary limited distros                                         
close all
w=2;
d=11;
l=3;
for s=[1 10 round(numel(Distro_1.location(l).daytype(w).daytime(d).scale)/2) numel(Distro_1.location(l).daytype(w).daytime(d).scale)]
    figure
    bar([Scale_fit.new_lim_distr(:, d, s, w, l) Distro_1.location(l).daytype(w).daytime(d).scale(s).lim_amount])
    legend('Fit', 'Actual')
title(['Scale: ', num2str(s)])
end

%% Pass to distro                                                          
for l=1:6
    for w=1:4
        for d=1:24
            for s=1:trash.max_scale
                Distro_1.location(l).daytype(w).daytime(d).scale(s).new_lim_amount = Scale_fit.new_lim_distr(:, d, s, w, l);     
                Distro_1.location(l).daytype(w).daytime(d).scale(s).new_exp_amount = sum(Distro_1.location(l).daytype(w).daytime(d).scale(s).new_lim_amount.*[0:2]');        
            end
        end
    end
end

%% Small Scale_fit                                                         
Scale_fit_large = Scale_fit;
clear Scale_fit
global Scale_fit
Scale_fit.fix_exp_amount = Scale_fit_large.fix_exp_amount;
Scale_fit.new_lim_distr = Scale_fit_large.new_lim_distr;
Scale_fit.new_lim_exp_am = Scale_fit_large.new_lim_exp_am;
