function [f_pos] = fig_pos_x (Data, Max_data, Width)
    if ~exist('Width')
        Width=0;
    end
    if ~exist('Max_data')
        Max_data=1;
    end
    a = 0.13;
    b = 0.095;
    f_pos = (1-a-b)/Max_data*Data+a-Width*a;

end