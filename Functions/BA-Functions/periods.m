%% Compare days of different cases (season, weekday, ...)


%% Get Period elements
% Get weekday elements
Elem.weekenddays = find(sum(weekday(Good.start_datetime)==[7,1],2));
Elem.weekdays = find(sum(weekday(Good.start_datetime)==[2:6],2));
for m=1:12
    Elem.monthdays{m} = find(month(Good.start_datetime)==m);
end
for s=1:4
    Elem.seasonday{s} = find(quarter(Good.start_datetime)==s);
end
Elem.yeardays = find(year(Good.start_datetime)==2017);


%% Get season
% -> Daytime profile for every month
Month_name = {'Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'};
hour_width = 1/24; % 1/24 = 1 h, 'cause it's in days, actually :|
hour_start = 0;
figure
hold on
tic
for m=1:12
    Month(m).D_Hour = discretize_struct_fast(Good.data(Elem.monthdays{m},3), Good.data(Elem.monthdays{m},:), hour_width, hour_start, 'hours');
    Month(m).Daytime = periodic_course_III(Month(m).D_Hour, 'day');
    plot(Month(m).Daytime.datetime, Month(m).Daytime.mean_amount)
    text(Month(m).Daytime.datetime(12),Month(m).Daytime.mean_amount(12),['\leftarrow ', Month_name{m}], 'FontSize', 6)
    show_progress(m,1,12)
end
xlabel('Time')
ylabel('Mean Amount')
title('Comparison of Daily profiles per Month')
% Result: Okt

[min(Good.start_datetime) max(Good.start_datetime)]
start = Good.data(min(Elem.monthdays{12}),3)
% Discretize over month
D_Month = discretize_struct_fast(Good.data(:,3), Good.data, 1, 0, 'days');

D_Month.Daytime = periodic_course_III(D_Month, 'year');

figure
plot(D_Month.datetime, D_Month.amount)
xlabel('Time')
ylabel('#LVs')
title('

A=[1:3]
monthdis = count_same_integers(month(Good.start_datetime)+12*(year(Good.start_datetime)-2016));
monthdis([1,2,end],:) = [];
figure
bar(monthdis(:,2))

figure 
plot(datetime(datestr(D_Month.Daytime.datenum+367, TIME_FORMAT), 'Format', TIME_FORMAT_DT), D_Month.Daytime.mean_amount)  

%% Get increment of LV amount over whole time
% Starting from Nov16
D_Day=discretize_struct_fast(Good.data(min(Elem.monthdays{11}):end,3), 1, 1, 0, 'days');
figure
scatter(D_Day.datetime, D_Day.amount)
% Exponential for Amount per day till Nov16:
31.92*exp(0.002266*Day);

% cftool(monthdis(:,1)-11, monthdis(:,2))

% Polynomial:
p1 = 3.352;
p2 = 95.13;
p3 = 781.6;

m=0:24;
Fit=p1*m.^2 + p2*m + p3;

figure 
hold on
bar(monthdis(:,2))
plot(Fit)
hold off
xlabel('Month')
ylabel('total #LVs')
title('Development of monthly LVs over 2 years')
legend('Measured', 'Fit: 3.35*M^2+95.13*M+781.60', 'Location', 'northwest')
% Result: no large monthly deviation from general increment

% deviation
figure 
bar(monthdis(:,2)-Fit')


% Get mean LV amount deviation per month from expected (incremented) LV amount
A=[[mod(m,12)+1]' round(monthdis(:,2)-Fit')];
Ab = zeros(12,1);
for c=1:numel(A(:,1))
    Ab(A(c,1)) = Ab(A(c))+A(c,2);
end
Occ = count_same_integers(A(:,1));
Ab=Ab./Occ(:,2);

figure
bar(Ab)
title('Monthly deviation from expected monthly LV amount')
xlabel('Month')
ylabel('#LVs')
% Result: no strong seasonal deviation visible
% except: summer slightly more than autumn

%% Power class distribution per daytype
PCWD=count_same_integers(Good.data(Elem.weekdays,12));
PCWE=count_same_integers(Good.data(Elem.weekenddays,12));
figure
subplot(1,2,1)
pie(PCWD(:,2), strcat(Power_classes.names, ':  ', num2str(PCWD(:,2)/sum((PCWD(:,2)))*100, 2),  ' %'))
title('Weekday Power Class Distribution')
subplot(1,2,2)
pie(PCWE(:,2), strcat(Power_classes.names, ':  ', num2str(PCWE(:,2)/sum((PCWE(:,2)))*100, 2),  ' %'))
title('Weekend Power Class Distribution')

figure
pie(Power_classes.distribution(:,2), strcat(Power_classes.names, ':  ', num2str(Power_classes.distribution(:,2)/Good.info.nu_of_LVs*100, 2),  ' %'))
title('Mean Power Class Distribution')
