function [] = set_color_yy(Size, yyAx, h1, h2, Color1, Color2)
%% Set color and design for plotyy figure

    % constants
    if ~exist('Size')
        Size = 1;
    end
    BA_Test_constants
    
   
    % per axis
    for c=1:2
        GCA = yyAx(c);
        set(get(GCA,'Title'),'Color',Color_matrix{8})
        set(GCA, 'FontSize', 10+Size*2)
        set(GCA, 'FontWeight', 'normal')
        set(GCA, 'XColor', Color_matrix{8})
        set(GCA, 'GridColor', Color_matrix{8})% ...
          %  'YColor', Color_matrix{8}, 'ZColor', Color_matrix{8}, ,...
          %  'MinorGridColor', Color_matrix{8})
        set(gcf, 'color', 'w')
        set(get(GCA, 'XAxis'), 'LineWidth', 0.4*Size)
        set(get(GCA, 'YAxis'), 'LineWidth', 0.4*Size)
        grid on
    end
    
    % curves
    set(yyAx(1), 'YColor', Color_matrix{Color1})
    set(yyAx(2), 'YColor', Color_matrix{Color2})
    h1.LineWidth = Size*0.5;
    h2.LineWidth = Size*0.5;
    h1.Color = Color_matrix{Color1};
    h2.Color = Color_matrix{Color2};

    % fullscreen
    pause(0.00001);
    frame_h = get(handle(gcf),'JavaFrame');
    set(frame_h,'Maximized',1);
    
end

