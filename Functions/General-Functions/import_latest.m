function [File] = import_latest(location, Server)
%Import latest file in the directory

if ~exist('Server')
    Server = false;
end

old_location = cd(location);

Files = dir;

% Ignore directories
for c=1:numel(Files)
    trash.isdir(c,1) = Files(c).isdir;
end
Files = Files(~trash.isdir);

% get newest file
for c=1:numel(Files)
    Dates{c} = Files(c).date;
    % Convert to english
    Dates{c} = strrep(Dates{c}, 'Mrz', 'Mar');
end

[~,Most_recent] = max(datenum(Dates));

New_File = Files(Most_recent);
if ~Server
    fullname = fullfile(New_File.folder, New_File.name);
else
    fullname = New_File.name;
end

File = importdata(fullname);

File.date = Files(Most_recent).date;
File.name = Files(Most_recent).name;

cd(old_location)

end

