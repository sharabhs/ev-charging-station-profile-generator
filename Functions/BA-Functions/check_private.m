%% Check Private Daten

%% Clean up
close all
clear
clc
%% Constants
LOCATION = 4;
SCALE = 1;
BA_Test_constants
LOAD = false;

%% Import
if LOAD
    load BA_data_basis.mat
end

fullname = '../Daten/private_LP.csv';
uiimport(fullname);
Raw.Start = Start;
Raw.End = End;
Raw.LS = Ladepunkt;
Raw.Energy = MeterTotal;
clear Start End Ladepunkt Energy

%% Filter
trash.zero = [find(Raw.Energy<=10); find(strcmp(Raw.Start ,Raw.End))];


find(isnan(Raw.Energy))

Raw.End(trash.zero) = [];
Raw.Start(trash.zero) = [];
Raw.Energy(trash.zero) = [];
Raw.LS(trash.zero) = [];

%% Datetime
Start.dt = datetime(Raw.Start, 'Format', 'dd.MM.yyyy HH:mm');

End.dt = datetime(Raw.End, 'Format', 'dd.MM.yyyy HH:mm');

%% Per hour & daytype
Start.r = dateshift(Start.dt, 'start', 'Hour');
Start.r_day = (datenum(Start.r)-min(datenum(Start.r)))*24;

Start.daytype = weekday(Start.dt);

trash.saturday = find(Start.daytype == 7);
trash.sunday = find(Start.daytype == 1);

Start.daytype = ones(numel(Start.daytype),1);
Start.daytype(trash.saturday) = 2;
Start.daytype(trash.sunday) = 3;

Start.hour = hour(Start.dt);

%% Info
% general
Info.nu_of_days = hours(Start.dt(end) - Start.dt(1))/24;
Info.nu_of_weeks = Info.nu_of_days/7;
Info.nu_of_LV = numel(Start.dt);
Info.nu_of_LP = numel(unique(Raw.LS));
trash.days = [5,1,1];
% for c=1:3
%     Info.nu_of_LV_p_day(c) = sum(Start.week_p_day_LP(1+24*(c-1):24*c));
% end

% per LP
for c=1:5
    Start_LP(c).dt = Start.dt(Raw.LS==c);
    Info.nu_of_days_LP(c,1) = hours(Start_LP(c).dt(end) - Start_LP(c).dt(1))/24;
end


%% Week profile
set(0,'DefaultFigureVisible','off');
Start.week = [];
for c=1:3
    h=histogram(Start.hour(Start.daytype==c), 24);
    Start.week = [Start.week; h.Values'];
end
Start.week_dt = [datetime('2019-01-01 00:00:00', 'Format', 'yyyy-MM-dd HH:mm:ss'):hours(1):datetime('2019-01-01 23:00:00', 'Format', 'yyyy-MM-dd hh:mm:ss')];
set(0,'DefaultFigureVisible','on');


%% Week profile per Charger and day
trash.dim = [.75 .65 .15 .15];

set(0,'DefaultFigureVisible','off');
Start_All.week = zeros(24*3,1);
for d=1:5
    Start_LP(d).week = [];
    for c=1:3
        trash.el = get_same_elements(find(Start.daytype==c), find(Raw.LS==d));
        h=histogram(Start.hour(trash.el), 24);
        trash.p_day = h.Values/(Info.nu_of_days_LP(d)/7*trash.days(c));
        Start_LP(d).week = [Start_LP(d).week; trash.p_day'];
    end
    Start_All.week = Start_All.week + Start_LP(d).week;
    Info.nu_of_LV_p_LP(d) = sum(Raw.LS==d);
end
Start_All.week = Start_All.week / d;

set(0,'DefaultFigureVisible','on');

figure('Units', 'normalized', 'OuterPosition', [0 0 1 1])
hold on
for c=1:5
    plot([0:24*3-1], Start_LP(c).week, 'LineWidth', 4, 'Color', Color_matrix{c},...
        'DisplayName', [num2str(c), '. LS : ', num2str(round(Info.nu_of_days_LP(c))), ' Tage, ', num2str(Info.nu_of_LV_p_LP(c)), ' LV'])
end
hold off
trash.l = legend('show');
trash.l.TextColor = Color_matrix{8};
trash.l.EdgeColor = Color_matrix{8};
trash.l.Location = 'Northwest';
ylabel('#LV pro LS & Tageszeit')
title('Wochenprofile LS Zuhause')

% change xlabel
for d=1:72
    label{d} = [num2str((mod(d-1,24))) ':00'];
end
set(gca, 'XTick', 0:6:72, 'XTickLabel', label(1:6:end))


% Add daytype label
trash.ca=gca;
trash.distance = -max(trash.ca.YLim)/16; 
text('Units', 'Data', 'Position', [12,trash.distance], 'HorizontalAlignment', 'center', 'String', 'Werktag', 'Fontweight', 'bold', 'Color', Color_matrix{8}, 'FontSize', 16)
text('Units', 'Data', 'Position', [12+24,trash.distance], 'HorizontalAlignment', 'center', 'String', 'Samstag', 'Fontweight', 'bold', 'Color', Color_matrix{8}, 'FontSize', 16)
text('Units', 'Data', 'Position', [12+24*2,trash.distance], 'HorizontalAlignment', 'center', 'String', 'Sonntag', 'Fontweight', 'bold', 'Color', Color_matrix{8}, 'FontSize', 16)
text('Units', 'Data', 'Position', [0,trash.distance], 'HorizontalAlignment', 'center', 'String', '|', 'Fontweight', 'bold', 'Color', Color_matrix{8}, 'FontSize', 16)
text('Units', 'Data', 'Position', [24*3,trash.distance], 'HorizontalAlignment', 'center', 'String', '|', 'Fontweight', 'bold', 'Color', Color_matrix{8}, 'FontSize', 16)
text('Units', 'Data', 'Position', [24,trash.distance], 'HorizontalAlignment', 'center', 'String', '|', 'Fontweight', 'bold', 'Color', Color_matrix{8}, 'FontSize', 16)
text('Units', 'Data', 'Position', [24*2,trash.distance], 'HorizontalAlignment', 'center', 'String', '|', 'Fontweight', 'bold', 'Color', Color_matrix{8}, 'FontSize', 16)

set_color(4);

%% Accumulated Profile 
for w=1:4
    for d=1:24
        Intern.distro.new_exp_amount(d,w) = Distro_1.location(LOCATION).daytype(w).daytime(d).scale(SCALE).new_exp_amount_m;
    end
end
Intern.week_distro.exp_amount = Intern.distro.new_exp_amount(1:24*3)';

figure('Units', 'normalized', 'OuterPosition', [0 0 1 1])

hold on
plot([0:24*3-1], Start_All.week, 'Color', Color_matrix{6}, 'Linewidth', 4)
% figure('Units', 'normalized', 'OuterPosition', [0 0 1 1])
plot([0:24*3-1]', Intern.week_distro.exp_amount, 'Color', Color_matrix{4}, 'Linewidth', 4)
hold off

trash.l = legend('Sicher Privat', 'Vermutet Privat');
trash.l.TextColor = Color_matrix{8};
trash.l.EdgeColor = Color_matrix{8};
ylabel('#LV pro LS & Tageszeit')
title('�berpr�fung Wochenprofil LS Zuhause')

% change xlabel
for d=1:72
    label{d} = [num2str((mod(d-1,24))) ':00'];
end
set(gca, 'XTick', 0:6:72, 'XTickLabel', label(1:6:end))

% Add daytype label
trash.ca=gca;
trash.distance = -max(trash.ca.YLim)/16; 
text('Units', 'Data', 'Position', [12,trash.distance], 'HorizontalAlignment', 'center', 'String', 'Werktag', 'Fontweight', 'bold', 'Color', Color_matrix{8}, 'FontSize', 16)
text('Units', 'Data', 'Position', [12+24,trash.distance], 'HorizontalAlignment', 'center', 'String', 'Samstag', 'Fontweight', 'bold', 'Color', Color_matrix{8}, 'FontSize', 16)
text('Units', 'Data', 'Position', [12+24*2,trash.distance], 'HorizontalAlignment', 'center', 'String', 'Sonntag', 'Fontweight', 'bold', 'Color', Color_matrix{8}, 'FontSize', 16)
text('Units', 'Data', 'Position', [0,trash.distance], 'HorizontalAlignment', 'center', 'String', '|', 'Fontweight', 'bold', 'Color', Color_matrix{8}, 'FontSize', 16)
text('Units', 'Data', 'Position', [24*3,trash.distance], 'HorizontalAlignment', 'center', 'String', '|', 'Fontweight', 'bold', 'Color', Color_matrix{8}, 'FontSize', 16)
text('Units', 'Data', 'Position', [24,trash.distance], 'HorizontalAlignment', 'center', 'String', '|', 'Fontweight', 'bold', 'Color', Color_matrix{8}, 'FontSize', 16)
text('Units', 'Data', 'Position', [24*2,trash.distance], 'HorizontalAlignment', 'center', 'String', '|', 'Fontweight', 'bold', 'Color', Color_matrix{8}, 'FontSize', 16)

set_color(4);

%% Max power
Data.duration = End.dt - Start.dt;
Data.duration_s = seconds(Data.duration);
Data.duration_h = Data.duration_s/3600;
Data.Energy_kWh = Raw.Energy/1000;
Data.mean_power = Data.Energy_kWh./Data.duration_h;
min(Data.duration_h)
max(Data.mean_power)
figure
histogram(Data.mean_power, round(max(Data.mean_power))*10)

%% Max Power per LS
for c=1:5
    trash.max_p = Data.mean_power(find(Raw.LS==c));
    figure
    histogram(trash.max_p, round(max(trash.max_p))*10)
    xlabel('Power [KW]')
    ylabel('Occurance')
    title(['Mean Power LS ', num2str(c)])
end
    
% Result: Alle LS nur bis 3.6 kW, au�er LS 5: bis 14 kW

%% Anschlusszeit

figure
histogram(Data.duration_h, round(max(Data.duration_h)),'Normalization','probability', ...
    'DisplayStyle','stairs', 'Edgecolor', Color_matrix{6}, 'Linewidth', 4)

ylabel('H�ufigkeit [%]')
xlabel('Anschlusszeit [h]')
ytix = get(gca, 'YTick');
set(gca, 'YTick',ytix, 'YTickLabel',ytix*100)
title('Anschlusszeit Zuhause')
set_color(4)

%% Actual power
trash.pc = ones(numel(Raw.LS),1);
trash.pc(find(Raw.LS==5)) == 7;

trash.max_p = zeros(numel(Raw.LS),1);
for c=1:5
trash.max_p(find(Raw.LS==c),1) = max(Data.mean_power(find(Raw.LS==c)));
end

AP.data(:,3) = datenum(Start.dt);
AP.data(:,4) = datenum(End.dt);
AP.data(:,7) = Data.Energy_kWh;
AP.data(:,8) = Data.duration_s;
AP.data(:,10) = trash.max_p;
AP.data(:,12) = trash.pc;


[Act_Power_Priv, AP] = get_power_profiles(AP, 1);

figure
plot(Act_Power_Priv.datetime, Act_Power_Priv.data)

%% BA Standzeit
Data.act_dur = AP.data(:,15)/3600;
Data.Standzeit = Data.duration_h-Data.act_dur;
Data.Standzeit_pos = Data.Standzeit;
Data.Standzeit_pos(Data.Standzeit_pos==0) = NaN;

figure
hold on
histogram(Data.act_dur, round(max(Data.act_dur)*4),'Normalization','probability', ...
    'DisplayStyle','stairs', 'Edgecolor', Color_matrix{4}, 'Linewidth', 4)
histogram(Data.duration_h, round(max(Data.duration_h)*4),'Normalization','probability', ...
    'DisplayStyle','stairs', 'Edgecolor', Color_matrix{1}, 'Linewidth', 4)
histogram(Data.Standzeit, round(max(Data.Standzeit)*4),'Normalization','probability', ...
    'DisplayStyle','stairs', 'Edgecolor', Color_matrix{6}, 'Linewidth', 4)
hold off
ylabel('H�ufigkeit [%]')
xlabel('Anschlusszeit [h]')
legend('Ladezeit', 'Anschlusszeit', 'Standzeit')
ytix = get(gca, 'YTick');
% set(gca, 'YTick',ytix, 'YTickLabel',ytix*100)
GCA = gca;
GCA.XLim = [0,24];
GCA.YTickLabel = ytix*100;
title('Anschlusszeit Zuhause')
set_color(4)

%% Zeiten pro LS

figure
hold on
for c=1:5
    trash.act_dur = Data.act_dur(find(Raw.LS==c));
    histogram(trash.act_dur, round(max(trash.act_dur)*2), 'FaceColor', Color_matrix{c}, 'Normalization', 'probability')
end
hold off
set_legend('1','2','3','4','5')
xlabel('Ladezeit [h]')
ylabel('H�ufigkeit')
title(['Ladezeitverteilung pro LS '])
set_color(4)
GCA = gca;
GCA.YTickLabel = ytix*100;
GCA.XLim = [0,24];
% Results:
% - very different

figure
hold on
for c=1:5
    trash.whole_dur = Data.duration_h(find(Raw.LS==c));
    histogram(trash.whole_dur, round(max(trash.whole_dur)*2), 'FaceColor', Color_matrix{c}, 'Normalization', 'probability')
end
hold off
set_legend('1','2','3','4','5')
xlabel('Anschlusszeit [h]')
ylabel('H�ufigkeit')
title(['Anschlusszeitverteilung pro LS '])
set_color(4)
GCA = gca;
GCA.YTickLabel = ytix*100;
GCA.XLim = [0,24];
% Results:
% - some similarities: peak @ 10-15h & 0-5h

figure
hold on
for c=1:5
    trash.idle_dur = Data.Standzeit(find(Raw.LS==c));
    trash.bins =  round(max(trash.idle_dur)*2);
    if trash.bins ==0; trash.bins =1; end
    histogram(trash.idle_dur,trash.bins, 'FaceColor', Color_matrix{c}, 'Normalization', 'probability')
end
hold off
set_legend('1','2','3','4','5')
xlabel('Standzeit [h]')
ylabel('H�ufigkeit')
title(['Standzeitverteilung pro LS '])
set_color(4)
GCA = gca;
GCA.YTickLabel = ytix*100;
GCA.XLim = [0,24];
% Results:
% - very different

%% Charge cases comparison
a=count_same_integers(AP.data(:,13));
g=count_same_integers(Good.data(Location(4).LV_el,13));
figure 
b=bar([a(2:end,2)/sum(a(2:end,2)), g(:,2)/sum(g(:,2))])
set(gca, 'XTick', 1:5, 'XTickLabel', Good.cases.names)
b(1).EdgeColor = Color_matrix{1};
b(1).FaceColor = Color_matrix{6};
b(2).EdgeColor = Color_matrix{8};
b(2).FaceColor = Color_matrix{4};
set_legend('Sicher Privat', 'Vermutet Privat')
GCA = gca;
GCA.YTickLabel = ytix*100;
set_color(4)
title('Vergleich Private Ladef�lle')
ylabel('H�ufigkeit [%]')
xlabel('Ladefall')

% Result: bei sicher Privat: selten in Slope angefangen oder abgebrochen,
% da bei 3.6 kW slope nur 10 min lang

%% Energy pro LS

figure
hold on
for c=1:5
    trash.energy = Data.Energy_kWh(find(Raw.LS==c));
    histogram(trash.energy, round(max(trash.energy)), 'FaceColor', Color_matrix{c},'Normalization','probability')
end
hold off
set_legend('1','2','3','4','5')
xlabel('Energie [kWh]')
ylabel('H�ufigkeit [%]')
title(['Energieverteilung pro LS '])
set_color(4)
GCA = gca;
GCA.YTickLabel = ytix*100;

% Results: extreme Unterschiede -> nicht wirklich verwendbar