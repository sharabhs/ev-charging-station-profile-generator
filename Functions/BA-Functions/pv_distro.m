%% Distribute PV System Size

MEAN = 13.8; % kW
SIGMA = 4; % kW
MIN = 0; % kW
MAX = 100; % kW
PV_Percentage = 15.2;   % %
MODULE_POWER = .20;    % kW
HOUSES = 90;
PV_AMOUNT = round(HOUSES*PV_Percentage/100);
Power = randn(PV_AMOUNT*2,1)*SIGMA + MEAN;
Power(Power>MAX) = [];
Power(Power<MIN) = [];
Power = Power(1:PV_AMOUNT);
% round to multiples of MODULE_POWER
Power = round(Power/MODULE_POWER)*MODULE_POWER;
% number of Modules
Modules = round(Power/MODULE_POWER);

% Results:
Power = [
    14.60
    15.40
    12.20
    13.00
    21.80
    4.80
    22.60
    15.20
    17.80
    7.20
    11.40
    12.80
    15.40
    7.00   ]; % kW


Modules = [
    73
    77
    61
    65
   109
    24
   113
    76
    89
    36
    57
    64
    77
    35  ];




%% Plot result
figure
hold on
h=histogram(Power, round(max(Power)/2));
x = MIN:0.001:MAX;
y = 1/sqrt(2*pi*SIGMA^2)*exp(-(x-MEAN).^2/(2*SIGMA^2))*PV_AMOUNT*2;
plot(x,y, 'Color', Color_matrix{4}, 'LineWidth', 2)
hold off
h.FaceColor = Color_matrix{6};
h.EdgeColor = Color_matrix{8};
xlabel('Leistung [kW]')
ylabel('H�ufigkeit')
title('Verteilung der PV Leistungen')
trash.l = legend('Verteilt', ['Normalverteilung, \mu = ', num2str(MEAN), ' kW, \sigma = ', num2str(SIGMA), ' kW']);
trash.l.TextColor = Color_matrix{8};
trash.l.EdgeColor = Color_matrix{8};
set_color(4)


%% Distribute PV Orientation Angle
% 0� : S�d
% -90� :Ost
% +- 180� : Nord

% S�d:
MEAN = 0;
SIGMA = 5;
PV_AMOUNT_S = round(PV_AMOUNT*0.8);
Orient_S = randn(PV_AMOUNT_S,1)*SIGMA + MEAN;

% S�d-West
MEAN = 45;
SIGMA = 5;
PV_AMOUNT_SW = PV_AMOUNT - PV_AMOUNT_S;
Orient_SW = randn(PV_AMOUNT_SW,1)*SIGMA + MEAN;

% round to full degrees
Orient_S = round(Orient_S);
Orient_SW = round(Orient_SW);
% Display in dhuffeld order:
Orient = [Orient_S; Orient_SW];
Orient(randperm(PV_AMOUNT))

% Plot
figure
hold on
histogram(Orient_S, round(max(Orient_S)-min(Orient_S)) )
histogram(Orient_SW, round(max(Orient_SW)-min(Orient_SW)))
hold off

% Results:
Orient_S = [
     2
     2
    -4
     0
    -1
     3
     5
     6
    -4
     0
     -6 ];
 
 Orient_SW = [
    39
    45
    53  ];

%% Tilt angle

MEAN = 30;
SIGMA = 5;
Tilt = round(randn(PV_AMOUNT,1)*SIGMA + MEAN)

figure
histogram(Tilt)

% Result:
Tilt = [
    31
    29
    32
    32
    25
    29
    19
    36
    27
    24
    29
    23
    30
    27   ];